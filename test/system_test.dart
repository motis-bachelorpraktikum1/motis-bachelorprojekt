import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/app_configuration.dart' as config;
import 'package:supabase_quickstart/main.dart';


void _startApp() {
  Supabase.initialize(
    url: config.url,
    anonKey: config.anon,
  );
  runApp(MyApp());
}


void main() {
  testWidgets("Login Check", (WidgetTester tester) async{
  _startApp();
  
  await tester.pumpAndSettle();
  final emailText = find.byKey(const ValueKey("emailLoginText"));
  final button = find.byKey(const ValueKey("emailLoginButton"));
  const email = "test0303@gmail.com";

  await tester.enterText(emailText, email);
  await tester.tap(button);
  await tester.pumpAndSettle();

  expect(emailText, findsOneWidget);
  expect(button, findsOneWidget);
  }); 
}
