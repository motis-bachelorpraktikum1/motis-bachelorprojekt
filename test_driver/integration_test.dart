import 'package:integration_test/integration_test_driver.dart';

/* to run go to integration-test/app_test.dart and follow further instructions
*/
Future<void> main() => integrationDriver();
