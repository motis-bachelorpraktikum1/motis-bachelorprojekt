import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:supabase_quickstart/main.dart' as app;

/* to run start chormedriver (https://chromedriver.chromium.org/downloads) and print in console:
flutter drive \ --driver=test_driver/integration_test.dart \  --target=integration_test/app_test.dart \  -d chrome
*/
void main() {

IntegrationTestWidgetsFlutterBinding.ensureInitialized();
testWidgets("sign in", (tester) async{
  app.main();

  await tester.pumpAndSettle();
  final emailText = find.byKey(const ValueKey("emailLoginText"));
  final button = find.byKey(const ValueKey("emailLoginButton"));
  const email = "test0303@gmail.com";

  await tester.enterText(emailText, email);
  await tester.tap(button);
  await tester.pumpAndSettle();
});
}
