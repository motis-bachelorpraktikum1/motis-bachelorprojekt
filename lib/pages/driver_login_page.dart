import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class DriverLoginPage extends StatefulWidget {
  const DriverLoginPage({super.key});

  @override
  _DriverLoginPageState createState() => _DriverLoginPageState();
}

class _DriverLoginPageState extends MotisState<DriverLoginPage> {
  bool _isLoading = false;
  bool _redirecting = false;
  late final TextEditingController _emailController;
  late final StreamSubscription<AuthState> _authStateSubscription;

  Future<void> _signIn() async {
    setState(() {
      _isLoading = true;
    });
    try {
      await supabase.auth.signInWithOtp(
        email: _emailController.text,
        emailRedirectTo:
            kIsWeb ? null : 'io.supabase.flutterquickstart://login-callback/',
      );

      await supabase.from(supabase_tables.profilesTableId).update({
        Profile.SbVar_role: 'driver',
      }).eq(Profile.SbVar_eMail, _emailController.text);

      if (mounted) {
        context.showSnackBar(
          message: AppLocalizations.of(context).snackbarLookUpEmail,
        );
        _emailController.clear();
      }
    } on AuthException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    _emailController = TextEditingController();
    _authStateSubscription = supabase.auth.onAuthStateChange.listen((data) {
      if (_redirecting) return;
      final session = data.session;
      if (session != null) {
        _redirecting = true;
        Navigator.of(context).pushReplacementNamed('/');
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _authStateSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).titleMotisDriver),
        backgroundColor: theme.motisOrange,
      ),
      body: ListView(
        padding: getDefaultPadding(),
        children: createDefaultPaddedWidgetList([
          Text(AppLocalizations.of(context).lableEmailInput),
          TextFormField(
            controller: _emailController,
            decoration: InputDecoration(
                labelText: AppLocalizations.of(context).decorationEmailInput),
          ),
          ElevatedButton(
            onPressed: _isLoading ? null : _signIn,
            child: Text(_isLoading
                ? AppLocalizations.of(context).buttonSendEmailLoading
                : AppLocalizations.of(context).buttonSendEmailWaitForInput),
          ),
        ]),
      ),
    );
  }
}
