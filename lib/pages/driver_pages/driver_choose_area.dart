// ignore_for_file: use_string_buffers, prefer_interpolation_to_compose_strings

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:positioned_tap_detector_2/positioned_tap_detector_2.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/pages/location_services/geolocation.dart';
import 'package:supabase_quickstart/supabase_resources/area_of_operation.dart';

class DriverChooseAreaPage extends StatefulWidget {
  final DriverMainPageState driverMainState;

  const DriverChooseAreaPage({super.key, required this.driverMainState});

  @override
  State<DriverChooseAreaPage> createState() => _DriverChooseAreaPageState();
}

class _DriverChooseAreaPageState extends State<DriverChooseAreaPage> {
  late DriverMainPageState _driverMainState;
  late List<LatLng> tappedPoints;
  String polygonWKT = 'POLYGON((';
  late String polygon;
  bool areaSet = false;
  MapController? _mapController;

  @override
  void initState() {
    super.initState();
    _driverMainState = widget.driverMainState;
    tappedPoints = _driverMainState.driverData.areaOfOperationPolygons == null
        ? []
        : _driverMainState
            .driverData.areaOfOperationPolygons!.coordinates.coordinateList;
    polygon = "";
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _mapController = MapController();
    if (tappedPoints.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await _setCenterAndZoomOnCurrentPosition();
      });
    } else {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _setCenterAndZoomOnPolygon();
      });
    }
  }

  ///Moves the Map Controller to the current position of user.
  ///Sets the zoom.
  Future<void> _setCenterAndZoomOnCurrentPosition() async {
    final currentPosition = await determinePosition();
    final bounds = LatLngBounds.fromPoints(
        [LatLng(currentPosition.latitude, currentPosition.longitude)]);
    final CenterZoom cz = _mapController!.centerZoomFitBounds(bounds);
    _mapController!.move(cz.center, cz.zoom - 0.25);
  }

  ///Moves the Map Controller to the center of [tappedPoints].
  ///Sets the zoom.
  void _setCenterAndZoomOnPolygon() {
    final bounds = LatLngBounds.fromPoints(tappedPoints);
    final CenterZoom cz = _mapController!.centerZoomFitBounds(bounds);
    _mapController!.move(cz.center, cz.zoom - 0.25);
  }

  ///Converts [polygonWKT] with WKT being Well-Known-Text to a String
  String _convertToWKT() {
    polygon += polygonWKT;
    for (int i = 0; i < tappedPoints.length; i++) {
      polygon += tappedPoints[i].longitude.toString() +
          ' ' +
          tappedPoints[i].latitude.toString() +
          ', ';
    }

    return polygon += tappedPoints[0].longitude.toString() +
        ' ' +
        tappedPoints[0].latitude.toString() +
        '))';
  }

  @override
  Widget build(BuildContext context) {
    //Markers for current state
    List<Marker> markers = tappedPoints.map((latlng) {
      return Marker(
        width: 40,
        height: 40,
        point: latlng,
        builder: (_) => Icon(
          Icons.my_location_outlined,
          color: Colors.red.shade900,
        ),
      );
    }).toList();

    ///Deletes the area of operation in the database
    void deleteArea() {
      _driverMainState.driverData.areaOfOperationPolygons = AreaOfOperation(
        coordinates: Coordinates(coordinateList: tappedPoints),
      );
      _driverMainState.driverData.polygonWKT = polygon;
      _driverMainState.driverData.deleteAreaOfOperation();
      if (_driverMainState.mounted) {
        _driverMainState.context.showSnackBar(
          message:
              AppLocalizations.of(_driverMainState.context).areaDeletedMessage,
        );
      }
    }

    ///Delete [polygon] and reset [tappedPoints] and [markers].
    void deletePolygon() {
      setState(() {
        polygon = "";
        tappedPoints = [];
        markers = [];
      });
      deleteArea();
    }

    ///Returns an alert to confirm action to delete area
    AlertDialog getAlert() {
      return AlertDialog(
        title: Text(AppLocalizations.of(_driverMainState.context).alarm),
        content: Text(
            AppLocalizations.of(_driverMainState.context).deleteAreaString),
        actions: <Widget>[
          TextButton(
            child: Text(
              AppLocalizations.of(context).cancel,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text(
              AppLocalizations.of(context).accept,
            ),
            onPressed: () {
              Navigator.of(context).pop();
              setState(() {
                deletePolygon();
              });
            },
          ),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
          title: Text(
              AppLocalizations.of(_driverMainState.context).chooseAreaString)),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Text(AppLocalizations.of(_driverMainState.context)
                  .clickToAddAreaString),
            ),
            Flexible(
              child: FlutterMap(
                mapController: _mapController,
                options: MapOptions(onTap: _handleTap),
                children: [
                  TileLayer(
                    urlTemplate:
                        'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                    userAgentPackageName: 'com.motis.on_demand',
                  ),
                  MarkerLayer(markers: markers),
                  PolygonLayer(
                    polygons: [
                      Polygon(
                        points: tappedPoints,
                        borderColor: Colors.red.shade900,
                        color: Colors.red.shade900,
                        borderStrokeWidth: 4.0,
                      ),
                    ],
                  )
                ],
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: _saveArea,
                    child: Text(AppLocalizations.of(context).saveChangesLabel)),
                ElevatedButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return getAlert();
                        },
                      );
                    },
                    child:
                        Text(AppLocalizations.of(context).deleteButtonLabel)),
              ],
            ),
          ],
        ),
      ),
    );
  }

  ///Adds the [tapPosition] with corresponding [latlng] to [tappedPoints].
  void _handleTap(TapPosition tapPosition, LatLng latlng) {
    setState(() {
      tappedPoints.add(latlng);
    });
  }

  ///Saves chosen area to database
  void _saveArea() {
    _convertToWKT();
    _driverMainState.driverData.areaOfOperationPolygons =
        AreaOfOperation(coordinates: Coordinates(coordinateList: tappedPoints));
    _driverMainState.driverData.polygonWKT = polygon;
    _driverMainState.driverData.saveToSupabase();
    if (_driverMainState.mounted) {
      _driverMainState.context.showSnackBar(
        message: AppLocalizations.of(_driverMainState.context).areaSavedMessage,
      );
    }
  }
}
