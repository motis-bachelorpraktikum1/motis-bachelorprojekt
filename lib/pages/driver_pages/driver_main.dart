import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_account_page.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_choose_area.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_drive_page.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_schedule_page.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_trips_page.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

///Handles the Navigation of the Driver App.
///Receives [userProfile] and [driverData].
class DriverMainPage extends StatefulWidget {
  final Profile userProfile;
  final Driver driverData;

  const DriverMainPage(
      {super.key, required this.userProfile, required this.driverData});

  @override
  MotisState<DriverMainPage> createState() => DriverMainPageState(
        driverData: driverData,
        userProfile: userProfile,
      );
}

class DriverMainPageState extends MotisState<DriverMainPage> {
  DriverMainPageState({
    required this.driverData,
    required this.userProfile,
  });

  DriverDrivePage? drivePage;
  DriverAccountPage? accountPage;
  DriverViewTripsPage? tripViewPage;
  DriverChooseAreaPage? chooseAreaPage;
  DriverSchedulePage? driverSchedulePage;

  Profile userProfile;
  Driver driverData;

  int _selectedPage = 1;

  @override
  void initState() {
    accountPage = DriverAccountPage(driverMainState: this);
    tripViewPage = DriverViewTripsPage(driverMainState: this);
    drivePage = DriverDrivePage(driverMainState: this);
    chooseAreaPage = DriverChooseAreaPage(driverMainState: this);
    driverSchedulePage = DriverSchedulePage(driverMainState: this);
    super.initState();
  }

  Profile getCurrentProfile() {
    return userProfile;
  }

  Driver getCurrentDriver() {
    return driverData;
  }

  ///Switches and builds the shown Page corresponding with given [index].
  Widget getPage(int index) {
    switch (index) {
      case 0:
        return drivePage!;
      case 1:
        return tripViewPage!;
      case 2:
        return chooseAreaPage!;
      case 3:
        return DriverSchedulePage(driverMainState: this);
      case 4:
        return accountPage!;
      default:
        return const Text('No Page found');
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedPage = index;
    });
  }

  ///Returns a Bottom Bar widget
  Widget getBottomBar(BuildContext context, int selectedIndex) {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return ClipRRect(
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          child: BottomNavigationBar(
            selectedItemColor: state.darkTheme ? Colors.white : Colors.black,
            unselectedItemColor: state.darkTheme
                ? Color.fromARGB(255, 192, 192, 192)
                : Colors.white,
            backgroundColor: state.darkTheme
                ? Color.fromARGB(255, 151, 87, 3)
                : theme.motisOrange,
            onTap: (value) => _onItemTapped(value),
            currentIndex: selectedIndex,
            items: [
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.drive_eta,
                ),
                label: AppLocalizations.of(context).startDrivingLabel,
                backgroundColor:
                    state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.list,
                ),
                label: AppLocalizations.of(context).tripsLabel,
                backgroundColor:
                    state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.circle_outlined,
                ),
                label: AppLocalizations.of(context).workAreaLabel,
                backgroundColor:
                    state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.calendar_month,
                ),
                label: AppLocalizations.of(context).shiftPlannerLabel,
                backgroundColor:
                    state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.settings,
                ),
                label: AppLocalizations.of(context).settings,
                backgroundColor:
                    state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getPage(_selectedPage),
      bottomNavigationBar: getBottomBar(context, _selectedPage),
    );
  }
}
