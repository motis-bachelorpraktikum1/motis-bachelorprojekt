import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/supabase_resources/driver_trip.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as tables;

class DriverViewTripsPage extends StatefulWidget {
  final DriverMainPageState driverMainState;

  const DriverViewTripsPage({super.key, required this.driverMainState});

  @override
  State<DriverViewTripsPage> createState() => _DriverViewTripsPageState();
}

class _DriverViewTripsPageState extends State<DriverViewTripsPage> {
  late DriverMainPageState _driverMainState;
  List<DriverTrip> trips = [];
  List<List<String>> passengerFullNames = [];

  late Future<void> future;

  @override
  void initState() {
    super.initState();

    _driverMainState = widget.driverMainState;
    future = setupPage();
  }

  ///Gets all trips for the driver and the passenger fullnames for the trips
  Future<bool> setupPage() async {
    await _fetchTrips();
    await getPassengerNames();

    return true;
  }

  ///Adds passenger names to [passengerFullNames]
  Future<void> getPassengerNames() async {
    for (final DriverTrip trip in trips) {
      final List<String> passengersInCurrentTrip = [];
      for (final String passengerId in trip.passengers!) {
        final String passengerFullName =
            await _fetchPassengerFullName(passengerId);

        passengersInCurrentTrip.add(passengerFullName);
      }
      passengerFullNames.add(passengersInCurrentTrip);
    }
  }

  ///Gets the passenger fullname from the database for [passengerId]
  Future<String> _fetchPassengerFullName(String passengerId) async {
    final data = await supabase
        .from(tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, passengerId)
        .single() as Map;

    final Profile passengerProfile = Profile.parseProfileModel(data);
    return passengerProfile.fullName!;
  }

  ///Fetches trips in the future and in the last 30 days
  Future<void> _fetchTrips() async {
    final data = await supabase
        .from(tables.driverTripsTableId)
        .select()
        .eq("driver_id", supabase.auth.currentUser!.id)
        .gte('start_time', DateTime.now())
        .order('start_time', ascending: true) as List;
    trips = data.map((e) => DriverTrip.parseDriverOnDuty(e as Map)).toList();
  }

  ///Reloads the page to update shown trips
  Future<void> _reload() async {
    setState(() {
      future = _fetchTrips();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return const Center(child: CircularProgressIndicator());
          case ConnectionState.done:
          default:
            return Scaffold(
              appBar: AppBar(
                title: Text(AppLocalizations.of(context).driverTripsLabel),
                actions: [
                  ElevatedButton(
                    onPressed: () => _reload(),
                    child: const Icon(
                      Icons.refresh_rounded,
                    ),
                  ),
                  const Padding(padding: EdgeInsets.all(2.0)),
                ],
              ),
              body: trips.isEmpty
                  ? Wrap(
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          clipBehavior: Clip.antiAlias,
                          child: Column(
                            // ignore: prefer_const_literals_to_create_immutables
                            children: [
                              Container(
                                width: 400,
                                height: 400,
                                alignment: Alignment.center,
                                child: Text(
                                  AppLocalizations.of(context)
                                      .currentlyNoTripsRefresh,
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 25),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                  : ListView.separated(
                      shrinkWrap: true,
                      itemCount: trips.length,
                      itemBuilder: (context, tripIndex) {
                        final currentTrip = trips[tripIndex];
                        return Wrap(
                          children: [
                            InkWell(
                              onTap: () => showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return Dialog(
                                    child: ListView.separated(
                                      shrinkWrap: true,
                                      itemCount: currentTrip.passengers!.length,
                                      itemBuilder: (context, passengerIndex) {
                                        return ListTile(
                                          title: Text(
                                            passengerFullNames[tripIndex]
                                                [passengerIndex],
                                          ),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, int index) {
                                        return const Divider(
                                          indent: 10.0,
                                          endIndent: 10.0,
                                        );
                                      },
                                    ),
                                  );
                                },
                              ),
                              child: Card(
                                child: ListTile(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  isThreeLine: true,
                                  title: Text(
                                    "${currentTrip.startAdress} - ${currentTrip.endAdress}",
                                  ),
                                  subtitle: Text(
                                    "${DateFormat('dd.MM.yyyy kk:mm').format(currentTrip.startTime!)} - ${DateFormat('dd.MM.yyyy kk:mm').format(currentTrip.endTime!)}\n${AppLocalizations.of(context).clickToSeePassengers}",
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return const Divider(
                          indent: 10.0,
                          endIndent: 10.0,
                        );
                      },
                    ),
            );
        }
      },
    );
  }
}
