// ignore_for_file: use_string_buffers, prefer_interpolation_to_compose_strings
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/event.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_add_schedule_page.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/shift_schedule.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;
import 'package:table_calendar/table_calendar.dart';

class DriverSchedulePage extends StatefulWidget {
  final DriverMainPageState driverMainState;
  const DriverSchedulePage({super.key, required this.driverMainState});

  @override
  State<DriverSchedulePage> createState() => _DriverSchedulePageState();
}

class _DriverSchedulePageState extends State<DriverSchedulePage> {
  late DriverMainPageState _driverMainState;
  List<ShiftSchedule> shifts = [];
  bool isReload = false;
  DateTime _selectedDate = DateTime.now();
  DateTime _focusedDate = DateTime.now();
  CalendarFormat _calendarFormat = CalendarFormat.month;
  List<ShiftSchedule> _selectedShifts = [];
  DateFormat dateFormatDate = DateFormat('dd.MM.yyyy');
  DateFormat dateFormatTimeOnly = DateFormat('kk:mm');

  late Future<void> future;

  @override
  void initState() {
    super.initState();
    _driverMainState = widget.driverMainState;
    future = setupPage();
  }

  ///Gets the shift for [_selectedDate]
  Future<bool> setupPage() async {
    if (isReload) {
      _selectedShifts = _getShiftForDay(_selectedDate);
      isReload = false;
      return true;
    }

    var shiftData =
        await supabase.from(supabase_tables.driverScheduleTableId).select().eq(
              ShiftSchedule.SbVar_driverId,
              _driverMainState.getCurrentProfile().userId,
            );

    shifts = (shiftData as List)
        .map((e) => ShiftSchedule.parseShiftSchedule(e as Map))
        .toList();

    shifts.sort((a, b) => a.startTime!.compareTo(b.startTime!));

    _selectedShifts = _getShiftForDay(_selectedDate);

    return true;
  }

  ///Get a List of [Event] for the provided [day]
  List<Event> _getShiftEventForDay(DateTime day) {
    return _getShiftForDay(day).map((e) => Event('Event')).toList();
  }

  ///Get a List of [ShiftSchedule] for provided [day]
  List<ShiftSchedule> _getShiftForDay(DateTime day) {
    return shifts
        .where(
          (shift) =>
              isSameDay(shift.startTime, day) || isSameDay(shift.endTime, day),
        )
        .toList();
  }

  ///Card containing shift information
  Widget _buildSelectedDayShiftCards() {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        if (_selectedShifts.isEmpty) {
          return const SizedBox(
            width: 20,
            height: 20,
          );
        }
        List<Widget> widgetList = [];
        widgetList.add(
          Divider(
            color: Colors.blueGrey.shade600,
          ),
        );
        widgetList.addAll(
          _selectedShifts
              .map(
                (e) => Card(
                  margin: getDefaultPadding(),
                  color: state.darkTheme
                      ? const Color.fromARGB(255, 65, 80, 87)
                      : Colors.grey.shade300,
                  surfaceTintColor: state.darkTheme
                      ? const Color.fromARGB(255, 65, 80, 87)
                      : Colors.grey.shade300,
                  shadowColor: state.darkTheme ? Colors.black : Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: getDefaultPadding(),
                        child: Container(
                          width: 80,
                          child: Text(
                            dateFormatDate.format(e.startTime!),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: getDefaultPadding(),
                          child: Text(
                            '${dateFormatTimeOnly.format(e.startTime!)} - ${dateFormatTimeOnly.format(e.endTime!)}',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Padding(
                        padding: getDefaultPadding(),
                        child: Container(
                          width: 80,
                          child: Text(
                            isSameDay(e.startTime, e.endTime)
                                ? ' '
                                : dateFormatDate.format(e.endTime!),
                            textAlign: TextAlign.right,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
              .toList(),
        );

        return Flexible(
          child: ListView(
            shrinkWrap: true,
            padding: getDefaultPadding(),
            children: createDefaultPaddedWidgetList(
              widgetList,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            }
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  AppLocalizations.of(context).shiftPlannerLabel,
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 18),
                    child: ElevatedButton(
                      onPressed: () => Navigator.of(context)
                          .push(
                            MaterialPageRoute(
                              settings: const RouteSettings(
                                name: '/driver/schedule/add',
                              ),
                              builder: (_) => DriverAddSchedulePage(
                                profileUUID: _driverMainState
                                    .getCurrentProfile()
                                    .userId!,
                                selectedDate: _selectedDate,
                                shifts: shifts,
                              ),
                            ),
                          )
                          .then(
                            (shift) => setState(() {
                              shifts.add(shift as ShiftSchedule);
                              isReload = false;
                              future = setupPage();
                            }),
                          ),
                      child: Icon(
                        Icons.add,
                        color: state.darkTheme ? Colors.white : Colors.black,
                      ),
                    ),
                  )
                ],
              ),
              body: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: createDefaultPaddedWidgetList([
                    TableCalendar(
                      focusedDay: _focusedDate,
                      firstDay: DateTime.now().subtract(
                        const Duration(
                          days: 60,
                        ),
                      ),
                      lastDay: DateTime.now().add(
                        const Duration(days: 90),
                      ),
                      selectedDayPredicate: (day) {
                        return isSameDay(_selectedDate, day);
                      },
                      onDaySelected: (selectedDay, focusedDay) {
                        setState(() {
                          _selectedDate = selectedDay;
                          _focusedDate = focusedDay;
                          isReload = true;
                          future = setupPage();
                        });
                      },
                      calendarFormat: _calendarFormat,
                      onFormatChanged: (format) {
                        setState(() {
                          _calendarFormat = format;
                          isReload = true;
                          future = setupPage();
                        });
                      },
                      onPageChanged: (focusedDay) {
                        _focusedDate = focusedDay;
                      },
                      eventLoader: (day) {
                        return _getShiftEventForDay(day);
                      },
                    ),
                    _buildSelectedDayShiftCards(),
                  ]),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
