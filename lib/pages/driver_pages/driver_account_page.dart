import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/components/avatar.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/pages/driver_pages/utils_driver.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class DriverAccountPage extends StatefulWidget {
  final DriverMainPageState driverMainState;
  const DriverAccountPage({super.key, required this.driverMainState});

  @override
  State<DriverAccountPage> createState() => _DriverAccountPageState();
}

class _DriverAccountPageState extends State<DriverAccountPage> {
  late DriverMainPageState _driverMainState;
  Profile? userProfile;
  Driver? driverData;
  final _usernameController = TextEditingController();
  final _nameController = TextEditingController();
  String? _avatarUrl;
  bool _updated = false;
  late bool chosenTheme;

  late Future<bool> future;

  @override
  void initState() {
    super.initState();
    _driverMainState = widget.driverMainState;
    future = setupPage();
  }

  ///Fetches Profile and Data to show on Page.
  ///Sets up the Textfields and loads profile picture
  Future<bool> setupPage() async {
    userProfile = _driverMainState.getCurrentProfile();
    driverData = _driverMainState.getCurrentDriver();
    _avatarUrl = _driverMainState.getCurrentProfile().avatarUrl;
    _usernameController.text = userProfile!.userName!;
    _nameController.text = userProfile!.fullName!;

    return true;
  }

  ///Updates avatar with [imageUrl]
  Future<void> _onUpload(String imageUrl) async {
    try {
      final userId = supabase.auth.currentUser!.id;
      await supabase.from(supabase_tables.profilesTableId).update({
        Profile.SbVar_userId: userId,
        Profile.SbVar_avatarUrl: imageUrl,
        Profile.SbVar_updatedAt: DateTime.now().toIso8601String(),
      }).match({'id': userId});
      if (mounted) {
        context.showSnackBar(
          message: AppLocalizations.of(context).updateProfilePicture,
        );
      }
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }
    userProfile!.avatarUrl = imageUrl;
    setState(() {
      _avatarUrl = imageUrl;
    });
  }

  ///Updates profile
  Future<void> _updateProfile() async {
    try {
      final userId = supabase.auth.currentUser!.id;
      final userName = _usernameController.text;
      final fullName = _nameController.text;
      await supabase.from(supabase_tables.profilesTableId).update({
        Profile.SbVar_userId: userId,
        Profile.SbVar_userName: userName,
        Profile.SbVar_fullName: fullName,
        Profile.SbVar_updatedAt: DateTime.now().toIso8601String(),
      }).match({'id': userId});
      if (mounted) {
        context.showSnackBar(
          message: AppLocalizations.of(context).updateProfile,
        );
      }
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }

    final userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, userProfile!.userId)
        .single();

    userProfile = Profile.parseProfileModel(userData as Map);

    _driverMainState.setState(() {
      _driverMainState.userProfile = userProfile!;
      _updated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(
      builder: (context, theme, child) {
        chosenTheme = !theme.darkTheme;
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            }
            return Scaffold(
              appBar: createDriverAppBar(
                  AppLocalizations.of(context).appbarProfile),
              body: SingleChildScrollView(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: createDefaultPaddedWidgetList([
                    Avatar(
                      imageUrl: _avatarUrl,
                      onUpload: _onUpload,
                    ),
                    TextField(
                      controller: _usernameController,
                      decoration: InputDecoration(
                          labelText:
                              AppLocalizations.of(context).lableUserName),
                      textAlign: TextAlign.left,
                    ),
                    TextField(
                      controller: _nameController,
                      decoration: InputDecoration(
                          labelText:
                              AppLocalizations.of(context).nameLastnameLabel),
                      textAlign: TextAlign.left,
                    ),
                    ElevatedButton(
                      onPressed: _updateProfile,
                      child: Text(
                        AppLocalizations.of(context).buttonWaitingUpdateProfile,
                      ),
                    ),                   
                    if(driverData?.number_of_ratings == 0)
                    Text(AppLocalizations.of(context).driverRating),
                    if (!(driverData?.number_of_ratings == 0))
                                      RatingBarIndicator(
                      rating: driverData!.accumulated_rating!.toDouble() / driverData!.number_of_ratings!,
                      itemBuilder: (context, index) => const Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      itemCount: 5,
                      itemSize: 50.0,
                      direction: Axis.horizontal,
                    ),

                    Text(AppLocalizations.of(context).switchThemeText),
                    Switch(
                      activeColor: Colors.green,
                      activeTrackColor: Colors.blue,
                      inactiveThumbColor: Colors.blueGrey.shade600,
                      inactiveTrackColor: Colors.grey.shade400,
                      splashRadius: 50.0,
                      value: chosenTheme,
                      onChanged: (value) => {
                        chosenTheme = value,
                        if (value)
                          theme.switchThemeLight()
                        else
                          theme.switchThemeDark(),
                      },
                    ),
                  ]),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
