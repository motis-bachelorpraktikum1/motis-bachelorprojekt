import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

PreferredSizeWidget createDriverAppBar(String title) {
  final String pageTitle = title;
  return PreferredSize(
    preferredSize: const Size.fromHeight(kToolbarHeight),
    child: Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        final String pageTitle = title;
        return AppBar(
          title: Text(
            pageTitle,
            style: state.darkTheme
                ? const TextStyle(fontSize: 24, color: Colors.white)
                : const TextStyle(fontSize: 24, color: Colors.black),
          ),
          foregroundColor: state.darkTheme ? Colors.black : Colors.white,
          centerTitle: true,
        );
      },
    ),
  );
}

Widget getDefaultBottomBar(BuildContext context, int selectedIndex) {
  return Consumer<theme.MotisTheme>(
    builder: (context, state, child) {
      return ClipRRect(
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        child: BottomNavigationBar(
          selectedItemColor: state.darkTheme ? Colors.white : Colors.black,
          unselectedItemColor: state.darkTheme ? Colors.grey : Colors.white,
          backgroundColor:
              state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
          onTap: (value) {},
          currentIndex: selectedIndex,
          items: [
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.drive_eta,
              ),
              label: AppLocalizations.of(context).startDrivingLabel,
              backgroundColor:
                  state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
            ),
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.list,
              ),
              label: AppLocalizations.of(context).tripsLabel,
              backgroundColor:
                  state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
            ),
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.circle_outlined,
              ),
              label: AppLocalizations.of(context).workAreaLabel,
              backgroundColor:
                  state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
            ),
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.calendar_month,
              ),
              label: AppLocalizations.of(context).shiftPlannerLabel,
              backgroundColor:
                  state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
            ),
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.settings,
              ),
              label: AppLocalizations.of(context).settings,
              backgroundColor:
                  state.darkTheme ? theme.motisDarkOrange : theme.motisOrange,
            ),
          ],
        ),
      );
    },
  );
}

Scaffold getDefaultLoadingScaffold(BuildContext context, int selectedIndex) {
  return Scaffold(
    appBar: createDriverAppBar(AppLocalizations.of(context).appbarHomePage),
    body: ListView(
      padding: getDefaultPadding(),
      children: createDefaultPaddedWidgetList([
        Container(
          width: 50,
          height: 50,
          padding: getDefaultPadding(),
          alignment: Alignment.center,
          child: const CircularProgressIndicator(),
        ),
      ]),
    ),
    bottomNavigationBar: getDefaultBottomBar(context, selectedIndex),
  );
}
