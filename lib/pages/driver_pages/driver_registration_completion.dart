import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/driver_pages/utils_driver.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class DriverRegistrationPage extends StatefulWidget {
  const DriverRegistrationPage({super.key});

  @override
  _DriverRegistrationPage createState() => _DriverRegistrationPage();
}

class _DriverRegistrationPage extends State<DriverRegistrationPage> {
  Profile? currentProfile;
  Driver? currentDriver;
  String? profileUUID;
  final _userNameController = TextEditingController();
  final _fullNameController = TextEditingController();
  bool _reload = false;

  @override
  void initState() {
    super.initState();
    profileUUID = supabase.auth.currentSession!.user.id;
  }

  ///Saves user name and fullname to database
  Future<void> _saveData() async {
    if (currentProfile == null) {
      return;
    }
    currentProfile!.userName = _userNameController.value.text;
    currentProfile!.fullName = _fullNameController.value.text;

    Driver newDriver = Driver();
    newDriver.userId = profileUUID;
    newDriver.registered = false;

    try {
      await currentProfile!.saveToSupabase();
      await newDriver.saveToSupabase();

      setState(() {
        currentDriver = newDriver;
      });
    } on PostgrestException catch (error) {
      context.showSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unknownErrorMessage,
      );
    }
  }

  ///Fills username and fullname textfields if there is data already
  Future<bool> _prepareData() async {
    if (_reload) {
      _reload = false;
      return true;
    }
    var userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, profileUUID)
        .single();

    currentProfile = Profile.parseProfileModel(userData as Map);

    List driverData = await supabase
        .from(supabase_tables.driverTableId)
        .select()
        .eq(Driver.SbVar_userId, profileUUID) as List;

    if (driverData.isNotEmpty) {
      currentDriver = Driver.parseDriverModel(driverData[0] as Map);
    }

    _userNameController.text = currentProfile!.userName ?? '';
    _fullNameController.text = currentProfile!.fullName ?? '';

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _prepareData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        } else if (currentDriver != null) {
          return Scaffold(
            appBar: createDriverAppBar(
              AppLocalizations.of(context).welcomeToMotisOnDemand,
            ),
            body: Container(
              alignment: Alignment.topCenter,
              child: Container(
                transformAlignment: Alignment.center,
                width: 400,
                child: ListView(
                  children: [
                    SizedBox(
                      height: getDefaultVerticalSize(),
                    ),
                    Text(
                      AppLocalizations.of(context).driverAlreadyAppliedText,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontWeight: FontWeight.w300, fontSize: 20),
                    ),
                    SizedBox(
                      height: getDefaultVerticalSize() / 2,
                    ),
                    const Icon(
                      Icons.add_task,
                      size: 50,
                      color: theme.motisBlue,
                    ),
                  ],
                ),
              ),
            ),
          );
        }
        return Scaffold(
          appBar: createDriverAppBar(
            AppLocalizations.of(context).welcomeToMotisOnDemand,
          ),
          body: Container(
            alignment: Alignment.center,
            child: Container(
              transformAlignment: Alignment.center,
              width: 400,
              child: Column(
                children: createDefaultPaddedWidgetList([
                  Text(
                    AppLocalizations.of(context)
                        .registrationInstructionPassengerMessage,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontWeight: FontWeight.w100,
                      fontSize: 18,
                    ),
                  ),
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).nameLastnameLabel,
                    _fullNameController,
                  ),
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).userNameLabel,
                    _userNameController,
                  ),
                  ElevatedButton(
                    onPressed: () => _saveData(),
                    child: Text(AppLocalizations.of(context).saveChangesLabel),
                  ),
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
