// ignore_for_file: cancel_subscriptions

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/pages/location_services/geolocation.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/osrm_route_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/predictions_resource.dart';
import 'package:supabase_quickstart/supabase_resources/driver_trip.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

class DriverDrivePage extends StatefulWidget {
  final DriverMainPageState driverMainState;
  const DriverDrivePage({super.key, required this.driverMainState});

  @override
  State<DriverDrivePage> createState() => _DriverDrivePageState();
}

class _DriverDrivePageState extends State<DriverDrivePage> {
  late DriverMainPageState _driverMainState;

  List<DriverTrip>? tripsInSchedule;
  DriverTrip? currentTrip;
  DateTime currentDate = DateTime.now();
  Timer? drivingTimer;

  //Trip has begun.
  bool isOnTrip = false;
  //Trip has ended.
  bool tripFinished = false;
  //Driver has begun driving.
  bool begunDriving = false;

  MapController? _mapController;

  StreamSubscription<Position>? positionStreamSubscription;
  Position? currentPosition;
  LatLng? currentPositionLatLng;
  LatLng? currentTripStartLatLng;
  LatLng? currentTripEndLatLng;
  Polyline? currentRoute;
  List<Marker>? currentMarkers;
  Marker? currentDriverMarker;
  String? pickupString;
  String? dropOffString;

  late Future<bool> future;

  @override
  void initState() {
    super.initState();
    _driverMainState = widget.driverMainState;
    future = setupPage();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    //setting Center on Driver Current Location using Map Controller
    _mapController = MapController();
    if (currentPosition != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await _setCenterAndZoomOnCurrentPosition(currentPosition!);
      });
    }
  }

  ///Fetches the trips in current shift.
  ///Gets the current position of the driver.
  Future<bool> setupPage() async {
    await _fetchTrips();
    await _getCurrentPosition();

    if (tripsInSchedule!.isNotEmpty) {
      await _updateDisplay();
    }
    //Setup complete
    return true;
  }

  ///Updates the markers, route, [pickupString] and [dropOffString] of current trip
  Future<void> _updateDisplay() async {
    currentTrip = tripsInSchedule!.first;
    //route is trip start and end
    if (isOnTrip) {
      _setPositions();
      _setPointMarkers();
      await fetchCurrentRoute(
        PositionModel(
          latitude: currentTripStartLatLng!.latitude,
          lng: currentTripStartLatLng!.longitude,
        ),
        PositionModel(
          latitude: currentTripEndLatLng!.latitude,
          lng: currentTripEndLatLng!.longitude,
        ),
      );
      _setPointMarkers();
    }
    //route is current position and trip start
    else {
      _setPositions();
      await fetchCurrentRoute(
        PositionModel(
          latitude: currentPosition!.latitude,
          lng: currentPosition!.longitude,
        ),
        PositionModel(
          latitude: currentTripStartLatLng!.latitude,
          lng: currentTripStartLatLng!.longitude,
        ),
      );
      _setPointMarkers();
    }
    setState(() {
      if (tripsInSchedule!.isEmpty) {
        pickupString = AppLocalizations.of(context).currentlyNoTripsWait;
        dropOffString = '';
      } else {
        pickupString =
            "${currentTrip!.startAdress} ${DateFormat('dd.MM.yyyy - kk:mm').format(currentTrip!.startTime!)}";

        dropOffString =
            "${currentTrip!.endAdress} ${DateFormat('dd.MM.yyyy - kk:mm').format(currentTrip!.endTime!)}";
      }
    });
  }

  ///Moves the Map Controller to the current position of user.
  ///Sets the zoom.
  Future<void> _setCenterAndZoomOnCurrentPosition(
      Position currentPosition) async {
    final bounds = LatLngBounds.fromPoints(
        [LatLng(currentPosition.latitude, currentPosition.longitude)]);
    final CenterZoom cz = _mapController!.centerZoomFitBounds(bounds);
    _mapController!.move(cz.center, cz.zoom - 0.5);
  }

  ///Gets the trips in current shift in ascending order
  Future<void> _fetchTrips() async {
    final trips = await supabase.rpc(
      'get_driver_trips_for_shift',
      params: {
        'driver_id_p': _driverMainState.userProfile.userId,
        'date': currentDate.toIso8601String()
      },
    ).order('start_time', ascending: true) as List;

    tripsInSchedule =
        trips.map((e) => DriverTrip.parseDriverOnDuty(e as Map)).toList();

    if (begunDriving) {
      await _updateDisplay();
    }
  }

  ///Sets the Polyline for the Start and Destination provided
  Future<void> fetchCurrentRoute(
      PositionModel startP, PositionModel destP) async {
    final RouteResponse response = await getRouteResource(startP, destP);
    final Content routeInformation = response.content;

    final List<double> coordinates = routeInformation.polyline.coordinates;

    List<LatLng> points = [];

    for (int i = 0; i < coordinates.length - 1; i = i + 2) {
      final LatLng point = LatLng(
        coordinates.elementAt(i),
        coordinates.elementAt(i + 1),
      );
      points.add(point);
    }
    setState(() {
      currentRoute =
          Polyline(points: points, color: Colors.deepPurple, strokeWidth: 4.0);
    });
  }

  ///Checks if the start of a trip has been reached. If so it removes the
  ///This Function is to be called periodically once Driver has begun driving.
  Future<void> _checkIfStartReached() async {
    await _fetchTrips();
    if (tripsInSchedule!.isEmpty) {
      return;
    }

    if (DateTime.now().isBefore(currentTrip!.startTime!)) {
      return;
    }

    final distanceDriverToStart = Geolocator.distanceBetween(
        currentPosition!.latitude,
        currentPosition!.longitude,
        currentTrip!.startLocation!.latitude as double,
        currentTrip!.startLocation!.lng as double);

    if (distanceDriverToStart < 75.0) {
      setState(() {
        isOnTrip = true;
      });
      await _updateDisplay();
    }
  }

  ///Checks if a destination has been reached.
  ///Removes the current trip and sets up the next trip.
  ///Also fetches trips to keep driver updated.
  Future<void> _checkIfDestionationReached() async {
    await _fetchTrips();
    if (tripsInSchedule!.isEmpty) {
      return;
    }

    //check if driver is there before time
    if (DateTime.now().isBefore(currentTrip!.endTime!)) {
      return;
    }

    //if current Trip was the last known Trip, remove it and reset everything
    if (tripsInSchedule!.length == 1) {
      tripsInSchedule!.removeAt(0);
      setState(() {
        pickupString = AppLocalizations.of(context).currentlyNoTripsWait;
        dropOffString = '';
        currentTripStartLatLng = null;
        currentTripEndLatLng = null;
        currentRoute = null;
        _setPointMarkers();
      });
      return;
    }

    final distanceDriverToDestination = Geolocator.distanceBetween(
        currentPosition!.latitude,
        currentPosition!.longitude,
        currentTrip!.endLocation!.latitude as double,
        currentTrip!.endLocation!.lng as double);

    if (distanceDriverToDestination < 75.0) {
      setState(() {
        tripFinished = true;
      });

      tripsInSchedule!.removeAt(0);
      isOnTrip = false;
      await _updateDisplay();

      setState(() {
        tripFinished = false;
      });
    }
  }

  Future<void> _getCurrentPosition() async {
    currentPosition = await determinePosition();

    setState(() {
      currentPositionLatLng =
          LatLng(currentPosition!.latitude, currentPosition!.longitude);
      _setDriverMarker();
    });
  }

  ///Listens to location changes and updates the current Position
  Future<void> _trackDriver() async {
    const LocationSettings locationSettings = LocationSettings(
      accuracy: LocationAccuracy.high,
      distanceFilter: 100,
      timeLimit: Duration(minutes: 10),
    );

    final StreamSubscription<Position> positionStream =
        Geolocator.getPositionStream(locationSettings: locationSettings)
            .listen((Position? position) {
      if (position != null) {
        setState(() {
          currentPosition = Position(
            longitude: position.longitude,
            latitude: position.latitude,
            timestamp: position.timestamp,
            accuracy: position.accuracy,
            altitude: position.altitude,
            heading: position.heading,
            speed: position.speed,
            speedAccuracy: position.speedAccuracy,
          );
          currentPositionLatLng =
              LatLng(currentPosition!.latitude, currentPosition!.longitude);
          _setDriverMarker();
          if (begunDriving) {
            _setCenterAndZoomOnCurrentPosition(currentPosition!);
          }
        });
      }
    });
  }

  ///Sets the Positions in LatLng Format for the current Position and Trip
  void _setPositions() {
    currentPositionLatLng =
        LatLng(currentPosition!.latitude, currentPosition!.longitude);

    currentTripStartLatLng = LatLng(
        currentTrip!.startLocation!.latitude as double,
        currentTrip!.startLocation!.lng as double);

    currentTripEndLatLng = LatLng(currentTrip!.endLocation!.latitude as double,
        currentTrip!.endLocation!.lng as double);
  }

  ///Sets the Markers for the current position
  void _setDriverMarker() {
    currentDriverMarker = Marker(
      width: 50,
      height: 50,
      point: currentPositionLatLng!,
      builder: (_) => Icon(
        Icons.local_taxi_rounded,
        color: Colors.blue.shade800,
      ),
    );
    setState(() {
      currentMarkers = [currentDriverMarker!];
    });
  }

  ///Sets the Markers for the current Trip
  void _setPointMarkers() {
    _setDriverMarker();
    if (tripsInSchedule!.isEmpty) {
      return;
    }
    Marker startPositionMarker = Marker(
      width: 50,
      height: 50,
      point: currentTripStartLatLng!,
      builder: (_) => Icon(
        Icons.location_history,
        color: Colors.red.shade900,
      ),
    );

    Marker endPositionMarker = Marker(
      width: 50,
      height: 50,
      point: currentTripEndLatLng!,
      builder: (_) => Icon(
        Icons.location_on,
        color: Colors.red.shade900,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<Marker>? showCurrentMarkers = currentMarkers;
    final Polyline? showCurrentRoute = currentRoute;

    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return const Center(child: CircularProgressIndicator());
              case ConnectionState.done:
              default:
                return Scaffold(
                  appBar: AppBar(
                    actions: [
                      ElevatedButton(
                        onPressed: begunDriving ? null : _beginDriving,
                        child:
                            Text(AppLocalizations.of(context).buttonStartTrip),
                      ),
                      const Padding(padding: EdgeInsets.all(2.0)),
                      ElevatedButton(
                        onPressed: begunDriving ? _stopDriving : null,
                        child:
                            Text(AppLocalizations.of(context).buttonFinishTrip),
                      ),
                      const Padding(padding: EdgeInsets.all(2.0)),
                    ],
                  ),
                  body: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          tripsInSchedule!.isNotEmpty
                              ? "$pickupString ${AppLocalizations.of(context).toLabel}\n $dropOffString"
                              : pickupString == null
                                  ? ""
                                  : "$pickupString",
                        ),
                      ),
                      Flexible(
                        child: FlutterMap(
                          mapController: _mapController,
                          options: MapOptions(
                              center: currentPositionLatLng, zoom: 15),
                          children: [
                            TileLayer(
                              urlTemplate:
                                  'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                              userAgentPackageName: 'com.motis.on_demand',
                            ),
                            MarkerLayer(
                              markers: showCurrentMarkers ?? [],
                            ),
                            PolylineLayer(
                              polylines: showCurrentRoute == null
                                  ? []
                                  : [showCurrentRoute],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
            }
          },
        );
      },
    );
  }

  ///Starts timer to continously fetch current Location and Trips
  Future<void> _beginDriving() async {
    _trackDriver();
    begunDriving = true;
    //if no trips
    if (tripsInSchedule!.isEmpty) {
      setState(() {
        pickupString = AppLocalizations.of(context).currentlyNoTripsWait;
        dropOffString = '';
      });
    } else if (!isOnTrip) {
      _setPositions();
      await fetchCurrentRoute(
        PositionModel(
          latitude: currentPosition!.latitude,
          lng: currentPosition!.longitude,
        ),
        PositionModel(
          latitude: currentTripStartLatLng!.latitude,
          lng: currentTripStartLatLng!.longitude,
        ),
      );
      setState(() {
        _setPointMarkers();
      });
    } else {
      _setPositions();
      await fetchCurrentRoute(
        PositionModel(
          latitude: currentTripStartLatLng!.latitude,
          lng: currentTripStartLatLng!.longitude,
        ),
        PositionModel(
          latitude: currentTripEndLatLng!.latitude,
          lng: currentTripEndLatLng!.longitude,
        ),
      );
      setState(() {
        _setPointMarkers();
      });
    }

    drivingTimer = Timer.periodic(
      const Duration(seconds: 30),
      (timer) => {
        if (tripsInSchedule!.isEmpty)
          {
            _checkIfStartReached(),
          },
        if (!isOnTrip)
          {
            _checkIfStartReached(),
          }
        else
          {
            _checkIfDestionationReached(),
          }
      },
    );

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await _setCenterAndZoomOnCurrentPosition(currentPosition!);
    });
  }

  ///Cancels timers and location streams
  void _stopDriving() {
    drivingTimer?.cancel();
    positionStreamSubscription?.cancel();

    setState(() {
      begunDriving = false;
    });
  }
}
