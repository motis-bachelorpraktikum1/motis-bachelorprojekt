import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

///Creates a Flutter Map
class MotisMap extends FlutterMap {
  LatLng? start;
  LatLng? destination;
  Marker? startMarker;
  Marker? destinationMarker;
  Polyline? polyLine;
  BuildContext? context;
  MapController? routeMapController;
  LatLngBounds? bounds;

  MotisMap.defaultMap(this.context, this.start)
      : super(
          options: MapOptions(
            center: start,
            slideOnBoundaries: true,
            screenSize: MediaQuery.of(context!).size,
          ),
          nonRotatedChildren: [
            AttributionWidget.defaultWidget(
              source: 'OpenStreetMap contributors',
            ),
          ],
          children: [
            TileLayer(
              urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
              userAgentPackageName: 'com.motis.on_demand',
            )
          ],
        );

  MotisMap.showRoute(
    this.context,
    this.polyLine,
    this.start,
    this.startMarker,
    this.destinationMarker,
    this.routeMapController,
  ) : super(
          mapController: routeMapController,
          options: MapOptions(
            slideOnBoundaries: true,
            screenSize: MediaQuery.of(context!).size,
          ),
          nonRotatedChildren: [
            AttributionWidget.defaultWidget(
              source: 'OpenStreetMap contributors',
            ),
          ],
          children: [
            TileLayer(
              urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
              userAgentPackageName: 'com.example.app',
            ),
            PolylineLayer(
              polylines: [polyLine!],
            ),
            MarkerLayer(
              markers: [startMarker!, destinationMarker!],
            ),
          ],
        );
}
