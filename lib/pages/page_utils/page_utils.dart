import 'package:flutter/material.dart';

///This method returns the default padding for the page.
EdgeInsetsGeometry getDefaultPadding() {
  return const EdgeInsets.symmetric(vertical: 18, horizontal: 12);
}

double getDefaultVerticalSize() {
  return 36;
}

///This method takes the given widget list and inserts the padding between the objects by the given padding amount.
List<Widget> createPaddedWidgetList(double padding, List<Widget> widgets) {
  List<Widget> paddedList = <Widget>[];
  if (widgets == null || widgets.isEmpty) {
    return widgets;
  }

  paddedList.add(widgets[0]);

  for (int i = 1; i < widgets.length; i++) {
    paddedList.add(SizedBox(height: padding, width: 18));
    paddedList.add(widgets[i]);
  }
  return paddedList;
}

///This Method work similiar to createPaddedWidgetList with the exception that the padding is set to the default value "18"
List<Widget> createDefaultPaddedWidgetList(List<Widget> widgets) {
  return createPaddedWidgetList(18, widgets);
}

Widget createStaticTextInputRow(String labelText, String initialValue) {
  return Flexible(
    child: TextFormField(
      decoration: InputDecoration(labelText: labelText),
      initialValue: initialValue,
      textAlign: TextAlign.left,
      readOnly: true,
    ),
  );
}

/// Use to create a row with a Labeltext and an inputfield for the text
Widget createLabeledTextInputRow(
    String labelText, TextEditingController textEditingController) {
  return Flexible(
    child: TextField(
      decoration: InputDecoration(labelText: labelText),
      controller: textEditingController,
      textAlign: TextAlign.left,
    ),
  );
}

/// Creates a standard Datacolumn for a tableheader
DataColumn createTableHeaderDataColumn(String name) {
  return DataColumn(
    label: Expanded(
      child: Text(
        name,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    ),
  );
}

DataCell createTextDataCell(String content) {
  return DataCell(Text(content));
}
