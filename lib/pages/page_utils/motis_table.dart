import 'package:flutter/material.dart';

///Creates a [DataTable].
class MotisTable extends DataTable {
  MotisTable(
      {required super.columns,
      required super.rows,
      super.clipBehavior = Clip.hardEdge});
}

///Returns a DataColumn with [name] as title.
DataColumn createHeaderColumn(String name) {
  return DataColumn(
    label: Expanded(
      child: Text(
        name,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
    ),
  );
}

///Returns a list of DataColumn for provided List of String [columnNames].
List<DataColumn> createColumnsForColumnNames(List<String> columnNames) {
  return columnNames.map((e) => createHeaderColumn(e)).toList();
}
