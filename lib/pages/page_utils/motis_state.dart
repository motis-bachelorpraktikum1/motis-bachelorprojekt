import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

abstract class MotisState<T extends StatefulWidget> extends State
    with Diagnosticable {
  Future<void> _signOut() async {
    try {
      await supabase.auth.signOut();
    } on AuthException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unexpectedError);
    }
    if (mounted) {
      Navigator.of(context).pushReplacementNamed('/');
    }
  }

  TextButton createLogoutButton() {
    return TextButton(
        onPressed: _signOut,
        child: Text(AppLocalizations.of(context).buttonLogOut),
        style: TextButton.styleFrom(
            foregroundColor: Colors.white, backgroundColor: theme.motisRed));
  }
}
