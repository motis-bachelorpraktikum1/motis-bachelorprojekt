import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/admin_driver_add_schedule_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/admin_driver_trip_view.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/driver_pages/event.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/shift_schedule.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;
import 'package:table_calendar/table_calendar.dart';

class AdminDriverSchedulePage extends StatefulWidget {
  AdminDriverSchedulePage({super.key, required this.profileUUID});

  String profileUUID;

  @override
  _AdminDriverSchedulePage createState() =>
      _AdminDriverSchedulePage(profileUUID: profileUUID);
}

class _AdminDriverSchedulePage extends State<AdminDriverSchedulePage> {
  _AdminDriverSchedulePage({required this.profileUUID});

  String profileUUID;
  List<ShiftSchedule> shifts = [];
  bool isReload = false;
  DateTime _selectedDate = DateTime.now();
  DateTime _focusedDate = DateTime.now();
  CalendarFormat _calendarFormat = CalendarFormat.month;
  List<ShiftSchedule> _selectedShifts = [];
  DateFormat dateFormatDate = DateFormat('dd.MM.yyyy');
  DateFormat dateFormatTimeOnly = DateFormat('kk:mm');

  @override
  void initState() {
    super.initState();
  }

  Future<bool> setupPage() async {
    if (isReload) {
      _selectedShifts = _getShiftForDay(_selectedDate);
      isReload = false;
      return true;
    }

    //****************** Get Shifts ******************
    var shiftData =
        await supabase.from(supabase_tables.driverScheduleTableId).select().eq(
              ShiftSchedule.SbVar_driverId,
              profileUUID,
            );

    shifts = (shiftData as List)
        .map((e) => ShiftSchedule.parseShiftSchedule(e as Map))
        .toList();

    shifts.sort((a, b) => a.startTime!.compareTo(b.startTime!));

    _selectedShifts = _getShiftForDay(_selectedDate);

    return true;
  }

  List<Event> _getShiftEventForDay(DateTime day) {
    return _getShiftForDay(day).map((e) => Event('Event')).toList();
  }

  void _openTripViewPage(ShiftSchedule shift) {
    Navigator.of(context).push(
      MaterialPageRoute(
        settings: const RouteSettings(
          name: '/admin/driver/trip-view',
        ),
        builder: (_) => AdminDriverTripView(
          profileUUID: profileUUID,
          shift: shift,
        ),
      ),
    );
  }

  List<ShiftSchedule> _getShiftForDay(DateTime day) {
    return shifts
        .where(
          (shift) =>
              isSameDay(shift.startTime, day) || isSameDay(shift.endTime, day),
        )
        .toList();
  }

  Widget _buildSelectedDayShiftCards() {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        if (_selectedShifts.isEmpty) {
          return const SizedBox(
            width: 20,
            height: 20,
          );
        }
        List<Widget> widgetList = [];
        widgetList.add(
          const Divider(
            color: Colors.black,
          ),
        );
        widgetList.addAll(
          _selectedShifts
              .map(
                (e) => Card(
                  margin: getDefaultPadding(),
                  color: state.darkTheme
                      ? const Color.fromARGB(255, 65, 80, 87)
                      : Colors.grey.shade300,
                  surfaceTintColor: state.darkTheme
                      ? const Color.fromARGB(255, 65, 80, 87)
                      : Colors.grey.shade300,
                  shadowColor: state.darkTheme ? Colors.black : Colors.white,
                  child: InkWell(
                    onTap: () => _openTripViewPage(e),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: getDefaultPadding(),
                          child: Container(
                            width: 80,
                            child: Text(
                              dateFormatDate.format(e.startTime!),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: getDefaultPadding(),
                            child: Text(
                              '${dateFormatTimeOnly.format(e.startTime!)} - ${dateFormatTimeOnly.format(e.endTime!)}',
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Padding(
                          padding: getDefaultPadding(),
                          child: Container(
                            width: 80,
                            child: Text(
                              isSameDay(e.startTime, e.endTime)
                                  ? ' '
                                  : dateFormatDate.format(e.endTime!),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
              .toList(),
        );

        return Flexible(
          child: ListView(
            shrinkWrap: true,
            padding: getDefaultPadding(),
            children: createDefaultPaddedWidgetList(
              widgetList,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return FutureBuilder(
          future: setupPage(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return getDefaultLoadingScreen();
            }
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  AppLocalizations.of(context).shiftPlannerLabel,
                ),
                backgroundColor: theme.motisPurple,
                actions: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 18),
                    child: ElevatedButton(
                      onPressed: () => Navigator.of(context)
                          .push(
                            MaterialPageRoute(
                              settings: const RouteSettings(
                                name: '/admin/driver/shift/add',
                              ),
                              builder: (_) => AdminDriverAddSchedulePage(
                                profileUUID: profileUUID,
                                selectedDate: _selectedDate,
                                shifts: shifts,
                              ),
                            ),
                          )
                          .then(
                            (shift) => setState(() {
                              shifts.add(shift as ShiftSchedule);
                              isReload = false;
                            }),
                          ),
                      child: Icon(
                        Icons.add,
                        color: state.darkTheme ? Colors.white : Colors.black,
                      ),
                    ),
                  )
                ],
              ),
              body: Padding(
                padding: const EdgeInsets.all(8),
                child: ListView(
                  children: createDefaultPaddedWidgetList([
                    TableCalendar(
                      focusedDay: _focusedDate,
                      firstDay: DateTime.now().subtract(
                        const Duration(
                          days: 60,
                        ),
                      ),
                      lastDay: DateTime.now().add(
                        const Duration(days: 90),
                      ),
                      selectedDayPredicate: (day) {
                        return isSameDay(_selectedDate, day);
                      },
                      onDaySelected: (selectedDay, focusedDay) {
                        setState(() {
                          _selectedDate = selectedDay;
                          _focusedDate = focusedDay;
                          isReload = true;
                        });
                      },
                      calendarFormat: _calendarFormat,
                      onFormatChanged: (format) {
                        setState(() {
                          _calendarFormat = format;
                          isReload = true;
                        });
                      },
                      onPageChanged: (focusedDay) {
                        _focusedDate = focusedDay;
                      },
                      eventLoader: (day) {
                        return _getShiftEventForDay(day);
                      },
                    ),
                    _buildSelectedDayShiftCards(),
                  ]),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
