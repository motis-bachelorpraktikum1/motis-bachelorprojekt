import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/predictions_resource.dart';
import 'package:supabase_quickstart/supabase_resources/driver_trip.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

PreferredSizeWidget createAdminAppBar(String title) {
  final String pageTitle = title;
  return PreferredSize(
    preferredSize: const Size.fromHeight(kToolbarHeight),
    child: Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        bool chosenTheme = !state.darkTheme;
        return AppBar(
          title: Text(pageTitle),
          backgroundColor: theme.motisPurple,
          foregroundColor: Colors.white,
          centerTitle: true,
          actions: [
            Switch(
              activeColor: Colors.green,
              activeTrackColor: Colors.blue,
              inactiveThumbColor: Colors.blueGrey.shade600,
              inactiveTrackColor: Colors.grey.shade400,
              splashRadius: 50.0,
              value: chosenTheme,
              onChanged: (value) => {
                chosenTheme = value,
                if (value)
                  state.switchThemeLight()
                else
                  state.switchThemeDark(),
              },
            ),
          ],
        );
      },
    ),
  );
}

Future<void> _signOut(BuildContext context, bool mounted) async {
  try {
    await supabase.auth.signOut();
  } on AuthException catch (error) {
    context.showErrorSnackBar(message: error.message);
  } catch (error) {
    context.showErrorSnackBar(
        message: AppLocalizations.of(context).unknownErrorMessage);
  }
  if (mounted) {
    Navigator.of(context).pushReplacementNamed('/');
  }
}

Drawer createAdminDrawer(BuildContext context, State state) {
  return Drawer(
      child: ListView(
    padding: getDefaultPadding(),
    children: createDefaultPaddedWidgetList([
      Title(
        color: theme.motisBlue,
        child: Text(
          AppLocalizations.of(context).motisAdminTitle,
          textAlign: TextAlign.center,
        ),
      ),
      ElevatedButton(
        onPressed: () => Navigator.pushNamed(context, '/admin/passenger'),
        child: Text(AppLocalizations.of(context).manageUsersLabel),
      ),
      ElevatedButton(
        onPressed: () => Navigator.pushNamed(context, '/admin/drivers'),
        child: Text(AppLocalizations.of(context).manageDriversLabel),
      ),
      ElevatedButton(
        onPressed: () => Navigator.pushNamed(context, '/admin/vehicle'),
        child: Text(AppLocalizations.of(context).manageVehiclesLabel),
      ),
      ElevatedButton(
        onPressed: () => Navigator.pushNamed(context, '/admin/fleet'),
        child: Text(AppLocalizations.of(context).manageFleetsLabel),
      ),
      ElevatedButton(
        onPressed: () => Navigator.pushNamed(context, '/admin/trips-today'),
        child: Text(AppLocalizations.of(context).adminTripViewTitle),
      ),
      ElevatedButton(
        onPressed: () =>
            Navigator.pushNamed(context, '/admin/driver-applications'),
        child: Text(AppLocalizations.of(context).seeDriverApplicationsLabel),
      ),
      TextButton(
        onPressed: () => _signOut(context, state.mounted),
        style: TextButton.styleFrom(
            foregroundColor: Colors.white, backgroundColor: theme.motisRed),
        child: Text(AppLocalizations.of(context).signOutLabel),
      ),
    ]),
  ));
}

Widget createInfoCard(double height, List<Widget> elements) {
  return Consumer<theme.MotisTheme>(
    builder: (context, state, child) {
      return Container(
        width: 800,
        height: height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(
            color: state.darkTheme
                ? Color.fromARGB(255, 18, 104, 145)
                : Colors.lightBlueAccent,
          ),
          borderRadius:
              BorderRadius.all(Radius.circular(getDefaultVerticalSize())),
          color: state.darkTheme
              ? Color.fromARGB(255, 18, 104, 145)
              : Colors.lightBlueAccent,
        ),
        child: Padding(
          padding: getDefaultPadding(),
          child: ListView(
            children: elements,
          ),
        ),
      );
    },
  );
}

Container getDefaultLoadingScreen() {
  return Container(
    width: 50,
    height: 50,
    padding: getDefaultPadding(),
    alignment: Alignment.center,
    child: const CircularProgressIndicator(),
  );
}

/**
 * Returns true if any element contains the searchString
 */
bool anyContains(List<String> list, String searchString) {
  for (String search in list) {
    if (search.toLowerCase().contains(searchString.toLowerCase())) {
      return true;
    }
  }
  return false;
}

bool sameHourAndMinute(DateTime a, DateTime b) {
  return a.hour == b.hour && a.minute == b.minute;
}

Color randomColorGenerator() {
  final r = Random();
  return Color.fromRGBO(r.nextInt(256), r.nextInt(256), r.nextInt(256), 0.75);
}

int calculateNumberOfHoursBetween(DateTime from, DateTime to) {
  int dayMod = from.day != to.day ? 24 : 0;
  return to.minute - from.minute + 60 * ((to.hour - from.hour) + dayMod);
}

class TableData {
  TableData({
    required this.driverTrip,
    required this.passenger,
    required this.startTime,
    required this.endTime,
    required this.startLocation,
    required this.endLocation,
    required this.endAddress,
  });

  DriverTrip driverTrip;
  Profile passenger;
  DateTime startTime;
  DateTime endTime;
  PositionModel startLocation;
  PositionModel endLocation;
  String endAddress;
  TableData? prev;
  TableData? next;
}
