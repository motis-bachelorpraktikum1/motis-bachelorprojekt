import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/edit_driver_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/edit_passenger_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_table.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/passenger.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/role.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class ProfileTablePage extends StatefulWidget {
  const ProfileTablePage(this.type, {super.key});

  final Role type;

  @override
  _ProfileTablePage createState() => _ProfileTablePage(type);
}

class _ProfileTablePage extends MotisState<ProfileTablePage> {
  _ProfileTablePage(this._pageType);

  List<Object>? _dataList;

  final Role _pageType;
  final _searchController = TextEditingController();

  bool _isFiltering = false;

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  String getAppBarTitle() {
    switch (_pageType) {
      case Role.DRIVER:
        return AppLocalizations.of(context).manageDriversLabel;
      case Role.PASSENGER:
        return AppLocalizations.of(context).manageUsersLabel;
      default:
        throw ArgumentError(
          'Invalid TableType returned {}',
          _pageType.toString(),
        );
    }
  }

  String buildTypePath() {
    switch (_pageType) {
      case Role.DRIVER:
        return '/admin/drivers';
      case Role.PASSENGER:
        return '/admin/passenger';
      default:
        throw ArgumentError(
          'Invalid TableType returned {}',
          _pageType.toString(),
        );
    }
  }

  List<DataColumn> _getTableHeader() {
    switch (_pageType) {
      case Role.DRIVER:
      case Role.PASSENGER:
        return createColumnsForColumnNames([
          AppLocalizations.of(context).eMailLabel,
          AppLocalizations.of(context).userNameLabel,
          AppLocalizations.of(context).nameLastnameLabel,
          AppLocalizations.of(context).editLabel
        ]);
      default:
        throw ArgumentError(
          'Invalid TableType returned {}',
          _pageType.toString(),
        );
    }
  }

  Future<bool> _getData() async {
    if (_isFiltering) {
      _isFiltering = false;
      return true;
    }

    //Get Data depending on driver or passenger type
    switch (_pageType) {
      case Role.DRIVER:
        return _getDriverData();
      case Role.PASSENGER:
        return _getPassengerData();
      default:
        throw ArgumentError(
          'Invalid TableType returned {}',
          _pageType.toString(),
        );
    }
  }

  Future<bool> _getPassengerData() async {
    var userData;
    final passengerData =
        await supabase.from(supabase_tables.passengerTableId).select();

    List<String> passengerList = (passengerData as List)
        .map((e) => Passenger.parsePassenger(e as Map).userId!)
        .toList();

    userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .in_(Profile.SbVar_userId, passengerList)
        .eq(Profile.SbVar_role, 'passenger');

    _dataList = (userData as List)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();

    return true;
  }

  Future<bool> _getDriverData() async {
    var userData;

    final driverData = await supabase
        .from(supabase_tables.driverTableId)
        .select()
        .eq(Driver.SbVar_registered, 'true');

    List<String> driverList = (driverData as List)
        .map((e) => Driver.parseDriverModel(e as Map).userId!)
        .toList();

    userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .in_(Profile.SbVar_userId, driverList)
        .eq(Profile.SbVar_role, 'driver');

    _dataList = (userData as List<dynamic>)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();

    return true;
  }

  List<DataRow> _createDataRows() {
    switch (_pageType) {
      case Role.DRIVER:
      case Role.PASSENGER:
        return _createUserRows();
      default:
        throw ArgumentError(
          'Invalid TableType returned {}',
          _pageType.toString(),
        );
    }
  }

  List<DataRow> _createUserRows() {
    if (_dataList == null) {
      return [];
    }
    return (_dataList! as List<Profile>)
        .where(
          (element) => anyContains(
            [element.eMail!, element.fullName!, element.userName!],
            _searchController.text,
          ),
        )
        .map(
          (e) => DataRow(
            cells: [
              createTextDataCell(e.eMail!),
              createTextDataCell(e.userName!),
              createTextDataCell(e.fullName!),
              DataCell(
                ElevatedButton(
                  onPressed: () => _editDataPoint(e.userId!),
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: theme.motisPurple,
                  ),
                  child: Icon(
                    Icons.edit_outlined,
                    size: 24,
                    semanticLabel: AppLocalizations.of(context).editLabel,
                  ),
                ),
              ),
            ],
          ),
        )
        .toList();
  }

  void _editDataPoint(String entityId) {
    // open corresponding edit page for passenger or driver
    switch (_pageType) {
      case Role.DRIVER:
        Navigator.of(context)
            .push(
          MaterialPageRoute(
            settings: const RouteSettings(name: '/admin/driver/edit'),
            builder: (_) => AddEditDriverPage(
              driverUUID: entityId,
            ),
          ),
        )
            .then((value) {
          setState(() {});
        });
        break;
      case Role.PASSENGER:
        Navigator.of(context)
            .push(
          MaterialPageRoute(
            settings: const RouteSettings(name: '/admin/passenger/edit'),
            builder: (_) => AddEditPassengerPage(
              profileUUID: entityId,
            ),
          ),
        )
            .then((value) {
          setState(() {});
        });
        break;
      default:
        throw ArgumentError(
          'Invalid TableType returned {}',
          _pageType.toString(),
        );
    }
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: createAdminAppBar(getAppBarTitle()),
          drawer: createAdminDrawer(context, this),
          body: Padding(
            padding: getDefaultPadding(),
            child: Column(
              children: createDefaultPaddedWidgetList([
                Flexible(
                  child: TextFormField(
                    controller: _searchController,
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).searchLabel,
                    ),
                    onChanged: (search) => setState(() {
                      _isFiltering = true;
                    }),
                  ),
                ),
                MotisTable(
                  columns: _getTableHeader(),
                  rows: _createDataRows(),
                ),
              ]),
            ),
          ),
        );
      },
    );
  }
}
