import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/edit_driver_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_table.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class DriverApplicationsTable extends StatefulWidget {
  const DriverApplicationsTable({super.key});

  @override
  _ProfileTablePage createState() => _ProfileTablePage();
}

class _ProfileTablePage extends MotisState<DriverApplicationsTable> {
  List<Driver>? _appliedDriverList;
  List<Profile>? _profileList;

  final _searchController = TextEditingController();

  bool _isFiltering = false;

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  List<DataColumn> _getTableHeader() {
    return createColumnsForColumnNames([
      AppLocalizations.of(context).eMailLabel,
      AppLocalizations.of(context).userNameLabel,
      AppLocalizations.of(context).nameLastnameLabel,
      AppLocalizations.of(context).editLabel
    ]);
  }

  Future<bool> _getData() async {
    if (_isFiltering) {
      _isFiltering = false;
      return true;
    }

    //****************** Get applied Drivers ******************
    var driverData = await supabase
        .from(supabase_tables.driverTableId)
        .select()
        .eq(Driver.SbVar_registered, 'false');

    _appliedDriverList = (driverData as List<dynamic>)
        .map((e) => Driver.parseDriverModel(e as Map))
        .toList();

    if (_appliedDriverList!.isEmpty) {
      _profileList = [];
      return true;
    }

    final List<String> ids = _appliedDriverList!.map((e) => e.userId!).toList();

    //****************** Get corresponding Profiles ******************
    var profileData = await supabase
          .from(supabase_tables.profilesTableId)
          .select()
          .in_(Profile.SbVar_userId, ids);

    _profileList = (profileData as List)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();
    return true;
  }

  List<DataRow> _createDataRows() {
    if (_profileList == null) {
      return [];
    }
    return (_profileList! as List<Profile>)
        .where(
          (element) => anyContains(
            [element.eMail!, element.fullName!, element.userName!],
            _searchController.text,
          ),
        )
        .map(
          (e) => DataRow(
            cells: [
              createTextDataCell(e.eMail!),
              createTextDataCell(e.userName!),
              createTextDataCell(e.fullName!),
              DataCell(
                ElevatedButton(
                  onPressed: () => _editDataPoint(e.userId!),
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: theme.motisPurple,
                  ),
                  child: Icon(
                    Icons.check,
                    size: 24,
                    semanticLabel: AppLocalizations.of(context).editLabel,
                  ),
                ),
              ),
            ],
          ),
        )
        .toList();
  }

  void _editDataPoint(String entityId) {
    Navigator.of(context).push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/driver/edit'),
        builder: (_) => AddEditDriverPage(
          driverUUID: entityId,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: createAdminAppBar(
              AppLocalizations.of(context).seeDriverApplicationsLabel),
          drawer: createAdminDrawer(context, this),
          body: Padding(
            padding: getDefaultPadding(),
            child: Column(
              children: createDefaultPaddedWidgetList([
                Flexible(
                  child: TextFormField(
                    controller: _searchController,
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).searchLabel,
                    ),
                    onChanged: (search) => setState(() {
                      _isFiltering = true;
                    }),
                  ),
                ),
                Flexible(
                  child: MotisTable(
                    columns: _getTableHeader(),
                    rows: _createDataRows(),
                  ),
                ),
              ]),
            ),
          ),
        );
      },
    );
  }
}
