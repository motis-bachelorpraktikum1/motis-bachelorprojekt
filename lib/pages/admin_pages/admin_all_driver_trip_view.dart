import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/predictions_resource.dart';
import 'package:supabase_quickstart/supabase_resources/driver_trip.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/shift_schedule.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class AdminAllDriverTripView extends StatefulWidget {
  const AdminAllDriverTripView({super.key, required this.selectedTime});

  final DateTime selectedTime;

  @override
  _AdminAllDriverTripView createState() => _AdminAllDriverTripView();
}

class _AdminAllDriverTripView extends State<AdminAllDriverTripView> {
  late String profileUUID;
  late DateTime startDate;
  late DateTime endDate;
  late ShiftSchedule shift;
  List<Profile> driverProfiles = [];
  List<Profile> passengers = [];
  Map<String, List<TableData>> tableData = {};
  bool isReload = false;

  //View Variables
  late int viewRange;

  double chartBarHeight = 60;
  double nameWidth = 250; //The block on the left that holds the driver mail
  int viewRangeToFitScreen = 1020; //How many units you can see in your screen
  int lowerBoundRangeToFitScreen = 420;
  int upperBoundRangeToFitScreen = 1420;
  late double chartViewWidth =
      MediaQuery.of(context).size.width; //scale of table DONT GO LOWER

  DateFormat timeFormat = DateFormat('dd.MM.yyyy kk:mm');

  @override
  void initState() {
    super.initState();
    DateTime selectedDate = widget.selectedTime;
    startDate = DateTime(selectedDate.year, selectedDate.month, selectedDate.day);
    endDate = startDate.add(
      const Duration(
        days: 1,
      ),
    );
    viewRange = calculateNumberOfHoursBetween(startDate, endDate);
  }

  Future<bool> _getData() async {
    if (isReload) {
      isReload = false;
      return true;
    }
    tableData = {};

    chartViewWidth = MediaQuery.of(context).size.width * 0.7;

    //****************** GetTrips during day ******************
    final driverTripsData = await supabase
        .from(supabase_tables.driverTripsTableId)
        .select()
        .gte(DriverTrip.SbVar_startTime, startDate.toIso8601String())
        .lte(DriverTrip.SbVar_endTime, endDate.toIso8601String());

    List<DriverTrip> driverTrips = (driverTripsData as List)
        .map((e) => DriverTrip.parseDriverOnDuty(e as Map))
        .toList();

    //****************** Get Drivers ******************
    List<String> driverIDs =
        driverTrips.map((e) => e.driverId!).toSet().toList();

    final driverData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .in_(Profile.SbVar_userId, driverIDs);

    driverProfiles = (driverData as List)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();

    driverTrips.sort(
      (a, b) => a.startTime!.compareTo(b.startTime!),
    );

    //****************** Retrieve PassengerData ******************
    List<String> passengerIds = driverTrips
        .expand((e) => e.passengers!)
        .toSet() //distinct Ids
        .toList();

    final passengerData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .in_(Profile.SbVar_userId, passengerIds);

    passengers = (passengerData as List)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();

    //****************** Accumulate Data ******************
    //Sort into driver buckets
    Map<String, List<DriverTrip>> bucketedDriverTrips = {};
    driverProfiles.forEach((driver) {
      bucketedDriverTrips[driver.userId!] = driverTrips
          .where((trip) => trip.driverId! == driver.userId!)
          .toList();
    });

    // establish connection of all trips in regard to a passenger
    driverProfiles.forEach((driver) {
      List<TableData> driverResult = [];
      List<DriverTrip> singleDriverTrip = bucketedDriverTrips[driver.userId!]!;

      //We start the connection on base for each driver
      passengers.forEach((passenger) {
        //We collect all trips that contain the passenger
        List<DriverTrip> passengerTrips = singleDriverTrip
            .where((trip) => trip.passengers!.contains(passenger.userId))
            .toList();

        // Iterating through these, acts like result list for a single passenger
        List<TableData> tripsGraph = [];

        passengerTrips.forEach((pT) {
          //passengerTrip
          TableData pTData = TableData(
            driverTrip: pT,
            passenger: passenger,
            startTime: pT.startTime!,
            endTime: pT.endTime!,
            startLocation: pT.startLocation!,
            endLocation: pT.endLocation!,
            endAddress: pT.endAdress!,
          );
          //has previous connecting trip
          List<TableData> leftConnectorList = tripsGraph
              .where(
                (trip) =>
                    sameHourAndMinute(trip.endTime, pTData.startTime) &&
                    trip.endLocation.isSamePosition(pTData.startLocation),
              )
              .toList();
          //Connect the trips
          if (leftConnectorList.isNotEmpty) {
            TableData leftConnector = leftConnectorList.first;
            pTData.prev = leftConnector;
            leftConnector.next = pTData;
            tripsGraph.add(pTData);
          }
          //has next connecting trip
          List<TableData> rightConnectorList = tripsGraph
              .where(
                (trip) =>
                    sameHourAndMinute(trip.startTime, pTData.endTime) &&
                    trip.startLocation.isSamePosition(pTData.endLocation),
              )
              .toList();
          //Connect the trips
          if (rightConnectorList.isNotEmpty) {
            TableData rightConnector = rightConnectorList.first;
            pTData.next = rightConnector;
            rightConnector.prev = pTData;
          }
          tripsGraph.add(pTData);
        });

        driverResult.addAll(tripsGraph);
      });
      // Reduce preResults to only a single trip per Passenger
      tableData[driver.userId!] =
          driverResult.where((element) => element.prev == null).toList();
    });
    /**
     * At this point, the preResult list should contain every trip times the passenger
     * in this trip. They should also be connected so there is a path from the
     * start of passenger journy to end of its journey
     */
    //Prep up TableData for convenience
    tableData.forEach((key, list) {
      list.forEach((data) {
        if (data.next == null) {
          return;
        }

        //Retrieve last next
        TableData lastData = data;
        for (TableData itr = data; itr.next != null; itr = itr.next!) {
          lastData = itr.next!;
        }

        //Adjust Data
        data.endAddress = lastData.endAddress;
        data.endTime = lastData.endTime;
      });
    });

    tableData.forEach(
      (key, list) => list.sort((a, b) => a.startTime.compareTo(b.startTime)),
    );
    return true;
  }

  int calculateDistanceToLeftBorder(DateTime tripStartAt) {
    if (tripStartAt.compareTo(startDate) <= 0) {
      return 0;
    } else {
      return calculateNumberOfHoursBetween(startDate, tripStartAt) - 1;
    }
  }

  int calculateRemainingWidth(
    DateTime tripStartedAt,
    DateTime tripEndedAt,
  ) {
    int projectLength =
        calculateNumberOfHoursBetween(tripStartedAt, tripEndedAt);
    if (tripStartedAt.compareTo(startDate) >= 0 &&
        tripStartedAt.compareTo(endDate) <= 0) {
      if (projectLength <= viewRange) {
        return projectLength;
      } else {
        return viewRange -
            calculateNumberOfHoursBetween(startDate, tripStartedAt);
      }
    } else if (tripStartedAt.isBefore(startDate) &&
        tripEndedAt.isBefore(startDate)) {
      return 0;
    } else if (tripStartedAt.isBefore(startDate) &&
        tripEndedAt.isBefore(endDate)) {
      return projectLength -
          calculateNumberOfHoursBetween(tripStartedAt, startDate);
    } else if (tripStartedAt.isBefore(startDate) &&
        tripEndedAt.isAfter(endDate)) {
      return viewRange;
    }
    return 0;
  }

  Widget buildHeader(double chartViewWidth, Color color) {
    List<Widget> headerItems = [];

    headerItems.add(
      Container(
        width: nameWidth,
        child: const Text(
          'NAME',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 10.0,
          ),
        ),
      ),
    );
    DateTime tempDate = startDate;

    bool leftFillUp = true;

    for (int i = 0; i < viewRange; i += leftFillUp ? 1 : 30) {
      if (tempDate.minute % 30 == 0) {
        leftFillUp = false;
      }

      headerItems.add(
        Container(
          width: leftFillUp
              ? chartViewWidth / viewRangeToFitScreen
              : chartViewWidth / viewRangeToFitScreen * 30,
          child: (tempDate.minute % 30 == 0)
              ? Text(
                  DateFormat('kk:mm').format(tempDate),
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 10.0,
                  ),
                )
              : const SizedBox(),
        ),
      );
      tempDate = tempDate.add(
        Duration(minutes: leftFillUp ? 1 : 30),
      ); //Calculation step width for blocks
    }

    return Container(
      height: 25.0,
      color: color.withAlpha(100),
      child: Row(
        children: headerItems,
      ),
    );
  }

  Widget buildGrid(double chartViewWidth) {
    List<Widget> gridColumns = [];

    DateTime tempDate = startDate;

    for (int i = 0; i <= viewRange; i++) {
      gridColumns.add(
        Container(
          decoration: BoxDecoration(
            border: Border(
              right: BorderSide(
                color: tempDate.minute == 0
                    ? Colors.grey.withAlpha(100)
                    : Colors.white.withAlpha(0),
              ),
            ),
          ),
          width: (i == 0) ? nameWidth : chartViewWidth / viewRangeToFitScreen,
          //height: 300.0,
        ),
      );
      tempDate = tempDate.add(const Duration(minutes: 1));
    }

    return Row(
      children: gridColumns,
    );
  }

  void _showDetailDialog(TableData tableData) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SizedBox(
            height: 400,
            width: 400,
            child: ListView(
              children: [
                createStaticTextInputRow(
                  AppLocalizations.of(context).eMailLabel,
                  tableData.passenger.eMail!,
                ),
                createStaticTextInputRow(
                  AppLocalizations.of(context).startLocation,
                  tableData.driverTrip.startAdress!,
                ),
                createStaticTextInputRow(
                  AppLocalizations.of(context).destination,
                  tableData.endAddress,
                ),
                createStaticTextInputRow(
                  AppLocalizations.of(context).departureLabel,
                  timeFormat.format(tableData.startTime),
                ),
                createStaticTextInputRow(
                  AppLocalizations.of(context).arrivalLabel,
                  timeFormat.format(tableData.endTime),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  List<Widget> buildChartBars(
    List<TableData> data,
    double chartViewWidth,
    Color color,
  ) {
    List<Widget> chartBars = [];

    for (int i = 0; i < data.length; i++) {
      final remainingWidth =
          calculateRemainingWidth(data[i].startTime, data[i].endTime);
      if (remainingWidth > 0) {
        chartBars.add(
          InkWell(
            onTap: () => _showDetailDialog(data[i]),
            child: Container(
              decoration: BoxDecoration(
                color: color.withAlpha(100),
                borderRadius: BorderRadius.circular(10.0),
              ),
              height: chartBarHeight,
              width: remainingWidth * chartViewWidth / viewRangeToFitScreen,
              margin: EdgeInsets.only(
                left: calculateDistanceToLeftBorder(data[i].startTime) *
                    chartViewWidth /
                    viewRangeToFitScreen,
                top: i == 0 ? 4.0 : 2.0,
                bottom: i == data.length - 1 ? 4.0 : 2.0,
              ),
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  data[i].driverTrip.endAdress!,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontSize: 10.0),
                ),
              ),
            ),
          ),
        );
      }
    }

    return chartBars;
  }

  Widget buildChartForEachUser(Profile profile, List<TableData> driverData) {
    Color color = randomColorGenerator();
    final chartBars = buildChartBars(driverData, chartViewWidth, color);
    return Container(
      height: chartBars.length * (chartBarHeight) + chartBarHeight + 4.0 > 150
          ? chartBars.length * (chartBarHeight) + chartBarHeight + 4.0
          : 150,
      child: ListView(
        physics: new ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Stack(
            fit: StackFit.loose,
            children: <Widget>[
              buildGrid(chartViewWidth),
              buildHeader(chartViewWidth, color),
              Container(
                margin: const EdgeInsets.only(top: 25.0),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: nameWidth,
                              height:
                                  chartBars.length * chartBarHeight + 4.0 > 100
                                      ? chartBars.length * chartBarHeight + 4.0
                                      : 100,
                              color: color.withAlpha(100),
                              child: Center(
                                child: RotatedBox(
                                  quarterTurns:
                                      chartBars.length * 29.0 + 4.0 > 50
                                          ? 0
                                          : 0,
                                  child: Text(
                                    profile.eMail!,
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: chartBars,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _increaseZoom() {
    if (viewRangeToFitScreen < upperBoundRangeToFitScreen) {
      setState(() {
        viewRangeToFitScreen += 200;
        isReload = true;
      });
    }
  }

  void _decreseZoom() {
    if (viewRangeToFitScreen > lowerBoundRangeToFitScreen) {
      setState(() {
        viewRangeToFitScreen -= 200;
        isReload = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: AppBar(
            title: Text(
              AppLocalizations.of(context).shiftPlannerLabel,
            ),
            backgroundColor: theme.motisPurple,
            actions: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                child: Row(
                  children: createDefaultPaddedWidgetList([
                    ElevatedButton(
                      onPressed: () => _decreseZoom(),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                      ),
                      child: const Icon(
                        Icons.text_increase,
                        color: Colors.black,
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () => _increaseZoom(),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                      ),
                      child: const Icon(
                        Icons.text_decrease,
                        color: Colors.black,
                      ),
                    )
                  ]),
                ),
              )
            ],
          ),
          drawer: createAdminDrawer(context, this),
          body: buildGantt(),
        );
      },
    );
  }

  ListView buildGantt() {
    if (tableData.isEmpty) {
      return ListView(
        padding: getDefaultPadding(),
        children: [Text(AppLocalizations.of(context).noTripViewMessage)],
      );
    } else {
      List<Widget> chart = [];
      tableData.forEach(
        (key, value) => chart.add(
          buildChartForEachUser(
            driverProfiles.firstWhere((p) => p.userId! == key),
            value,
          ),
        ),
      );

      return ListView(
        padding: getDefaultPadding(),
        children: chart,
      );
    }
  }
}
