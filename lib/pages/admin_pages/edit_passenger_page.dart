import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/passenger.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/role.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class AddEditPassengerPage extends StatefulWidget {
  const AddEditPassengerPage({super.key, this.profileUUID});

  final String? profileUUID;

  @override
  _AddEditPassengerPage createState() => _AddEditPassengerPage();
}

class _AddEditPassengerPage extends State<AddEditPassengerPage> {
  Profile? currentProfile;
  Passenger? currentPassenger;
  String? profileUUID;
  final _userNameController = TextEditingController();
  final _fullNameController = TextEditingController();
  final _eMailController = TextEditingController();
  bool disabilityFriendlyRequired = false;

  @override
  void initState() {
    super.initState();
    profileUUID = widget.profileUUID;
  }

  Future<void> _saveData() async {
    if (currentProfile == null) {
      return;
    }

    //********** Get Data ***************
    currentProfile!.userName = _userNameController.value.text;
    currentProfile!.fullName = _fullNameController.value.text;
    currentProfile!.eMail = _eMailController.value.text;

    try {
      await currentProfile!.saveToSupabase();

      context.showSnackBar(
          message: AppLocalizations.of(context).userUpdateSuccessMessage,
          backgroundColor: theme.motisBlue);
    } on PostgrestException catch (error) {
      context.showSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unknownErrorMessage);
    }
  }

  Future<bool> _getData() async {

    //********** Get User and Passenger Data ***************
    var userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, profileUUID)
        .single();

    var passengerData = await supabase
        .from(supabase_tables.passengerTableId)
        .select()
        .eq(Profile.SbVar_userId, profileUUID)
        .single();

    currentProfile = Profile.parseProfileModel(userData as Map);

    currentPassenger = Passenger.parsePassenger(passengerData as Map);

    //********** Set data for page ***************

    _userNameController.text = currentProfile!.userName ?? '';
    _fullNameController.text = currentProfile!.fullName ?? '';
    _eMailController.text = currentProfile!.eMail ?? '';
    disabilityFriendlyRequired = currentPassenger!.disabilityFriendlyRequired!;

    return true;
  }

  void _openPromoteToAdminDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          contentTextStyle: const TextStyle(color: Colors.black),
          content: SizedBox(
            height: 180,
            width: 400,
            child: ListView(
              padding: getDefaultPadding(),
              children: createDefaultPaddedWidgetList([
                Text(
                  AppLocalizations.of(context).promoteAdminConfirmMessage,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontWeight: FontWeight.w200,
                    fontSize: 25,
                  ),
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.redAccent),
                    foregroundColor: MaterialStateProperty.all(Colors.black),
                  ),
                  onPressed: () => _promoteToAdmin(),
                  child: Text(
                    AppLocalizations.of(context).promoteAdminButtonLabel,
                  ),
                )
              ]),
            ),
          ),
        );
      },
    );
  }

  void _promoteToAdmin() async {
    await supabase
        .from(supabase_tables.passengerTableId)
        .delete()
        .match({Passenger.SbVar_userId: profileUUID});

    currentProfile!.role = Role.ADMIN;

    currentProfile!.saveToSupabase();

    Navigator.pushReplacementNamed(context, '/admin/passenger');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: AppBar(
            title: Text(
              AppLocalizations.of(context).shiftPlannerLabel,
            ),
            backgroundColor: theme.motisPurple,
            actions: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.white),
                    foregroundColor: MaterialStateProperty.all(Colors.black),
                  ),
                  onPressed: () => _openPromoteToAdminDialog(),
                  child: Text(
                    AppLocalizations.of(context).promoteAdminLabel,
                  ),
                ),
              )
            ],
          ),
          drawer: createAdminDrawer(context, this),
          body: Container(
            alignment: Alignment.center,
            child: Container(
              transformAlignment: Alignment.center,
              width: 400,
              child: ListView(
                padding: getDefaultPadding(),
                children: createDefaultPaddedWidgetList([
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).nameLastnameLabel,
                    _fullNameController,
                  ),
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).userNameLabel,
                    _userNameController,
                  ),
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).eMailLabel,
                    _eMailController,
                  ),
                  Row(
                    children: [
                      Checkbox(
                        value: currentPassenger!.disabilityFriendlyRequired,
                        onChanged: (bool? value) {
                          setState(() {
                            disabilityFriendlyRequired = value!;
                            currentPassenger!.disabilityFriendlyRequired =
                                disabilityFriendlyRequired;
                            currentPassenger!.saveToSupabase();
                          });
                        },
                      ),
                      Text(
                        AppLocalizations.of(context)
                            .disabilityFriendlyTravellingLabel,
                      ),
                    ],
                  ),
                  ElevatedButton(
                    onPressed: () => _saveData(),
                    child: Text(AppLocalizations.of(context).saveChangesLabel),
                  ),
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
