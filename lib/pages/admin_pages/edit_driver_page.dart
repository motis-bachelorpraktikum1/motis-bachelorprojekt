import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/add_edit_vehicle_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/add_work_area_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/admin_driver_schedule_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/fleet.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/supabase_resources/vehicle.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

class AddEditDriverPage extends StatefulWidget {
  const AddEditDriverPage({super.key, required this.driverUUID});

  final String? driverUUID;

  @override
  _AddEditDriverPage createState() => _AddEditDriverPage();
}

class _AddEditDriverPage extends State<AddEditDriverPage> {
  Profile? _currentProfile;
  Driver? _currentDriver;
  Vehicle? _currentVehicle;
  Vehicle? _oldVehicle;
  Fleet? _currentFleet;
  List<Fleet> _fleetList = [];
  List<Vehicle> _vehicleList = [];
  List<Vehicle> _displayedVehicles = [];
  String? driverUUID;
  final _userNameController = TextEditingController();
  final _fullNameController = TextEditingController();
  final _eMailController = TextEditingController();

  bool updateVehicleListOnly = false;

  @override
  void initState() {
    super.initState();
    driverUUID = widget.driverUUID;
  }

  Future<void> _saveData() async {
    if (_currentProfile == null) {
      return;
    }

    //****************** Get Data ******************
    _currentProfile!.userName = _userNameController.value.text;
    _currentProfile!.fullName = _fullNameController.value.text;
    _currentProfile!.eMail = _eMailController.value.text;
    _currentDriver!.registered = true;

    //********** Save Data ***************
    try {
      await _currentProfile!.saveToSupabase();
      await _currentDriver!.saveToSupabase();

      //Handle Vehicle changes
      if (_currentVehicle != null) {
        if (_oldVehicle!.vehicleId != _currentVehicle!.vehicleId &&
            _oldVehicle!.vehicleId != '') {
          _oldVehicle!.saveToSupabase(context);
        }

        if (_currentVehicle!.vehicleId != '') {
          await _currentVehicle!.saveToSupabase(context);
        }
      }

      context.showSnackBar(
          message: AppLocalizations.of(context).driverUpdateSuccessMessage,
          backgroundColor: theme.motisBlue);
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.details.toString());
    } catch (error) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unknownErrorMessage);
    }
  }

  Future<bool> _getData() async {
    if (updateVehicleListOnly) {
      updateVehicleList();
      updateVehicleListOnly = false;
      return true;
    }

    //****************** Get Vehicles ******************
    final vehicleData =
        await supabase.from(supabase_tables.vehicleTableId).select();

    _vehicleList = (vehicleData as List<dynamic>)
        .map((e) => Vehicle.parseVehicleModel(e as Map))
        .toList();

    Vehicle placeHolderVehicle = Vehicle();
    placeHolderVehicle.vehicleId = '';
    placeHolderVehicle.licensePlate = '/';
    placeHolderVehicle.vehicleName = '/';
    _vehicleList.add(placeHolderVehicle);

    if (driverUUID!.isEmpty) {
      return true;
    }
    //********** Get Profile Data ***************
    final profileData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, driverUUID)
        .single();

    _currentProfile = Profile.parseProfileModel(profileData as Map);
    _userNameController.text = _currentProfile!.userName ?? '';
    _fullNameController.text = _currentProfile!.fullName ?? '';
    _eMailController.text = _currentProfile!.eMail ?? '';

    //********** Get Driver Data ***************
    final driverData = await supabase
        .from(supabase_tables.driverTableId)
        .select()
        .eq(Driver.SbVar_userId, driverUUID)
        .single();

    _currentDriver = Driver.parseDriverModel(driverData as Map);

    //********** Get Fleet Data ***************
    final fleetData =
        await supabase.from(supabase_tables.fleetTableId).select();

    _fleetList = (fleetData as List<dynamic>)
        .map((e) => Fleet.parseFleetMap(e as Map))
        .toList();

    Fleet noFleetPlaceHolder = Fleet();
    noFleetPlaceHolder.fleetId = '';
    noFleetPlaceHolder.fleetName =
        AppLocalizations.of(context).independentLabel;

    _fleetList.add(noFleetPlaceHolder);

    _currentFleet = _fleetList
        .firstWhere((element) => element.fleetId == _currentDriver!.fleetId!);

    //********** prepare data for page ***************
    updateVehicleList();

    _currentVehicle = _vehicleList.firstWhere(
      (element) => element.vehicleId == _currentDriver!.currentVehicleId,
    );
    _oldVehicle = _currentVehicle;

    return true;
  }

  Future<void> updateVehicleList() async {
    _displayedVehicles = _vehicleList
        .where(
          (element) =>
              (element.fleetId == _currentFleet!.fleetId &&
                  (element.driverID!.isEmpty ||
                      element.driverID! == driverUUID)) ||
              element.vehicleId!.isEmpty,
        )
        .toList();

    if (_currentVehicle!.vehicleId!.isNotEmpty &&
        _currentFleet!.fleetId! != _currentVehicle!.fleetId) {
      _currentVehicle =
          _vehicleList.firstWhere((element) => element.vehicleId!.isEmpty);
    }
  }

  Widget createFilledVehicleInfoCard() {
    if (_currentVehicle == null || _currentVehicle!.vehicleId == '') {
      return createInfoCard(150, [
        createStaticTextInputRow(
          AppLocalizations.of(context).vehicleLabel,
          AppLocalizations.of(context).noVehicleAssignedLabel,
        )
      ]);
    } else {
      return createInfoCard(
        300,
        [
          Row(
            children: [
              createStaticTextInputRow(
                AppLocalizations.of(context).vehicleLicensePlateLabel,
                _currentVehicle!.licensePlate!,
              ),
              createStaticTextInputRow(
                AppLocalizations.of(context).nameLabel,
                _currentVehicle!.vehicleName!,
              )
            ],
          ),
          Row(
            children: [
              createStaticTextInputRow(
                AppLocalizations.of(context).numberOfSeatsLabel,
                _currentVehicle!.numberOfSeats.toString(),
              ),
              createStaticTextInputRow(
                AppLocalizations.of(context)
                    .numberOfDisabilityFriendlySeatsLabel,
                _currentVehicle!.numberOfDisabilityFriendlySeats!.toString(),
              ),
            ],
          ),
          createStaticTextInputRow(
            AppLocalizations.of(context).fleetLabel,
            _currentVehicle!.fleetId!.isEmpty
                ? AppLocalizations.of(context).notAvailableLabel
                : _currentFleet!.fleetName!,
          ),
          Padding(padding: getDefaultPadding()),
          ElevatedButton(
            onPressed: () => _editVehicle(_currentVehicle!.vehicleId!),
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: theme.motisPurple,
            ),
            child: Icon(
              Icons.edit_outlined,
              size: 24,
              semanticLabel: AppLocalizations.of(context).editVehicleLabel,
            ),
          ),
        ],
      );
    }
  }

  void _editVehicle(String entityId) {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/vehicle/edit'),
        builder: (_) => AddEditVehiclePage(
          vehicleUUID: entityId,
        ),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  void clearOldVehicle() {
    _currentVehicle!.driverID = '';
    _currentVehicle!.saveToSupabase(context);
  }

  void _chooseArea() {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/driver/area'),
        builder: (_) => ChooseWorkAreaPage(
          driver: _currentDriver!,
        ),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  void _shiftPlanner() {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/driver/shift'),
        builder: (_) => AdminDriverSchedulePage(
          profileUUID: _currentDriver!.userId!,
        ),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: createAdminAppBar(
            AppLocalizations.of(context).editDriverLabel,
          ),
          drawer: createAdminDrawer(context, this),
          body: Container(
            alignment: Alignment.center,
            child: Container(
              transformAlignment: Alignment.center,
              width: 800,
              child: ListView(
                padding: getDefaultPadding(),
                children: createDefaultPaddedWidgetList([
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).nameLastnameLabel,
                    _fullNameController,
                  ),
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).userNameLabel,
                    _userNameController,
                  ),
                  createLabeledTextInputRow(
                    AppLocalizations.of(context).eMailLabel,
                    _eMailController,
                  ),
                  ElevatedButton(
                    onPressed: () => _chooseArea(),
                    child: Text(AppLocalizations.of(context).chooseAreaString),
                  ),
                  ElevatedButton(
                    onPressed: () => _shiftPlanner(),
                    child: Text(AppLocalizations.of(context).shiftPlannerLabel),
                  ),
                  const Divider(
                    color: Colors.black,
                  ),
                  DropdownSearch<Fleet>(
                    popupProps: const PopupProps.menu(
                      showSelectedItems: true,
                    ),
                    items: _fleetList,
                    itemAsString: (Fleet u) => u.fleetName!,
                    dropdownDecoratorProps: DropDownDecoratorProps(
                      dropdownSearchDecoration: InputDecoration(
                        labelText: AppLocalizations.of(context).fleetLabel,
                      ),
                    ),
                    selectedItem: _currentFleet,
                    onChanged: (value) {
                      _currentFleet = value;
                      _currentDriver!.fleetId = value!.fleetId;
                      setState(() {
                        updateVehicleListOnly = true;
                      });
                    },
                    compareFn: (item1, item2) => item1.fleetId == item2.fleetId,
                  ),
                  DropdownSearch<Vehicle>(
                    popupProps: const PopupProps.menu(
                      showSelectedItems: true,
                    ),
                    items: _displayedVehicles,
                    itemAsString: (Vehicle u) => u.vehicleId!.isNotEmpty
                        ? '${u.licensePlate!} | ${u.vehicleName!}'
                        : '/',
                    dropdownDecoratorProps: DropDownDecoratorProps(
                      dropdownSearchDecoration: InputDecoration(
                        labelText: AppLocalizations.of(context).vehicleLabel,
                      ),
                    ),
                    selectedItem: _currentVehicle,
                    onChanged: (value) {
                      _oldVehicle!.driverID = '';
                      _currentVehicle = value;
                      _currentVehicle!.driverID = driverUUID;
                      _currentDriver!.currentVehicleId = value!.vehicleId;
                      setState(() {
                        updateVehicleListOnly = true;
                      });
                    },
                    compareFn: (item1, item2) => item1.fleetId == item2.fleetId,
                  ),
                  Text(
                    AppLocalizations.of(context).vehicleLabel,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  createFilledVehicleInfoCard(),
                  ElevatedButton(
                    onPressed: () => _saveData(),
                    child: Text(AppLocalizations.of(context).saveChangesLabel),
                  ),
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
