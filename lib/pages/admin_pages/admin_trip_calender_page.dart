import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/pages/admin_pages/admin_all_driver_trip_view.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/shift_schedule.dart';
import 'package:supabase_quickstart/theme.dart' as theme;
import 'package:table_calendar/table_calendar.dart';

class AdminTripCalendarPage extends StatefulWidget {
  const AdminTripCalendarPage({super.key});

  @override
  _AdminTripCalendarPage createState() => _AdminTripCalendarPage();
}

class _AdminTripCalendarPage extends State<AdminTripCalendarPage> {
  bool isReload = false;
  DateTime _selectedDate = DateTime.now();
  DateTime _focusedDate = DateTime.now();
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateFormat dateFormatDate = DateFormat('dd.MM.yyyy');
  DateFormat dateFormatTimeOnly = DateFormat('kk:mm');

  @override
  void initState() {
    super.initState();
  }

  void _openTripViewPage() {
    Navigator.of(context).push(
      MaterialPageRoute(
        settings: const RouteSettings(
          name: '/admin/driver/trip-view',
        ),
        builder: (_) => AdminAllDriverTripView(
          selectedTime: _selectedDate,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              AppLocalizations.of(context).shiftPlannerLabel,
            ),
            backgroundColor: theme.motisPurple,
          ),
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: ListView(
              children: createDefaultPaddedWidgetList([
                TableCalendar(
                  focusedDay: _focusedDate,
                  firstDay: DateTime.now().subtract(
                    const Duration(
                      days: 60,
                    ),
                  ),
                  lastDay: DateTime.now().add(
                    const Duration(days: 90),
                  ),
                  selectedDayPredicate: (day) {
                    return isSameDay(_selectedDate, day);
                  },
                  onDaySelected: (selectedDay, focusedDay) {
                    setState(() {
                      _selectedDate = selectedDay;
                      _focusedDate = focusedDay;
                      isReload = true;
                    });
                  },
                  calendarFormat: _calendarFormat,
                  onFormatChanged: (format) {
                    setState(() {
                      _calendarFormat = format;
                      isReload = true;
                    });
                  },
                  onPageChanged: (focusedDay) {
                    _focusedDate = focusedDay;
                  },
                ),
                ElevatedButton(
                  onPressed: () => _openTripViewPage(),
                  child: Text(AppLocalizations.of(context).adminTripViewTitle),
                ),
              ]),
            ),
          ),
        );
      },
    );
  }
}
