import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';

class AdminHomePage extends StatefulWidget {
  const AdminHomePage({super.key});

  @override
  _AdminHomePage createState() => _AdminHomePage();
}

class _AdminHomePage extends MotisState<AdminHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: createAdminAppBar(AppLocalizations.of(context).motisAdminTitle),
      body: Container(
        alignment: Alignment.center,
        child: Container(
          transformAlignment: Alignment.center,
          width: 400,
          child: ListView(
            padding: getDefaultPadding(),
            children: createDefaultPaddedWidgetList([
              ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, '/admin/passenger'),
                child: Text(AppLocalizations.of(context).manageUsersLabel),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, '/admin/drivers'),
                child: Text(AppLocalizations.of(context).manageDriversLabel),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, '/admin/vehicle'),
                child: Text(AppLocalizations.of(context).manageVehiclesLabel),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, '/admin/fleet'),
                child: Text(AppLocalizations.of(context).manageFleetsLabel),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, '/admin/trips-today'),
                child: Text(AppLocalizations.of(context).adminTripViewTitle),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, '/admin/driver-applications'),
                child: Text(AppLocalizations.of(context).seeDriverApplicationsLabel),
              ),
              createLogoutButton()
            ]),
          ),
        ),
      ),
    );
  }
}
