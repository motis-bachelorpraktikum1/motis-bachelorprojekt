import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/fleet_vehicle_table_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_table.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/fleet.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/supabase_resources/vehicle.dart';
import 'package:supabase_quickstart/theme.dart' as theme;
import 'package:uuid/uuid.dart';

class FleetTablePage extends StatefulWidget {
  const FleetTablePage({super.key});

  @override
  _FleetTablePage createState() => _FleetTablePage();
}

class _FleetTablePage extends MotisState<FleetTablePage> {
  _FleetTablePage();

  List<TableRow>? _tableRows;
  bool isFiltering = false;

  final _searchController = TextEditingController();
  final _fleetNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  List<DataColumn> _getTableHeader() {
    return createColumnsForColumnNames([
      AppLocalizations.of(context).nameLabel,
      AppLocalizations.of(context).numberOfVehiclesLabel,
      AppLocalizations.of(context).editVehicleLabel
    ]);
  }

  Future<bool> _getData() async {
    if (isFiltering) {
      isFiltering = false;
      return true;
    }
    //********** Get Fleet and Vehicle data ***************
    final fleetData =
        await supabase.from(supabase_tables.fleetTableId).select();
    final vehiclesData =
        await supabase.from(supabase_tables.vehicleTableId).select();

    List<Vehicle> vehicles = (vehiclesData as List<dynamic>)
        .map((e) => Vehicle.parseVehicleModel(e as Map))
        .toList();

    _tableRows = (fleetData as List<dynamic>)
        .map((e) => Fleet.parseFleetMap(e as Map))
        .map((e) => _parseToTableRow(e, vehicles))
        .toList();

    return true;
  }

  TableRow _parseToTableRow(Fleet fleet, List<Vehicle> vehicles) {
    return TableRow(
      fleet.fleetId!,
      fleet.fleetName!,
      vehicles
          .where((element) => element.fleetId == fleet.fleetId)
          .length
          .toString(),
    );
  }

  List<DataRow> _createDataRows() {
    if (_tableRows == null) {
      return [];
    } else {
      return (_tableRows!)
          .where(
            (element) => element.name
                .toLowerCase()
                .contains(_searchController.text.toLowerCase()),
          )
          .map(
            (e) => DataRow(
              cells: [
                createTextDataCell(e.name),
                createTextDataCell(e.numberOfCars),
                DataCell(
                  ElevatedButton(
                    onPressed: () => _editDataPoint(e.fleetId, e.name),
                    style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: theme.motisPurple,
                    ),
                    child: Icon(
                      Icons.edit_outlined,
                      size: 24,
                      semanticLabel:
                          AppLocalizations.of(context).editFleetLabel,
                    ),
                  ),
                ),
              ],
            ),
          )
          .toList();
    }
  }

  void _editDataPoint(String fleetID, String fleetName) {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/fleet/edit'),
        builder: (_) =>
            FleetVehicleTablePage(fleetUUID: fleetID, fleetName: fleetName),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  void saveNewFleet() {
    String uuid = const Uuid().v4();
    Fleet newFleet = Fleet();
    newFleet.fleetId = uuid;
    newFleet.fleetName = _fleetNameController.text;

    try {
      newFleet.saveToSupabase(context);

      context.showSnackBar(
        message: AppLocalizations.of(context).fleetUpdateSuccessMessage,
        backgroundColor: theme.motisBlue,
      );
    } on PostgrestException catch (error) {
      context.showSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unknownErrorMessage);
    }
    setState(() {
      isFiltering = false;
    });
    Navigator.pop(context);
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        } else {
          return Scaffold(
            appBar: createAdminAppBar(
              AppLocalizations.of(context).manageFleetsLabel,
            ),
            drawer: createAdminDrawer(context, this),
            body: Padding(
              padding: getDefaultPadding(),
              child: Column(
                children: createDefaultPaddedWidgetList([
                  Row(
                    children: createDefaultPaddedWidgetList([
                      ElevatedButton(
                        onPressed: () => showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content: SizedBox(
                                height: 180,
                                width: 400,
                                child: ListView(
                                  padding: getDefaultPadding(),
                                  children: createDefaultPaddedWidgetList([
                                    Text(
                                      AppLocalizations.of(context)
                                          .createFleetLabel,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 25,
                                      ),
                                    ),
                                    createLabeledTextInputRow(
                                      AppLocalizations.of(context).nameLabel,
                                      _fleetNameController,
                                    ),
                                    ElevatedButton(
                                      onPressed: () => saveNewFleet(),
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .saveChangesLabel,
                                      ),
                                    )
                                  ]),
                                ),
                              ),
                            );
                          },
                        ),
                        child:
                            Text(AppLocalizations.of(context).createFleetLabel),
                      ),
                      Flexible(
                        child: TextFormField(
                          controller: _searchController,
                          textAlign: TextAlign.left,
                          decoration: InputDecoration(
                            hintText: AppLocalizations.of(context).searchLabel,
                          ),
                          onChanged: (search) => setState(() {
                            isFiltering = true;
                          }),
                        ),
                      ),
                    ]),
                  ),
                  Flexible(
                    child: MotisTable(
                      columns: _getTableHeader(),
                      rows: _createDataRows(),
                    ),
                  )
                ]),
              ),
            ),
          );
        }
      },
    );
  }
}

class TableRow {
  String fleetId;
  String name;
  String numberOfCars;

  TableRow(this.fleetId, this.name, this.numberOfCars);
}
