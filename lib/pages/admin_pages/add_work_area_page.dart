// ignore_for_file: prefer_interpolation_to_compose_strings, use_string_buffers

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:positioned_tap_detector_2/positioned_tap_detector_2.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/supabase_resources/area_of_operation.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';

class ChooseWorkAreaPage extends StatefulWidget {
  ChooseWorkAreaPage({super.key, required this.driver});

  final Driver driver;

  @override
  State<ChooseWorkAreaPage> createState() => _ChooseWorkAreaPageState();
}

class _ChooseWorkAreaPageState extends State<ChooseWorkAreaPage> {
  late Driver _currentDriver;
  late List<LatLng> tappedPoints;
  String polygonWKT = 'POLYGON((';
  late String polygon;
  bool areaSet = false;
  MapController? _mapController;

  @override
  void initState() {
    _currentDriver = widget.driver;
    tappedPoints = _currentDriver.areaOfOperationPolygons == null
        ? []
        : _currentDriver.areaOfOperationPolygons!.coordinates.coordinateList;
    polygon = "";

    super.initState();
  }

  @override
  void didChangeDependencies() {
    _mapController = MapController();

    if (tappedPoints.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await _setCenterAndZoomDefault();
      });
    } else {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _setCenterAndZoomOnPolygon();
      });
    }
    super.didChangeDependencies();
  }

  ///Set default position on Darmstadt, Hesse
  Future<void> _setCenterAndZoomDefault() async {
    final bounds = LatLngBounds.fromPoints(
      [LatLng(49.878708, 8.646927)],
    );
    final CenterZoom cz = _mapController!.centerZoomFitBounds(bounds);
    _mapController!.move(cz.center, cz.zoom - 0.25);
  }

  void _setCenterAndZoomOnPolygon() {
    final bounds = LatLngBounds.fromPoints(tappedPoints);
    final CenterZoom cz = _mapController!.centerZoomFitBounds(bounds);
    _mapController!.move(cz.center, cz.zoom - 0.25);
  }

  @override
  void dispose() {
    _mapController!.dispose();
    super.dispose();
  }

  Future<bool> setupPage() async {
    polygon = "";
    return true;
  }

  String _convertToWKT() {
    polygon += polygonWKT;
    for (int i = 0; i < tappedPoints.length; i++) {
      polygon += tappedPoints[i].longitude.toString() +
          ' ' +
          tappedPoints[i].latitude.toString() +
          ', ';
    }

    return polygon += tappedPoints[0].longitude.toString() +
        ' ' +
        tappedPoints[0].latitude.toString() +
        '))';
  }

  void _handleTap(TapPosition tapPosition, LatLng latlng) {
    setState(() {
      tappedPoints.add(latlng);
    });
  }

  void _saveArea() {
    _convertToWKT();
    _currentDriver.areaOfOperationPolygons =
        AreaOfOperation(coordinates: Coordinates(coordinateList: tappedPoints));
    _currentDriver.polygonWKT = polygon;
    _currentDriver.saveToSupabase();
    if (mounted) {
      context.showSnackBar(
        message: AppLocalizations.of(context).areaSavedMessage,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Marker> markers = tappedPoints.map((latlng) {
      return Marker(
        width: 40,
        height: 40,
        point: latlng,
        builder: (_) => Icon(
          Icons.my_location_outlined,
          color: Colors.red.shade900,
        ),
      );
    }).toList();

    void _deleteArea() {
      _currentDriver.areaOfOperationPolygons = AreaOfOperation(
          coordinates: Coordinates(coordinateList: tappedPoints));
      _currentDriver.polygonWKT = polygon;
      _currentDriver.deleteAreaOfOperation();
      if (mounted) {
        context.showSnackBar(
          message: AppLocalizations.of(context).areaDeletedMessage,
        );
      }
    }

    void _deletePolygon() {
      setState(() {
        polygon = "";
        tappedPoints = [];
        markers = [];
      });
      _deleteArea();
    }

    AlertDialog getAlert() {
      return AlertDialog(
        title: Text(AppLocalizations.of(context).alarm),
        content: Text(AppLocalizations.of(context).deleteAreaString),
        actions: <Widget>[
          TextButton(
            child: Text(
              AppLocalizations.of(context).cancel,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text(
              AppLocalizations.of(context).accept,
            ),
            onPressed: () {
              Navigator.of(context).pop();
              setState(() {
                _deletePolygon();
              });
            },
          ),
        ],
      );
    }

    return Scaffold(
      appBar:
          AppBar(title: Text(AppLocalizations.of(context).chooseAreaString)),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 8, bottom: 8),
              child: Text(AppLocalizations.of(context).clickToAddAreaString),
            ),
            Flexible(
              child: FlutterMap(
                mapController: _mapController,
                options: MapOptions(
                  onTap: _handleTap,
                ),
                children: [
                  TileLayer(
                    urlTemplate:
                        'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                    userAgentPackageName: 'dev.fleaflet.flutter_map.example',
                  ),
                  MarkerLayer(markers: markers),
                  PolygonLayer(
                    polygons: [
                      Polygon(
                        points: tappedPoints,
                        borderColor: Colors.red.shade900,
                        color: Colors.red.shade900,
                        borderStrokeWidth: 4.0,
                      ),
                    ],
                  )
                ],
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: _saveArea,
                    child: Text(AppLocalizations.of(context).saveChangesLabel)),
                ElevatedButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return getAlert();
                        },
                      );
                    },
                    child:
                        Text(AppLocalizations.of(context).deleteButtonLabel)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
