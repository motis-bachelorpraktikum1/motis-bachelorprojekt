import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/shift_schedule.dart';
import 'package:time_range_picker/time_range_picker.dart';
import 'package:uuid/uuid.dart';

class AdminDriverAddSchedulePage extends StatefulWidget {
  AdminDriverAddSchedulePage({
    super.key,
    required this.profileUUID,
    required this.selectedDate,
    required this.shifts,
  });

  String profileUUID;
  DateTime selectedDate;
  List<ShiftSchedule> shifts;

  @override
  _AdminDriverAddSchedulePage createState() => _AdminDriverAddSchedulePage(
      profileUUID: profileUUID,
      selectedStartDate: selectedDate,
      shifts: shifts);
}

class _AdminDriverAddSchedulePage extends State<AdminDriverAddSchedulePage> {
  _AdminDriverAddSchedulePage({
    required this.profileUUID,
    required this.selectedStartDate,
    required this.shifts,
  });

  String profileUUID;
  DateTime selectedStartDate;
  DateTime selectedEndDate = DateTime.now().add(const Duration(hours: 4));
  TimeRange selectedTime = TimeRange(
    startTime: TimeOfDay.now(),
    endTime: TimeOfDay(
      hour: TimeOfDay.now().hour + 4,
      minute: TimeOfDay.now().minute,
    ),
  );
  List<ShiftSchedule> shifts;

  final TextEditingController _dateInput = TextEditingController();
  final TextEditingController _timeInput = TextEditingController();

  OutlineInputBorder textFieldBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
  );

  @override
  void initState() {
    super.initState();
    _dateInput.text = DateFormat('dd.MM.yyyy').format(selectedStartDate);
  }

  Future<void> _selectDate() async {
    final DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: selectedStartDate,
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(const Duration(days: 90)),
      helpText: AppLocalizations.of(context).whichDateLabel,
      cancelText: AppLocalizations.of(context).cancel,
    );
    if (newDate != null) {
      setState(() {
        selectedStartDate = newDate;
      });
      _dateInput.text = DateFormat('dd.MM.yyyy').format(selectedStartDate);
    }
  }

  Future<void> _selectTime() async {
    TimeRange newTimeRange = await showTimeRangePicker(
      context: context,
      maxDuration: const Duration(hours: 12),
    ) as TimeRange;
    if (newTimeRange != null) {
      setState(() {
        selectedTime = newTimeRange;
      });
      selectedStartDate = DateTime(
        selectedStartDate.year,
        selectedStartDate.month,
        selectedStartDate.day,
        selectedTime.startTime.hour,
        selectedTime.startTime.minute,
      );

      selectedEndDate = DateTime(
        selectedStartDate.year,
        selectedStartDate.month,
        selectedStartDate.day,
        selectedTime.endTime.hour,
        selectedTime.endTime.minute,
      );

      if (selectedStartDate.isAfter(selectedEndDate)) {
        selectedEndDate = selectedEndDate.add(const Duration(days: 1));
      }

      _timeInput.text =
          '${selectedTime.startTime.format(context)} - ${selectedTime.endTime.format(context)}';
    }
    if (selectedStartDate.isBefore(DateTime.now())) {
      _timeInput.clear();
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).noShiftInPast);
    }
  }

  Future<void> _saveShift() async {
    if (_timeInput.value.text.isEmpty || _dateInput.value.text.isEmpty) {
      return;
    }

    //****************** Build shift ******************
    ShiftSchedule shift = ShiftSchedule();
    shift.shiftId = Uuid().v4();
    shift.driverId = profileUUID;
    shift.startTime = selectedStartDate;
    shift.endTime = selectedEndDate;

    if (checkOverlap(shifts, shift)) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).shiftOverlapError);
      return;
    }

    //****************** Save ******************
    try {
      shift.saveToSupabase();
      context.showSnackBar(
          message: AppLocalizations.of(context).shiftCreateSuccess);
      Navigator.pop(context, shift);
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }
  }

  bool checkOverlap(List<ShiftSchedule> shifts, ShiftSchedule newShift) {
    //Constant to avoid duplicates of a shift
    Duration oneSecond = const Duration(seconds: 1);

    return shifts.any(
      (shift) =>
          //Case 1: startTime in between an existing shift
          (shift.startTime!.isBefore(newShift.startTime!.add(oneSecond)) &&
              shift.endTime!
                  .isAfter(newShift.startTime!.subtract(oneSecond))) ||
          //Case 2: endTime in between an existing shift
          (shift.startTime!.isBefore(newShift.endTime!.add(oneSecond)) &&
              shift.endTime!.isAfter(newShift.endTime!.subtract(oneSecond))) ||
          //Case 3: new shift start before old shift and ends after old shift
          (newShift.startTime!.isBefore(shift.startTime!) &&
              newShift.endTime!.isAfter(shift.endTime!)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: createAdminAppBar(
        AppLocalizations.of(context).chooseDateLabel,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: createDefaultPaddedWidgetList([
            TextField(
              controller: _dateInput,
              decoration: InputDecoration(
                filled: true,
                icon: const Icon(Icons.calendar_today),
                hintText: AppLocalizations.of(context).chooseDateLabel,
                border: textFieldBorder,
              ),
              readOnly: true,
              onTap: () => _selectDate(),
            ),
            Divider(height: 5, color: Colors.blueGrey.shade600),
            TextField(
              controller: _timeInput,
              decoration: InputDecoration(
                filled: true,
                icon: const Icon(Icons.access_time_filled),
                hintText: AppLocalizations.of(context).shiftDurationLabel,
                border: textFieldBorder,
              ),
              readOnly: true,
              onTap: () => _selectTime(),
            ),
            ElevatedButton(
              onPressed: _saveShift,
              child: Text(
                AppLocalizations.of(context).saveChangesLabel,
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
