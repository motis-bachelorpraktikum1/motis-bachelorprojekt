import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/add_edit_vehicle_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_table.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/fleet.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/supabase_resources/vehicle.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

class FleetVehicleTablePage extends StatefulWidget {
  const FleetVehicleTablePage({
    super.key,
    required this.fleetUUID,
    required this.fleetName,
  });

  final String? fleetUUID;
  final String? fleetName;

  @override
  _FleetVehicleTablePage createState() => _FleetVehicleTablePage();
}

class _FleetVehicleTablePage extends State<FleetVehicleTablePage> {
  List<TableRow>? _tableRows;
  String? fleetUUID;
  String? fleetName;
  late Future<bool> getDataFuture;

  final _searchController = TextEditingController();
  final _fleetNameController = TextEditingController();

  bool _isFiltering = false;

  @override
  void initState() {
    super.initState();
    fleetUUID = widget.fleetUUID;
    fleetName = widget.fleetName;
    getDataFuture = _getData();
    setState(() {});
  }

  List<DataColumn> _getTableHeader() {
    return createColumnsForColumnNames([
      AppLocalizations.of(context).vehicleLicensePlateLabel,
      AppLocalizations.of(context).nameLabel,
      AppLocalizations.of(context).numberOfSeatsLabel,
      AppLocalizations.of(context).numberOfDisabilityFriendlySeatsLabel,
      AppLocalizations.of(context).driverLabel,
      AppLocalizations.of(context).editLabel
    ]);
  }

  Future<bool> _getData() async {
    if (_isFiltering) {
      _isFiltering = false;
      return true;
    }

    //********** Get Vehilce Data ***************
    final vehicleData = await supabase
        .from(supabase_tables.vehicleTableId)
        .select()
        .eq(Vehicle.SbVar_fleetId, fleetUUID);

    //********** Get Profiles of drivers ***************
    final profileData =
        await supabase.from(supabase_tables.profilesTableId).select();

    List<Vehicle> vehicleList = (vehicleData as List<dynamic>)
        .map((e) => Vehicle.parseVehicleModel(e as Map))
        .toList();

    List<Profile> profileList = (profileData as List<dynamic>)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();

    _tableRows =
        vehicleList.map((e) => _parseToTableRow(e, profileList)).toList();

    _fleetNameController.text = fleetName!;

    return true;
  }

  TableRow _parseToTableRow(Vehicle vehicle, List<Profile> drivers) {
    TableRow tableRow = TableRow(
      vehicle.vehicleId!,
      vehicle.licensePlate!,
      vehicle.vehicleName!,
      vehicle.numberOfSeats!.toString(),
      vehicle.numberOfDisabilityFriendlySeats!.toString(),
    );

    if (vehicle.driverID!.isNotEmpty) {
      Profile driverForVehicle =
          drivers.firstWhere((element) => element.userId == vehicle.driverID!);

      tableRow.driverMail = driverForVehicle.eMail;
    }

    return tableRow;
  }

  List<DataRow> _createDataRows() {
    if (_tableRows == null) {
      return [];
    }
    return (_tableRows!)
        .where(
          (element) => anyContains(
            [element.name, element.licensePlate],
            _searchController.text,
          ),
        )
        .map(
          (e) => DataRow(
            cells: [
              createTextDataCell(e.licensePlate),
              createTextDataCell(e.name),
              createTextDataCell(e.numberOfSeatsString),
              createTextDataCell(e.numberOfDisabilityFriendlySeatsString),
              createTextDataCell(e.driverMail ?? ''),
              DataCell(
                ElevatedButton(
                  onPressed: () => _editDataPoint(e.vehicleId),
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: theme.motisPurple,
                  ),
                  child: Icon(
                    Icons.edit_outlined,
                    size: 24,
                    semanticLabel:
                        AppLocalizations.of(context).editVehicleLabel,
                  ),
                ),
              ),
            ],
          ),
        )
        .toList();
  }

  void _editDataPoint(String entityId) {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/vehicle/edit'),
        builder: (_) => AddEditVehiclePage(
          vehicleUUID: entityId,
        ),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  void saveFleetName() async {
    fleetName = _fleetNameController.text;
    try {
      await supabase.from(supabase_tables.fleetTableId).update({
        Fleet.SbVar_fleetId: fleetUUID,
        Fleet.SbVar_fleetName: fleetName
      }).eq(
        Fleet.SbVar_fleetId,
        fleetUUID,
      );
      context.showSnackBar(
          message: AppLocalizations.of(context).fleetUpdateSuccessMessage,
          backgroundColor: theme.motisBlue);
    } on PostgrestException catch (error) {
      context.showSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unknownErrorMessage,
      );
    }
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getDataFuture,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar:
              createAdminAppBar(AppLocalizations.of(context).manageFleetsLabel),
          drawer: createAdminDrawer(context, this),
          body: Padding(
            padding: getDefaultPadding(),
            child: Column(
              children: createDefaultPaddedWidgetList([
                SizedBox(
                  width: 400,
                  child: Row(
                    children: [
                      createLabeledTextInputRow(
                        AppLocalizations.of(context).nameLabel,
                        _fleetNameController,
                      ),
                      ElevatedButton(
                        onPressed: () => saveFleetName(),
                        child:
                            Text(AppLocalizations.of(context).saveChangesLabel),
                      ),
                    ],
                  ),
                ),
                const Divider(
                  color: Colors.black,
                ),
                Flexible(
                  child: TextFormField(
                    controller: _searchController,
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      hintText: AppLocalizations.of(context).searchLabel,
                    ),
                    onChanged: (search) => setState(() {
                      _isFiltering = true;
                    }),
                  ),
                ),
                MotisTable(
                  columns: _getTableHeader(),
                  rows: _createDataRows(),
                ),
              ]),
            ),
          ),
        );
      },
    );
  }
}

class TableRow {
  String vehicleId;
  String licensePlate;
  String name;
  String numberOfSeatsString;
  String numberOfDisabilityFriendlySeatsString;
  String? driverMail;

  TableRow(
    this.vehicleId,
    this.licensePlate,
    this.name,
    this.numberOfSeatsString,
    this.numberOfDisabilityFriendlySeatsString,
  );
}
