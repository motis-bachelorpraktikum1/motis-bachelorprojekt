import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/fleet.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as Supabase_tables;
import 'package:supabase_quickstart/supabase_resources/vehicle.dart';
import 'package:supabase_quickstart/theme.dart' as theme;
import 'package:uuid/uuid.dart';

class AddEditVehiclePage extends StatefulWidget {
  AddEditVehiclePage({super.key, required this.vehicleUUID});

  final String vehicleUUID;

  @override
  _AddEditVehiclePage createState() => _AddEditVehiclePage();
}

class _AddEditVehiclePage extends State<AddEditVehiclePage> {
  Vehicle? _currentVehicle;
  Fleet? _currentFleet;
  List<Fleet> _fleetList = [];
  String? vehicleUUID;

  final _vehicleNameController = TextEditingController();
  final _licensePlateController = TextEditingController();
  final _numberOfSeatsController = TextEditingController();
  final _numberOfDisabilityFriendlySeats = TextEditingController();

  @override
  void initState() {
    super.initState();
    vehicleUUID = widget.vehicleUUID;
  }

  Future<void> _saveData() async {
    //********** if this is a newly created vehicle **********
    if (vehicleUUID!.isEmpty) {
      vehicleUUID = const Uuid().v4();
      _currentVehicle!.vehicleId = vehicleUUID;
    }

    //********** get Data **********
    _currentVehicle!.vehicleName = _vehicleNameController.text;
    _currentVehicle!.licensePlate = _licensePlateController.text;
    _currentVehicle!.numberOfSeats = int.parse(_numberOfSeatsController.text);
    _currentVehicle!.numberOfDisabilityFriendlySeats =
        int.parse(_numberOfDisabilityFriendlySeats.text);

    //********** Save **********
    try {
      _currentVehicle!.saveToSupabase(context);
      context.showSnackBar(
          message: AppLocalizations.of(context).vehicleUpdateSuccessMessage,
          backgroundColor: theme.motisBlue);
    } on PostgrestException catch (error) {
      context.showSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unknownErrorMessage);
    }
  }

  Future<bool> _getData() async {
    //********** Get List of Fleets ***************
    var fleetData = await supabase.from(Supabase_tables.fleetTableId).select();

    _fleetList = (fleetData as List<dynamic>)
        .map((e) => Fleet.parseFleetMap(e as Map))
        .toList();

    // if this is a vehicle-create form
    if (vehicleUUID!.isEmpty) {
      _currentVehicle = Vehicle();
      return true;
    }
    //********** Get Vehicle Data ***************
    var vehicleData = await supabase
        .from(Supabase_tables.vehicleTableId)
        .select()
        .eq(Vehicle.SbVar_vehicleId, vehicleUUID)
        .single();

    _currentVehicle = Vehicle.parseVehicleModel(vehicleData as Map);

    //********** add independent to fleet list ***************

    Fleet noFleetPlaceHolder = Fleet();
    noFleetPlaceHolder.fleetId = '';
    noFleetPlaceHolder.fleetName =
        AppLocalizations.of(context).independentLabel;

    _fleetList.add(noFleetPlaceHolder);

    //********** Get Fleet of Vehicle ***************

    _currentFleet = _fleetList.firstWhere(
      (element) => element.fleetId == _currentVehicle!.fleetId,
      orElse: () => noFleetPlaceHolder,
    );

    //********** Pass data to controller ***************
    _vehicleNameController.text = _currentVehicle!.vehicleName!;
    _licensePlateController.text = _currentVehicle!.licensePlate!;
    _numberOfSeatsController.text = _currentVehicle!.numberOfSeats!.toString();
    _numberOfDisabilityFriendlySeats.text =
        _currentVehicle!.numberOfDisabilityFriendlySeats!.toString();

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: createAdminAppBar(vehicleUUID!.isNotEmpty
              ? AppLocalizations.of(context).editVehicleLabel
              : AppLocalizations.of(context).createVehicleLabel),
          drawer: createAdminDrawer(context, this),
          body: Container(
            alignment: Alignment.center,
            child: Container(
              transformAlignment: Alignment.center,
              width: 800,
              child: ListView(
                padding: getDefaultPadding(),
                children: createDefaultPaddedWidgetList([
                  createLabeledTextInputRow(
                      AppLocalizations.of(context).nameLabel,
                      _vehicleNameController),
                  createLabeledTextInputRow(
                      AppLocalizations.of(context).vehicleLicensePlateLabel,
                      _licensePlateController),
                  createLabeledTextInputRow(
                      AppLocalizations.of(context).numberOfSeatsLabel,
                      _numberOfSeatsController),
                  createLabeledTextInputRow(
                      AppLocalizations.of(context)
                          .numberOfDisabilityFriendlySeatsLabel,
                      _numberOfDisabilityFriendlySeats),
                  DropdownSearch<Fleet>(
                    popupProps: const PopupProps.menu(
                      showSelectedItems: true,
                    ),
                    items: _fleetList,
                    itemAsString: (Fleet u) => u.fleetName!,
                    dropdownDecoratorProps: DropDownDecoratorProps(
                      dropdownSearchDecoration: InputDecoration(
                        labelText: AppLocalizations.of(context).fleetLabel,
                      ),
                    ),
                    selectedItem: _currentFleet,
                    onChanged: (value) {
                      _currentFleet = value;
                      _currentVehicle!.fleetId = value!.fleetId;
                    },
                    compareFn: (item1, item2) => item1.fleetId == item2.fleetId,
                  ),
                  ElevatedButton(
                    onPressed: () => _saveData(),
                    child: Text(AppLocalizations.of(context).saveChangesLabel),
                  ),
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
