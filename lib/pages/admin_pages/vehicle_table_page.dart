import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/add_edit_vehicle_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_table.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/supabase_resources/fleet.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/supabase_resources/vehicle.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

class VehicleTablePage extends StatefulWidget {
  const VehicleTablePage({super.key});

  @override
  _VehicleTablePage createState() => _VehicleTablePage();
}

class _VehicleTablePage extends MotisState<VehicleTablePage> {
  _VehicleTablePage();

  List<TableRow>? _tableRows;

  final _searchController = TextEditingController();

  bool _isFiltering = false;

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  List<DataColumn> _getTableHeader() {
    return createColumnsForColumnNames([
      AppLocalizations.of(context).vehicleLicensePlateLabel,
      AppLocalizations.of(context).nameLabel,
      AppLocalizations.of(context).numberOfSeatsLabel,
      AppLocalizations.of(context).numberOfDisabilityFriendlySeatsLabel,
      AppLocalizations.of(context).driverLabel,
      AppLocalizations.of(context).fleetLabel,
      AppLocalizations.of(context).editLabel
    ]);
  }

  Future<bool> _getData() async {
    if (_isFiltering) {
      _isFiltering = false;
      return true;
    }

    //********** Get Data ***************
    final vehicleData =
        await supabase.from(supabase_tables.vehicleTableId).select();
    final profileData =
        await supabase.from(supabase_tables.profilesTableId).select();
    final fleetData =
        await supabase.from(supabase_tables.fleetTableId).select();

    //********** parse Data ***************
    List<Vehicle> vehicleList = (vehicleData as List<dynamic>)
        .map((e) => Vehicle.parseVehicleModel(e as Map))
        .toList();

    List<Profile> profileList = (profileData as List<dynamic>)
        .map((e) => Profile.parseProfileModel(e as Map))
        .toList();

    List<Fleet> fleetList = (fleetData as List<dynamic>)
        .map((e) => Fleet.parseFleetMap(e as Map))
        .toList();

    _tableRows = vehicleList
        .map((e) => _parseToTableRow(e, profileList, fleetList))
        .toList();

    return true;
  }

  TableRow _parseToTableRow(
      Vehicle vehicle, List<Profile> drivers, List<Fleet> fleets) {
    TableRow tableRow = TableRow(
      vehicle.vehicleId!,
      vehicle.licensePlate!,
      vehicle.vehicleName!,
      vehicle.numberOfSeats!.toString(),
      vehicle.numberOfDisabilityFriendlySeats!.toString(),
    );

    if (vehicle.driverID!.isNotEmpty) {
      Profile driverForVehicle =
          drivers.firstWhere((element) => element.userId == vehicle.driverID!);

      tableRow.driverMail = driverForVehicle.eMail;
    }

    if (vehicle.fleetId!.isNotEmpty) {
      Fleet fleetForVehicle =
          fleets.firstWhere((element) => element.fleetId == vehicle.fleetId!);

      tableRow.fleetName = fleetForVehicle.fleetName;
    }
    return tableRow;
  }

  List<DataRow> _createDataRows() {
    if (_tableRows == null) {
      return [];
    }
    return (_tableRows!)
        .where(
          (element) => anyContains(
            [element.name, element.licensePlate],
            _searchController.text,
          ),
        )
        .map(
          (e) => DataRow(
            cells: [
              createTextDataCell(e.licensePlate),
              createTextDataCell(e.name),
              createTextDataCell(e.numberOfSeatsString),
              createTextDataCell(e.numberOfDisabilityFriendlySeatsString),
              createTextDataCell(e.driverMail ?? ''),
              createTextDataCell(e.fleetName ?? ''),
              DataCell(
                ElevatedButton(
                  onPressed: () => _editDataPoint(e.vehicleId),
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: theme.motisPurple,
                  ),
                  child: Icon(
                    Icons.edit_outlined,
                    size: 24,
                    semanticLabel:
                        AppLocalizations.of(context).editVehicleLabel,
                  ),
                ),
              ),
            ],
          ),
        )
        .toList();
  }

  void _editDataPoint(String entityId) {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/vehicle/edit'),
        builder: (_) => AddEditVehiclePage(
          vehicleUUID: entityId,
        ),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  void _createVehicle() {
    Navigator.of(context)
        .push(
      MaterialPageRoute(
        settings: const RouteSettings(name: '/admin/vehicle/add'),
        builder: (_) => AddEditVehiclePage(
          vehicleUUID: '',
        ),
      ),
    )
        .then((value) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: createAdminAppBar(
            AppLocalizations.of(context).manageVehiclesLabel,
          ),
          drawer: createAdminDrawer(context, this),
          body: Padding(
            padding: getDefaultPadding(),
            child: Column(
              children: createDefaultPaddedWidgetList([
                Row(
                  children: createDefaultPaddedWidgetList([
                    ElevatedButton(
                      onPressed: () => _createVehicle(),
                      child:
                          Text(AppLocalizations.of(context).createVehicleLabel),
                    ),
                    Flexible(
                      child: TextFormField(
                        controller: _searchController,
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context).searchLabel,
                        ),
                        onChanged: (search) => setState(() {
                          _isFiltering = true;
                        }),
                      ),
                    ),
                  ]),
                ),
                Flexible(
                  child: MotisTable(
                    columns: _getTableHeader(),
                    rows: _createDataRows(),
                  ),
                ),
              ]),
            ),
          ),
        );
      },
    );
  }
}

class TableRow {
  String vehicleId;
  String licensePlate;
  String name;
  String numberOfSeatsString;
  String numberOfDisabilityFriendlySeatsString;
  String? driverMail;
  String? fleetName;

  TableRow(
    this.vehicleId,
    this.licensePlate,
    this.name,
    this.numberOfSeatsString,
    this.numberOfDisabilityFriendlySeatsString,
  );
}
