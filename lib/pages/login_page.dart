import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends MotisState<LoginPage> {
  bool _isLoading = false;
  bool _redirecting = false;
  late final TextEditingController _emailController;
  late final StreamSubscription<AuthState> _authStateSubscription;

  Future<void> _signIn() async {
    setState(() {
      _isLoading = true;
    });
    try {
      await supabase.auth.signInWithOtp(
        email: _emailController.text,
        emailRedirectTo:
            kIsWeb ? null : 'io.supabase.flutterquickstart://login-callback/',
      );
      if (mounted) {
        context.showSnackBar(
            message: AppLocalizations.of(context).snackbarLookUpEmail);
        _emailController.clear();
      }
    } on AuthException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    _emailController = TextEditingController();
    _authStateSubscription = supabase.auth.onAuthStateChange.listen((data) {
      if (_redirecting) return;
      final session = data.session;
      if (session != null) {
        _redirecting = true;
        Navigator.of(context).pushReplacementNamed('/');
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _authStateSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(builder: (context, state, child) {
      return Scaffold(
        appBar: AppBar(title: Text(AppLocalizations.of(context).buttonLogIn)),
        body: ListView(
          padding: getDefaultPadding(),
          children: createDefaultPaddedWidgetList([
            Text(AppLocalizations.of(context).lableEmailInput),
            TextFormField(
              key: const ValueKey("emailLoginText"),
              controller: _emailController,
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context).decorationEmailInput,
              ),
            ),
            ElevatedButton(
              key: const ValueKey("emailLoginButton"),
              onPressed: _isLoading ? null : _signIn,
              child: Text(
                _isLoading
                    ? AppLocalizations.of(context).buttonSendEmailLoading
                    : AppLocalizations.of(context).buttonSendEmailWaitForInput,
              ),
            ),
            RichText(
              text: TextSpan(
                text: AppLocalizations.of(context).driverRegistrationLink,
                style: const TextStyle(color: Colors.blue),
                recognizer: TapGestureRecognizer()
                  ..onTap = () => Navigator.of(context)
                      .pushReplacementNamed('/driver/registration'),
              ),
            ),
          ]),
        ),
      );
    });
  }
}
