import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart';
import 'package:supabase_quickstart/supabase_resources/trip.dart';

/// Check that the passenger does not have a trip in that timeslot already.
/// Only check with Trips that are scheduled a day before at most for performance.
Future<bool> checkForOverlap(DateTime pickupDate, DateTime dropOffDate) async {
  final data = await supabase
      .from(tripTableId)
      .select()
      .eq('passenger', supabase.auth.currentUser!.id)
      .match({'cancelled': false})
      .gte('scheduled_on', pickupDate.subtract(const Duration(days: 1)))
      .lte('scheduled_on', pickupDate.add(const Duration(days: 1))) as List;

  final List<Trip> possibleTripsThatOverlap = data
      .map((e) => Trip.parseTripModel(e as Map))
      .where(
        (existingTrip) => isOverlapping(existingTrip, pickupDate, dropOffDate),
      )
      .toList();

  if (possibleTripsThatOverlap.isNotEmpty) {
    return true;
  }

  return false;
}

///Returns `true` if [existingTrip] overlaps with [pickupDate] and [dropOffDate].
///
///Returns `false` otherwise.
bool isOverlapping(
  Trip existingTrip,
  DateTime pickupDate,
  DateTime dropOffDate,
) {
  final DateTime predictedDropOffExistingTrip =
      existingTrip.scheduledOn!.add(Duration(seconds: existingTrip.duration!));

  final bool exactOverlap =
      pickupDate.isAtSameMomentAs(existingTrip.scheduledOn!) &&
          dropOffDate.isAtSameMomentAs(predictedDropOffExistingTrip);

  final bool tripIsInBetween = pickupDate.isAfter(existingTrip.scheduledOn!) &&
      dropOffDate.isBefore(predictedDropOffExistingTrip);

  final bool tripStartsBeforeEndsIn =
      pickupDate.isBefore(existingTrip.scheduledOn!) &&
          (dropOffDate.isBefore(predictedDropOffExistingTrip) &&
              dropOffDate.isAfter(existingTrip.scheduledOn!));

  final bool tripsStartsIn = (pickupDate.isAfter(existingTrip.scheduledOn!) &&
          pickupDate.isBefore(predictedDropOffExistingTrip)) &&
      dropOffDate.isAfter(predictedDropOffExistingTrip);

  final bool tripStartsBeforeEndsAfter =
      existingTrip.scheduledOn!.isAfter(pickupDate) &&
          predictedDropOffExistingTrip.isBefore(dropOffDate);

  return exactOverlap ||
      tripsStartsIn ||
      tripStartsBeforeEndsIn ||
      tripIsInBetween ||
      tripStartsBeforeEndsAfter;
}
