// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/account_page.dart';
import 'package:supabase_quickstart/pages/passenger_pages/create_trip_page.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/cancel_trip_resources/cancel_service.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/cancel_trip_resources/cancel_trip_response.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/cancel_trip_resources/passenger_trip_model.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_state.dart';
import 'package:supabase_quickstart/pages/passenger_pages/trip_view_page.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/passenger.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/trip.dart';
import 'package:supabase_quickstart/theme.dart' as theme;
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;

class PassengerMainPage extends StatefulWidget {
  const PassengerMainPage(
      {super.key, required this.userProfile, required this.passengerData});

  final Profile userProfile;
  final Passenger passengerData;

  @override
  MotisState<PassengerMainPage> createState() => PassengerMainPageState(
        userProfile: userProfile,
        passengerData: passengerData,
      );
}

class PassengerMainPageState extends MotisState<PassengerMainPage> {
  PassengerMainPageState({
    required this.passengerData,
    required this.userProfile,
  });

  int selectedPage = 0;
  bool _loading = false;
  bool reload = false;
  Profile userProfile;
  Passenger passengerData;

  AccountPage? accountPage;
  TripViewPage? tripViewPage;
  CreateTripPage? createTripPage;

  Profile getCurrentProfile() {
    return userProfile;
  }

  Passenger getCurrentPassenger() {
    return passengerData;
  }

  @override
  void initState() {
    super.initState();
    accountPage = AccountPage(passengerMainState: this);
    tripViewPage = TripViewPage(passengerMainState: this);
    createTripPage = CreateTripPage(passengerMainState: this);
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedPage = index;
    });
  }

  Widget getPage(int index) {
    switch (index) {
      case 0:
        return TripViewPage(passengerMainState: this);
      case 1:
        return createTripPage!;
      case 2:
        return AccountPage(passengerMainState: this);
      default:
        return const Text('No Page found');
    }
  }

  Future<void> _updateRating(double rating, Trip trip) async {
    final driverData = await supabase
        .from(supabase_tables.driverTableId)
        .select()
        .eq(Driver.SbVar_userId, trip.driverId)
        .single();

    Driver driver = Driver.parseDriverModel(driverData as Map);

    // first rating
    if (trip.rating! == 0) {
      driver.number_of_ratings = driver.number_of_ratings! + 1;
      driver.accumulated_rating = driver.accumulated_rating! + rating.toInt();
    }
    // update rating
    if (trip.rating! > 0) {
      driver.accumulated_rating =
          driver.accumulated_rating! - trip.rating! + rating.toInt();
      //reset rating
      if (rating == 0) {
        driver.number_of_ratings = driver.number_of_ratings! - 1;
      }
    }

    trip.rating = rating.toInt();
    trip.saveToSupabase(context);
    driver.saveToSupabase();
  }

  ///Creates a `PassengerTrip` for [trip] and sends a request to cancel Trip
  Future<void> cancelTrip(Trip trip) async {
    final data = await supabase
        .from('trip')
        .select()
        .match({'id': trip.tripId, 'cancelled': false}).single();

    final Trip passengerTrip = Trip.parseTripModel(data as Map);

    final PassengerTrip tripToCancel = PassengerTrip(
      tripId: passengerTrip.tripId!,
      requestedAt: passengerTrip.requestedAt!,
      driverId: passengerTrip.driverId!,
      passengerId: passengerTrip.passengerId!,
      cancelled: passengerTrip.cancelled!,
      scheduledOn: passengerTrip.scheduledOn!,
      duration: passengerTrip.duration!,
      startLocation: passengerTrip.startLocation!,
      endLocation: passengerTrip.endLocation!,
      startPointDisplayName: passengerTrip.startPointDisplayName!,
      destinationDisplayName: passengerTrip.destinationDisplayName!,
      isDeparturePicked: passengerTrip.isDeparturePicked!,
      rating: passengerTrip.rating!,
    );

    final CancelService cancelService = CancelService();
    final CancelTripResponse cancelResponse =
        await cancelService.cancelTrip(tripToCancel);
    if (cancelResponse.status == MatchingState.SUCCESS) {
      context.showSnackBar(
          message: AppLocalizations.of(context).snackbarCancelTrip);
    } else {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unexpectedError);
    }

    setState(() {
      _loading = false;
    });
  }

  ///Creates the cards containing the trips.
  Widget createTripCard(Trip trip) {
    return Consumer<theme.MotisTheme>(builder: (context, state, child) {
      return Wrap(
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(20),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: SizedBox(
                      height: 350,
                      width: 400,
                      child: ListView(
                        padding: getDefaultPadding(),
                        children: createDefaultPaddedWidgetList([
                          createStaticTextInputRow(
                            AppLocalizations.of(context).fromLabel,
                            trip.startPointDisplayName!,
                          ),
                          createStaticTextInputRow(
                            AppLocalizations.of(context).toLabel,
                            trip.destinationDisplayName!,
                          ),
                          Row(
                            children: [
                              createStaticTextInputRow(
                                AppLocalizations.of(context).departureLabel,
                                DateFormat('EEE, dd.MM.yyyy')
                                    .format(trip.scheduledOn!),
                              ),
                              createStaticTextInputRow(
                                '',
                                DateFormat(' HH:mm').format(trip.scheduledOn!),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              createStaticTextInputRow(
                                AppLocalizations.of(context).arrivalLabel,
                                DateFormat('EEE, dd.MM.yyyy').format(trip
                                    .scheduledOn!
                                    .add(Duration(seconds: trip.duration!))),
                              ),
                              createStaticTextInputRow(
                                '',
                                DateFormat(' HH:mm').format(trip.scheduledOn!
                                    .add(Duration(seconds: trip.duration!))),
                              )
                            ],
                          ),
                          if (trip.cancelled!)
                            Text(
                              AppLocalizations.of(context).cancelled,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          else if (trip.scheduledOn!.isBefore(DateTime.now()))
                            Column(
                              children: [
                                RatingBar.builder(
                                  initialRating: trip.rating!.toDouble(),
                                  itemBuilder: (context, index) {
                                    switch (index) {
                                      case 0:
                                        return const Icon(
                                          Icons.sentiment_very_dissatisfied,
                                          color: Colors.red,
                                        );
                                      case 1:
                                        return const Icon(
                                          Icons.sentiment_dissatisfied,
                                          color: Colors.redAccent,
                                        );
                                      case 2:
                                        return const Icon(
                                          Icons.sentiment_neutral,
                                          color: Colors.amber,
                                        );
                                      case 3:
                                        return const Icon(
                                          Icons.sentiment_satisfied,
                                          color: Colors.lightGreen,
                                        );
                                      case 4:
                                        return const Icon(
                                          Icons.sentiment_very_satisfied,
                                          color: Colors.green,
                                        );
                                      default:
                                        return const Text("");
                                    }
                                  },
                                  onRatingUpdate: (newrating) =>
                                      _updateRating(newrating, trip),
                                ),
                                const Padding(padding: EdgeInsets.all(4.0)),
                                Text(
                                  AppLocalizations.of(context).expired,
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )
                          else
                            ElevatedButton(
                              //*************** Cancel confirm ***************
                              onPressed: () {
                                // Cancel Bestaettigung
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text(
                                        AppLocalizations.of(context).alarm,
                                        textAlign: TextAlign.center,
                                      ),
                                      content: Text(
                                        AppLocalizations.of(context)
                                            .reallyCancel,
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          child: Text(
                                            AppLocalizations.of(context).cancel,
                                          ),
                                          onPressed: () =>
                                              Navigator.of(context).pop(),
                                        ),
                                        TextButton(
                                          child: Text(
                                            AppLocalizations.of(context).accept,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              _loading = true;
                                            });
                                            cancelTrip(trip);
                                            Navigator.of(context).pop();
                                            Navigator.of(context).pop();
                                            setState(() {
                                              _loading = false;
                                            });
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              style: ElevatedButton.styleFrom(
                                foregroundColor: Colors.white,
                                backgroundColor: theme.motisRed,
                              ),
                              child: Icon(
                                Icons.delete_outline_outlined,
                                size: 24,
                                semanticLabel:
                                    AppLocalizations.of(context).cancel,
                              ),
                            ),
                        ]),
                      ),
                    ),
                  );
                },
              );
            },
            //*************** TopLevel Card ********************
            child: Card(
              color: !state.darkTheme
                  ? !trip.cancelled!
                      ? !trip.scheduledOn!.isAfter(DateTime.now())
                          ? const Color.fromARGB(255, 111, 136, 148)
                              .withOpacity(0.12) // upcomming
                          : const Color.fromARGB(
                              255, 148, 213, 243) // past// Cancelled
                      : const Color.fromARGB(255, 255, 176, 169)
                  : !trip.cancelled!
                      ? !trip.scheduledOn!.isAfter(DateTime.now())
                          ? Color.fromARGB(255, 65, 80, 87)
                              .withOpacity(0.12) // upcomming
                          : Color.fromARGB(255, 17, 63, 161) // past// Cancelled
                      : Color.fromARGB(255, 124, 25, 16), // past
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
              clipBehavior: Clip.antiAlias,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    title: Text(
                      "${trip.startPointDisplayName} - ${trip.destinationDisplayName!}",
                    ),
                    subtitle: Text(
                      "${DateFormat('dd.MM').format(trip.scheduledOn!)} | ${DateFormat(' HH:mm').format(trip.scheduledOn!)} - ${DateFormat(' HH:mm').format(trip.scheduledOn!.add(Duration(seconds: trip.duration!)))}",
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    });
  }

  Widget getBottomBar() {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          child: BottomNavigationBar(
            selectedItemColor: state.darkTheme ? Colors.white : Colors.black,
            unselectedItemColor: state.darkTheme ? Colors.grey : Colors.white,
            backgroundColor: state.darkTheme
                ? const Color.fromARGB(100, 9, 101, 212)
                : theme.motisBlue,
            currentIndex: selectedPage,
            onTap: (value) => _onItemTapped(value),
            items: [
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.list,
                ),
                label: AppLocalizations.of(context).tripsLabel,
                backgroundColor: state.darkTheme
                    ? const Color.fromARGB(100, 9, 101, 212)
                    : theme.motisBlue,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.add_box_rounded,
                ),
                label: AppLocalizations.of(context).createTripLabel,
                backgroundColor: state.darkTheme
                    ? const Color.fromARGB(100, 9, 101, 212)
                    : theme.motisBlue,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.person,
                ),
                label: AppLocalizations.of(context).appbarProfile,
                backgroundColor: state.darkTheme
                    ? const Color.fromARGB(100, 9, 101, 212)
                    : theme.motisBlue,
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getPage(selectedPage),
      bottomNavigationBar: getBottomBar(),
    );
  }
}
