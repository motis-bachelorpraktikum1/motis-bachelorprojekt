import 'package:flutter/material.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/osrm_route_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/predictions_resource.dart';

///Encapsulates the schedule user picks for trip
class Schedule {
  late DateTime pickedDate;
  late TimeOfDay pickedTime;
  late bool isDeparturePicked;

  Schedule({
    required this.pickedDate,
    required this.pickedTime,
    required this.isDeparturePicked,
  });
}

///Encapsulates the trip for a preview on the map
class PreviewTripModel {
  final Content content;
  final String startAdress;
  final String destinationAdress;
  final PositionModel startPos;
  final PositionModel destPos;
  final DateTime pickupDate;
  final DateTime dropOffDate;
  final bool isDeparturePicked;

  const PreviewTripModel({
    required this.content,
    required this.startAdress,
    required this.destinationAdress,
    required this.startPos,
    required this.destPos,
    required this.pickupDate,
    required this.dropOffDate,
    required this.isDeparturePicked,
  });
}
