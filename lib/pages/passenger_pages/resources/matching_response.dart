import 'package:json_annotation/json_annotation.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_state.dart';

part 'matching_response.g.dart';

///Encapsulates a response from the server.
///
///MatchingState [status]
///
///Optional driver id
///
///Optional departure information as `String` containing Adress and time
///
///Optional arrival information information as `String` containing Adress and time
@JsonSerializable(explicitToJson: true)
class MatchingResponse {
  MatchingState status;
  String? driverId;
  String? departureInfo;
  String? arrivalInfo;

  MatchingResponse({
    required this.status,
    this.driverId,
    this.departureInfo,
    this.arrivalInfo,
  });

  factory MatchingResponse.fromJson(Map<String, dynamic> json) =>
      _$MatchingResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MatchingResponseToJson(this);
}
