import 'dart:convert';

import 'package:http/http.dart';
import 'package:supabase_quickstart/app_configuration.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_state.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/osrm_route_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_response.dart';

///Communicates with server to match trip to a driver.
class MatchingService {
  ///Sends POST request to server to match Trip [tripInformation].
  ///
  ///Returns a response containing the status.
  Future<MatchingResponse> matchTrip(TripInformation tripInformation) async {
    final Request request = Request('POST', matchingLink);

    final String tripRequest = jsonEncode(
      tripInformation.toJson(),
    );

    request.body = tripRequest;

    request.headers['Content-Type'] = 'application/json';
    request.headers['Accept'] = '*/*';
    request.headers['Accept-Encoding'] = 'gzip, deflate, br';

    final responseStream = await request.send();

    final String responseString = await responseStream.stream.bytesToString();
    if (responseStream.statusCode == 200) {
      // If the server did return a 200 OK response,
      final MatchingResponse response = createResponse(responseString);
      return response;
    } else {
      return MatchingResponse(
        status: MatchingState.REJECTED,
      );
    }
  }

  ///Parses and returns the [responseString] to a valid `MatchingResponse`.
  MatchingResponse createResponse(String responseString) {
    final Map<String, dynamic> serverResponseMap =
        jsonDecode(responseString) as Map<String, dynamic>;

    final MatchingResponse response =
        MatchingResponse.fromJson(serverResponseMap);

    return response;
  }
}
