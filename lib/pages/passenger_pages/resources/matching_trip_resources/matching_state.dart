///State of the response from server.
///
///`SUCCESS` Matched/cancelled succesfully
///
///`FAILED`, Trip could not be matched/cancelled
///
///`REJECTED`, Matching did throw an error
///
///`ERROR` Bad request
enum MatchingState { SUCCESS, FAILED, REJECTED, ERROR }
