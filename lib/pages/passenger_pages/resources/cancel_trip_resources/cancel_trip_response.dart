import 'package:json_annotation/json_annotation.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_state.dart';

part 'cancel_trip_response.g.dart';

///Encapsulates a response from the server.
///
///Contains a MatchingState [status] and an optional message.
@JsonSerializable()
class CancelTripResponse {
  MatchingState status;
  String? message;

  CancelTripResponse({required this.status, this.message});

  factory CancelTripResponse.fromJson(Map<String, dynamic> json) =>
      _$CancelTripResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CancelTripResponseToJson(this);
}
