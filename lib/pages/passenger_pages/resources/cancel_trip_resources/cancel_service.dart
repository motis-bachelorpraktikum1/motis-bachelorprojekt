import 'dart:convert';

import 'package:http/http.dart';
import 'package:supabase_quickstart/app_configuration.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/cancel_trip_resources/cancel_trip_response.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/cancel_trip_resources/passenger_trip_model.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_state.dart';

///Communicates with server to cancel trips.
class CancelService {
  ///Sends POST request to server to cancel [passengerTripToCancel].
  ///
  ///Returns a response containing the status.
  Future<CancelTripResponse> cancelTrip(
      PassengerTrip passengerTripToCancel) async {
    final Request request = Request('POST', cancelLink);

    final String cancelRequest = jsonEncode(
      passengerTripToCancel.toJson(),
    );

    request.body = cancelRequest;

    request.headers['Content-Type'] = 'application/json';
    request.headers['Accept'] = '*/*';
    request.headers['Accept-Encoding'] = 'gzip, deflate, br';

    final responseStream = await request.send();

    final String responseString = await responseStream.stream.bytesToString();
    if (responseStream.statusCode == 200) {
      // If the server did return a 200 OK response,
      final CancelTripResponse response = createResponse(responseString);
      return response;
    } else {
      return CancelTripResponse(
        status: MatchingState.REJECTED,
      );
    }
  }

  ///Parses and returns the [responseString] to a valid `CancelTripResponse`.
  CancelTripResponse createResponse(String responseString) {
    final Map<String, dynamic> serverResponseMap =
        jsonDecode(responseString) as Map<String, dynamic>;

    final CancelTripResponse response =
        CancelTripResponse.fromJson(serverResponseMap);

    return response;
  }
}
