// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cancel_trip_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CancelTripResponse _$CancelTripResponseFromJson(Map<String, dynamic> json) =>
    CancelTripResponse(
      status: $enumDecode(_$MatchingStateEnumMap, json['status']),
      message: json['message'] as String?,
    );

Map<String, dynamic> _$CancelTripResponseToJson(CancelTripResponse instance) =>
    <String, dynamic>{
      'status': _$MatchingStateEnumMap[instance.status]!,
      'message': instance.message,
    };

const _$MatchingStateEnumMap = {
  MatchingState.SUCCESS: 'SUCCESS',
  MatchingState.FAILED: 'FAILED',
  MatchingState.REJECTED: 'REJECTED',
  MatchingState.ERROR: 'ERROR',
};
