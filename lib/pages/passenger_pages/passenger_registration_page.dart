import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/admin_pages/utils_admin.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/utils_passenger.dart';
import 'package:supabase_quickstart/supabase_resources/passenger.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;

class PassengerRegistrationPage extends StatefulWidget {
  const PassengerRegistrationPage({super.key});

  @override
  _PassengerRegistrationPage createState() => _PassengerRegistrationPage();
}

class _PassengerRegistrationPage extends State<PassengerRegistrationPage> {
  Profile? currentProfile;
  Passenger? currentPassenger;
  String? profileUUID;
  final _userNameController = TextEditingController();
  final _fullNameController = TextEditingController();
  bool disabilityFriendlyRequired = false;
  bool _reload = false;

  @override
  void initState() {
    super.initState();
    profileUUID = supabase.auth.currentSession!.user.id;
  }

  Future<void> _saveData() async {
    if (currentProfile == null) {
      return;
    }
    currentProfile!.userName = _userNameController.value.text;
    currentProfile!.fullName = _fullNameController.value.text;

    try {
      await currentProfile!.saveToSupabase();
      await currentPassenger!.saveToSupabase();

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          settings: const RouteSettings(name: '/passenger/main'),
          builder: (_) => PassengerMainPage(
            userProfile: currentProfile!,
            passengerData: currentPassenger!,
          ),
        ),
      );
    } on PostgrestException catch (error) {
      context.showSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
          message: AppLocalizations.of(context).unknownErrorMessage);
    }
  }

  Future<bool> _prepareData() async {
    if (_reload) {
      _reload = false;
      return true;
    }
    var userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, profileUUID)
        .single();

    currentProfile = Profile.parseProfileModel(userData as Map);

    currentPassenger = Passenger();
    currentPassenger!.userId = profileUUID;
    currentPassenger!.disabilityFriendlyRequired = false;

    _userNameController.text = currentProfile!.userName ?? '';
    _fullNameController.text = currentProfile!.fullName ?? '';
    disabilityFriendlyRequired = currentPassenger!.disabilityFriendlyRequired!;

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _prepareData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return getDefaultLoadingScreen();
        }
        return Scaffold(
          appBar: createPassengerAppBar(
              AppLocalizations.of(context).welcomeToMotisOnDemand),
          body: Container(
            alignment: Alignment.center,
            child: Container(
              transformAlignment: Alignment.center,
              width: 400,
              child: ListView(
                padding: getDefaultPadding(),
                children: createDefaultPaddedWidgetList([
                  Text(
                    AppLocalizations.of(context)
                        .registrationInstructionPassengerMessage,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w100,
                      fontSize: 18,
                    ),
                  ),
                  createLabeledTextInputRow(
                      AppLocalizations.of(context).nameLastnameLabel,
                      _fullNameController),
                  createLabeledTextInputRow(
                      AppLocalizations.of(context).userNameLabel,
                      _userNameController),
                  Row(
                    children: [
                      Checkbox(
                        value: currentPassenger!.disabilityFriendlyRequired,
                        onChanged: (bool? value) {
                          setState(() {
                            _reload = true;
                            disabilityFriendlyRequired = value!;
                            currentPassenger!.disabilityFriendlyRequired =
                                disabilityFriendlyRequired;
                            currentPassenger!.saveToSupabase();
                          });
                        },
                      ),
                      Text(AppLocalizations.of(context)
                          .disabilityFriendlyTravellingLabel),
                    ],
                  ),
                  ElevatedButton(
                    onPressed: () => _saveData(),
                    child: Text(AppLocalizations.of(context).saveChangesLabel),
                  ),
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
