import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map/flutter_map.dart'; // Suitable for most situations
import 'package:flutter_map/plugin_api.dart'; // Only import if required functionality is not exposed by default
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/location_services/osmap.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_matching_page.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/create_trip_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/osrm_route_resource.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

class ShowRoutePage extends StatefulWidget {
  final PreviewTripModel? previewTrip;
  final PassengerMainPageState passengerMainState;

  const ShowRoutePage({
    super.key,
    required this.previewTrip,
    required this.passengerMainState,
  });

  @override
  State<ShowRoutePage> createState() => _ShowRoutePageState();
}

class _ShowRoutePageState extends State<ShowRoutePage> {
  PassengerMainPageState? _passengerMainState;
  PreviewTripModel? _previewTrip;
  String? startChosen;
  String? destChosen;
  LatLng? start;
  LatLng? destination;
  Marker? startMarker;
  Marker? destinationMarker;
  List<LatLng>? points;
  Polyline? line;
  DateTime? pickupDate;
  DateTime? dropOffDate;
  String? arrivalTime;
  String? departureTime;
  String? formattedDate;
  late final MapController _mapController;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _initialize();
    _mapController = MapController();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _setCenterAndZoom();
    });
  }

  void _initialize() {
    _passengerMainState = widget.passengerMainState;
    _previewTrip = widget.previewTrip;

    final int routeTime = (_previewTrip!.content.time / 60.0).ceil();
    pickupDate = _previewTrip!.pickupDate;
    dropOffDate = _previewTrip!.dropOffDate;

    formattedDate = DateFormat('dd.MM.yyyy').format(pickupDate!);
    departureTime = DateFormat.Hm().format(_previewTrip!.pickupDate);
    arrivalTime = DateFormat.Hm().format(_previewTrip!.dropOffDate);

    final List<double> coordinates = _previewTrip!.content.polyline.coordinates;
    points = [];

    for (int i = 0; i < coordinates.length - 1; i = i + 2) {
      final LatLng point = LatLng(
        coordinates.elementAt(i),
        coordinates.elementAt(i + 1),
      );
      points!.add(point);
    }

    line =
        Polyline(points: points!, color: Colors.deepPurple, strokeWidth: 4.0);

    _setPoints();
    _setMarkers();
  }

  void _setPoints() {
    start = points!.first;
    destination = points!.last;
    startChosen = _previewTrip!.startAdress;
    destChosen = _previewTrip!.destinationAdress;
  }

  void _setMarkers() {
    startMarker = Marker(
      point: start!,
      builder: (context) => const Icon(
        Icons.location_on,
        color: Colors.red,
        size: 30,
      ),
    );
    destinationMarker = Marker(
      point: destination!,
      builder: (context) => const Icon(
        Icons.location_on,
        color: Colors.red,
        size: 30,
      ),
    );
  }

  void _setCenterAndZoom() {
    final bounds = LatLngBounds.fromPoints(points!);
    final CenterZoom cz = _mapController.centerZoomFitBounds(bounds);
    _mapController.move(cz.center, cz.zoom - 0.5);
  }

  Future<void> _requestDriver() async {
    final TripInformation tripInfoToMatch = TripInformation(
      content: _previewTrip!.content,
      passengerId: supabase.auth.currentUser!.id,
      startAdress: _previewTrip!.startAdress,
      destinationAdress: _previewTrip!.destinationAdress,
      startPos: _previewTrip!.startPos,
      destPos: _previewTrip!.destPos,
      pickedDate: _previewTrip!.isDeparturePicked
          ? _previewTrip!.pickupDate
          : _previewTrip!.dropOffDate,
      isDeparturePicked: _previewTrip!.isDeparturePicked,
    );

    if (!mounted) return;
    Navigator.of(context)
        .push(
          MaterialPageRoute(
            settings: const RouteSettings(name: '/passenger/match'),
            builder: (_) => MatchingPage(
              tripInformation: tripInfoToMatch,
              passengerMainState: _passengerMainState!,
            ),
          ),
        )
        .then(
          (_) => _passengerMainState!.setState(() {
            _passengerMainState!.selectedPage = 0;
            Navigator.pop(context);
          }),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(AppLocalizations.of(context).tripLabel),
            centerTitle: true,
          ),
          body: Column(
            children: [
              Flexible(
                child: MotisMap.showRoute(
                  context,
                  line,
                  start,
                  startMarker,
                  destinationMarker,
                  _mapController,
                ),
              ),
              ListTile(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)),
                tileColor: state.darkTheme ? Colors.black : Colors.white,
                leading: Icon(
                  Icons.route_outlined,
                  color: state.darkTheme ? Colors.white : Colors.black,
                ),
                isThreeLine: true,
                title: Text(
                  style: TextStyle(
                      fontSize: 20,
                      color: state.darkTheme ? Colors.white : Colors.black),
                  AppLocalizations.of(context)
                      .yourRequestedTripMessage(formattedDate!),
                ),
                subtitle: Text(
                  style: TextStyle(
                      fontSize: 18,
                      color: state.darkTheme ? Colors.white : Colors.black),
                  AppLocalizations.of(context).tripInfoMessage(
                    departureTime!,
                    arrivalTime!,
                    _previewTrip!.startAdress,
                    _previewTrip!.destinationAdress,
                  ),
                ),
              ),
              ButtonBar(
                alignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      _requestDriver();
                    },
                    child:
                        Text(AppLocalizations.of(context).searchForDriverLabel),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
