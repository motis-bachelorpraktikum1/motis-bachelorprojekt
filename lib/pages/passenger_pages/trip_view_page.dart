import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/utils_passenger.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/supabase_resources/trip.dart';

class TripViewPage extends StatefulWidget {
  final PassengerMainPageState passengerMainState;
  const TripViewPage({super.key, required this.passengerMainState});

  @override
  State<TripViewPage> createState() => _TripViewPageState();
}

class _TripViewPageState extends State<TripViewPage> {
  late PassengerMainPageState _passengerMainState;
  late Future<bool> future;
  List<Trip> tripList = [];

  @override
  void initState() {
    super.initState();
    _passengerMainState = widget.passengerMainState;
    future = setupPage();
  }

  ///Fetches the trips in future and last 30 days.
  ///
  ///Sorts them by date and active > cancelled & expired
  Future<bool> setupPage() async {
    final tripsData =
        await supabase.from(supabase_tables.tripTableId).select().eq(
              Trip.SbVar_passengerId,
              _passengerMainState.getCurrentPassenger().userId,
            );

    tripList = (tripsData as List)
        .map((e) => Trip.parseTripModel(e as Map))
        .where(
          (element) => element.requestedAt!
              .isAfter(DateTime.now().subtract(const Duration(days: 30))),
        )
        .toList();

    tripList.sort(
      (a, b) {
        if (!a.cancelled! &&
            !b.cancelled! &&
            a.scheduledOn!.isAfter(DateTime.now()) &&
            b.scheduledOn!.isAfter(DateTime.now())) {
          return a.scheduledOn!.compareTo(
              b.scheduledOn!); //show the next trips in descending order
        }
        if (a.cancelled! || !a.scheduledOn!.isAfter(DateTime.now())) {
          return 1; //show cancelled and expired trips at the bottom (and in default ascending order -> cancelled & not expired Trips are above expired)
        }
        return -1;
      },
    );
    return true;
  }

  List<Widget> getTripCards() {
    if (tripList.isEmpty) {
      return [
        Wrap(
          children: [
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              clipBehavior: Clip.antiAlias,
              child: Column(
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    Container(
                      width: 400,
                      height: 400,
                      alignment: Alignment.center,
                      child: Text(
                        AppLocalizations.of(context).noTripPlaned,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 30),
                      ),
                    )
                  ]),
            ),
          ],
        )
      ];
    }
    return createPaddedWidgetList(
      10,
      tripList
          .map(
            (trip) => _passengerMainState.createTripCard(
              trip,
            ),
          )
          .toList(),
    );
  }

  ///Called when user taps on refresh button.
  Future<void> _reload() async {
    setState(() {
      future = setupPage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return const Center(
              child: CircularProgressIndicator(),
            );
          case ConnectionState.done:
          default:
            return Scaffold(
              appBar: AppBar(
                title: Text(AppLocalizations.of(context).yourTrips),
                actions: [
                  ElevatedButton(
                    onPressed: () => _reload(),
                    child: const Icon(Icons.refresh_rounded),
                  )
                ],
              ),
              body: ListView(
                padding: getDefaultPadding(),
                children: getTripCards(),
              ),
            );
        }
      },
    );
  }
}
