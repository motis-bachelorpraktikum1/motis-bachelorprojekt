import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/create_trip_resource.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

PreferredSizeWidget createPassengerAppBar(String title) {
  final String pageTitle = title;
  return PreferredSize(
    preferredSize: const Size.fromHeight(kToolbarHeight),
    child: Consumer<theme.MotisTheme>(
      builder: (context, state, child) {
        const Size.fromHeight(kToolbarHeight);
        return AppBar(
          title: Text(
            pageTitle,
            style: state.darkTheme
                ? const TextStyle(fontSize: 24, color: Colors.white)
                : const TextStyle(fontSize: 24, color: Colors.black),
          ),
          foregroundColor: state.darkTheme ? Colors.black : Colors.white,
          centerTitle: true,
        );
      },
    ),
  );
}

Widget getDefaultBottomBar(BuildContext context, int selectedIndex) {
  return Consumer<theme.MotisTheme>(
    builder: (context, state, child) {
      return ClipRRect(
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        child: BottomNavigationBar(
          selectedItemColor: state.darkTheme ? Colors.white : Colors.black,
          unselectedItemColor: state.darkTheme ? Colors.grey : Colors.white,
          backgroundColor: state.darkTheme
              ? const Color.fromARGB(100, 9, 101, 212)
              : theme.motisBlue,
          onTap: (value) {},
          currentIndex: selectedIndex,
          items: [
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.list,
              ),
              label: AppLocalizations.of(context).tripsLabel,
              backgroundColor: state.darkTheme
                  ? const Color.fromARGB(100, 9, 101, 212)
                  : theme.motisBlue,
            ),
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.add_box_rounded,
              ),
              label: AppLocalizations.of(context).createTripLabel,
              backgroundColor: state.darkTheme
                  ? const Color.fromARGB(100, 9, 101, 212)
                  : theme.motisBlue,
            ),
            BottomNavigationBarItem(
              icon: const Icon(
                Icons.person,
              ),
              label: AppLocalizations.of(context).appbarProfile,
              backgroundColor: state.darkTheme
                  ? const Color.fromARGB(100, 9, 101, 212)
                  : theme.motisBlue,
            ),
          ],
        ),
      );
    },
  );
}

Scaffold getDefaultLoadingScaffold(BuildContext context, int selectedIndex) {
  return Scaffold(
    appBar: createPassengerAppBar(AppLocalizations.of(context).appbarHomePage),
    body: ListView(
      padding: getDefaultPadding(),
      children: createDefaultPaddedWidgetList([
        Container(
          width: 50,
          height: 50,
          padding: getDefaultPadding(),
          alignment: Alignment.center,
          child: const CircularProgressIndicator(),
        ),
      ]),
    ),
    bottomNavigationBar: getDefaultBottomBar(context, selectedIndex),
  );
}

class BottomSheetSwitch extends StatefulWidget {
  BottomSheetSwitch({
    required this.switchValue,
    required this.valueChanged,
    required this.passengerMainState,
    required this.schedule,
  });

  final PassengerMainPageState passengerMainState;
  final bool switchValue;
  final ValueChanged valueChanged;
  final Schedule schedule;

  @override
  _BottomSheetSwitch createState() => _BottomSheetSwitch();
}

class _BottomSheetSwitch extends State<BottomSheetSwitch> {
  //chosen value for toggle buttons
  late bool _switchValue;
  late PassengerMainPageState _passengerMainState;
  final TextEditingController _dateInput = TextEditingController();
  final TextEditingController _timeInput = TextEditingController();
  late DateTime _date;
  late TimeOfDay _time;
  late List<bool> _isDeparturePicked;
  late Schedule _schedule;
  final Color _textFieldColor = Color(0x9fccf5).withOpacity(0.24);

  OutlineInputBorder textFieldBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
  );

  @override
  void initState() {
    _switchValue = widget.switchValue;
    //controls which toggle button is active
    if (_switchValue) {
      _isDeparturePicked = <bool>[true, false];
    } else {
      _isDeparturePicked = <bool>[false, true];
    }
    _date = widget.schedule.pickedDate;
    _time = widget.schedule.pickedTime;
    _passengerMainState = widget.passengerMainState;
    _dateInput.text = DateFormat('dd.MM.yyyy').format(_date);
    _timeInput.text = _time.format(_passengerMainState.context);
    _schedule = Schedule(
      pickedDate: _date,
      pickedTime: _time,
      isDeparturePicked: _switchValue,
    );
    super.initState();
  }

  Future<void> _selectDate() async {
    final DateTime? newDate = await showDatePicker(
      context: _passengerMainState.context,
      initialDate: _date,
      firstDate: DateTime.now(),
      lastDate: _date.add(const Duration(days: 30)),
      helpText: AppLocalizations.of(_passengerMainState.context).whichDateLabel,
      cancelText: AppLocalizations.of(_passengerMainState.context).cancel,
    );
    if (newDate != null) {
      setState(() {
        _date = newDate;
      });
      _dateInput.text = DateFormat('dd.MM.yyyy').format(_date);
    }
  }

  Future<void> _selectTime() async {
    final TimeOfDay? newTime = await showTimePicker(
      context: _passengerMainState.context,
      initialTime: _time,
    );
    if (newTime != null) {
      setState(() {
        _time = newTime;
      });
      _date = DateTime(
        _date.year,
        _date.month,
        _date.day,
        _time.hour,
        _time.minute,
      );
      _timeInput.text = _time.format(context);
    }
  }

  void _saveChangesAndReturn() {
    DateTime _selectedDate = DateTime(
      _date.year,
      _date.month,
      _date.day,
      _time.hour,
      _time.minute,
    );
    _schedule = Schedule(
      pickedDate: _selectedDate,
      pickedTime: _time,
      isDeparturePicked: _switchValue,
    );
    widget.valueChanged(_schedule);
    Navigator.pop(context, _schedule);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: getDefaultPadding(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: createDefaultPaddedWidgetList(
          [
            ToggleButtons(
              selectedColor: theme.motisBlue,
              selectedBorderColor: theme.motisBlue,
              fillColor: theme.motisBlue.withOpacity(0.08),
              splashColor: theme.motisBlue.withOpacity(0.12),
              hoverColor: theme.motisBlue.withOpacity(0.04),
              borderRadius: BorderRadius.circular(4.0),
              constraints: BoxConstraints(minHeight: 36.0),
              isSelected: _isDeparturePicked,
              onPressed: (int index) {
                // Respond to button selection
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < _isDeparturePicked.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      _isDeparturePicked[buttonIndex] = true;
                    } else {
                      _isDeparturePicked[buttonIndex] = false;
                    }
                  }
                  _switchValue = _isDeparturePicked[0];
                });
              },
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(AppLocalizations.of(context).departureLabel),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(AppLocalizations.of(context).arrivalLabel),
                ),
              ],
            ),
            Divider(height: 5, color: Colors.blueGrey.shade600),
            TextField(
              controller: _dateInput,
              decoration: InputDecoration(
                  filled: true,
                  fillColor: _textFieldColor,
                  icon: const Icon(Icons.calendar_today),
                  hintText: AppLocalizations.of(context).chooseDateLabel,
                  border: textFieldBorder),
              readOnly: true,
              onTap: () => _selectDate(),
            ),
            Divider(height: 5, color: Colors.blueGrey.shade600),
            TextField(
              controller: _timeInput,
              decoration: InputDecoration(
                filled: true,
                fillColor: _textFieldColor,
                icon: const Icon(Icons.access_time_filled),
                hintText: AppLocalizations.of(_passengerMainState.context)
                    .chooseTimeLabel,
                border: textFieldBorder,
              ),
              readOnly: true,
              onTap: () => _selectTime(),
            ),
            Divider(height: 5, color: Colors.blueGrey.shade600),
            ElevatedButton(
              onPressed: _saveChangesAndReturn,
              child:
                  Text(AppLocalizations.of(_passengerMainState.context).accept),
            ),
          ],
        ),
      ),
    );
  }
}
