// ignore_for_file: use_build_context_synchronously

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/location_services/osmap.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/create_trip_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/create_trip_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/osrm_route_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/predictions_resource.dart';
import 'package:supabase_quickstart/pages/passenger_pages/show_route_page.dart';
import 'package:supabase_quickstart/pages/passenger_pages/utils_passenger.dart';

class CreateTripPage extends StatefulWidget {
  final PassengerMainPageState passengerMainState;
  const CreateTripPage({super.key, required this.passengerMainState});

  @override
  State<CreateTripPage> createState() => _CreateTripPageState();
}

class _CreateTripPageState extends State<CreateTripPage> {
  late PassengerMainPageState _passengerMainState;

  Map<Suggestions, PositionModel>? destinationMap;
  Map<Suggestions, PositionModel>? startMap;
  final TextEditingController _startController = TextEditingController();
  final TextEditingController _destController = TextEditingController();
  final TextEditingController _scheduleInput = TextEditingController();
  String? startChosen;
  String? destChosen;
  PositionModel? startPos;
  PositionModel? destPos;
  MotisMap? routeMap;
  Position? currentPos;
  late DateTime _date;
  late TimeOfDay _time;
  bool departurePicked = true;
  late Schedule scheduleInfo;
  late String departureMode;
  final Color _textFieldColor = Color(0x9fccf5).withOpacity(0.24);
  OutlineInputBorder textFieldBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(4),
  );

  ///Sets up the schedule with current date
  @override
  void initState() {
    super.initState();
    _passengerMainState = widget.passengerMainState;
    _date = DateTime.now();
    _time = TimeOfDay.fromDateTime(_date);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      scheduleInfo = Schedule(
          pickedDate: _date,
          pickedTime: _time,
          isDeparturePicked: departurePicked);
      departureMode = AppLocalizations.of(context).departureTripInfoString;
      String pickedDateString =
          DateFormat('dd.MM.yyyy (kk:mm)').format(scheduleInfo.pickedDate);
      _scheduleInput.text = departureMode + pickedDateString;
    });
  }

  ///Gets autofill options for start adress.
  Future<void> _fetchStartAdresses(String startAdress) async {
    final PredictionsResource guess = await getResource(startAdress);
    final ContentModel places = guess.content;

    startMap = {
      for (var e in places.guesses)
        Suggestions(
          streetName: e.name,
          cityName:
              e.regions.where((region) => region.adminLevel <= 8).first.name,
        ): e.pos
    };
  }

  ///Gets autofill options for destination adress.
  Future<void> _fetchDestinationAdresses(String destAdress) async {
    final PredictionsResource guess = await getResource(destAdress);
    final ContentModel places = guess.content;

    destinationMap = {
      for (var e in places.guesses)
        Suggestions(
          streetName: e.name,
          cityName:
              e.regions.where((region) => region.adminLevel <= 8).first.name,
        ): e.pos
    };
  }

  ///Removes `, ` from [parsedString].
  ///
  ///Returns a List of String with separated Strings.
  List<String> _resolveString(String parsedString) {
    final int idx = parsedString.indexOf(", ");
    final List<String> parts = [
      parsedString.substring(0, idx).trim(),
      parsedString.substring(idx + 1).trim()
    ];
    return parts;
  }

  ///Returns `Suggestions` for [startChosen].
  Suggestions _getSuggestionFromStart() {
    final List<String> resolvedStart = _resolveString(startChosen!);

    Suggestions? startS;

    for (final Suggestions s in startMap!.keys) {
      if (s.streetName == resolvedStart.elementAt(0) &&
          s.cityName == resolvedStart.elementAt(1)) {
        startS = s;
      }
    }
    return startS!;
  }

  ///Returns `Suggestions` for [destChosen].
  Suggestions _getSuggestionFromDest() {
    final List<String> resolvedDest = _resolveString(destChosen!);

    Suggestions? destS;

    for (final Suggestions s in destinationMap!.keys) {
      if (s.streetName == resolvedDest.elementAt(0) &&
          s.cityName == resolvedDest.elementAt(1)) {
        destS = s;
      }
    }
    return destS!;
  }

  ///Sets [startPos] and [destPos].
  void _setPositions() {
    final Suggestions destS = _getSuggestionFromDest();
    final Suggestions startS = _getSuggestionFromStart();
    startPos = PositionModel(
      latitude: startMap![startS]!.latitude,
      lng: startMap![startS]!.lng,
    );

    destPos = PositionModel(
      latitude: destinationMap![destS]!.latitude,
      lng: destinationMap![destS]!.lng,
    );
  }

  /// Called when user taps `Show Route` button
  Future<void> _createTrip(BuildContext context) async {
    _setPositions();
    final RouteResponse response = await getRouteResource(startPos!, destPos!);
    final Content routeInformation = response.content;
    DateTime pickupDate;
    final DateTime dropOffDate;

    //Abfahrt wurde gewählt
    if (departurePicked) {
      pickupDate = scheduleInfo.pickedDate;
      dropOffDate = scheduleInfo.pickedDate
          .add(Duration(seconds: routeInformation.time as int));
    } else {
      dropOffDate = scheduleInfo.pickedDate;
      pickupDate = scheduleInfo.pickedDate
          .subtract(Duration(seconds: routeInformation.time as int));
    }

    //if Arrival is picked and the pickup time is in past then cancel
    if (!departurePicked && pickupDate.isBefore(DateTime.now())) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).noTripInPastMessage,
      );
      return;
    }

    //Check if the difference between current Time and picked Time is bigger than 5 minutes. Trip cannot be booked
    if (!DateTime.now().difference(pickupDate).isNegative &&
        DateTime.now().difference(pickupDate) > const Duration(minutes: 5)) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).noTripInPastMessage,
      );
      return;
    }

    //Check that the passenger does not have a trip in that timeslot already
    final bool tripsOverlap = await checkForOverlap(pickupDate, dropOffDate);

    if (tripsOverlap) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).tripAlreadyExistsMessage,
      );
      return;
    }

    await _showRouteOnMap(
      routeInformation,
      startPos!,
      destPos!,
      pickupDate,
      dropOffDate,
    );
  }

  ///Creates a `PreviewTripModel` and navigates to `ShowRoutePage`.
  ///
  ///Clears the textfields when user returns to this page
  Future<void> _showRouteOnMap(
    Content routeInformation,
    PositionModel startP,
    PositionModel destP,
    DateTime pickupDate,
    DateTime dropOffDate,
  ) async {
    final PreviewTripModel previewTripInfo = PreviewTripModel(
      content: routeInformation,
      startAdress: startChosen!,
      destinationAdress: destChosen!,
      startPos: startP,
      destPos: destP,
      pickupDate: pickupDate,
      dropOffDate: dropOffDate,
      isDeparturePicked: scheduleInfo.isDeparturePicked,
    );

    if (!mounted) return;
    Navigator.of(context)
        .push(
          MaterialPageRoute(
            settings: const RouteSettings(name: '/passenger/map'),
            builder: (_) => ShowRoutePage(
              previewTrip: previewTripInfo,
              passengerMainState: _passengerMainState,
            ),
          ),
        )
        .then(
          (_) => _passengerMainState.setState(() {
            _passengerMainState.selectedPage = 0;
            _startController.clear();
            _destController.clear();
            _scheduleInput.clear();
          }),
        );
  }

  ///Waits for picked schedule and fills textfield
  Future<void> _selectSchedule(BuildContext context) async {
    await _getInfoModalBottomSheet(context);

    if (departurePicked) {
      departureMode = AppLocalizations.of(context).departureTripInfoString;
    } else {
      departureMode = AppLocalizations.of(context).arrivalTripInfoString;
    }
    String pickedDateString =
        DateFormat('dd.MM.yyyy (kk:mm)').format(scheduleInfo.pickedDate);
    _scheduleInput.text = departureMode + pickedDateString;
  }

  ///Opens a `BottomSheetSwitch` and sets schedule on return.
  Future<void> _getInfoModalBottomSheet(BuildContext context) async {
    return showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      context: context,
      builder: (BuildContext context) {
        return BottomSheetSwitch(
          switchValue: departurePicked,
          schedule: scheduleInfo,
          valueChanged: (value) {
            scheduleInfo = value as Schedule;
            departurePicked = scheduleInfo.isDeparturePicked;
          },
          passengerMainState: _passengerMainState,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          createPassengerAppBar(AppLocalizations.of(context).appbarCreateTrip),
      body: ListView(
        padding: getDefaultPadding(),
        children: createDefaultPaddedWidgetList(
          [
            TypeAheadField(
              hideOnEmpty: true,
              textFieldConfiguration: TextFieldConfiguration(
                controller: _startController,
                autofocus: true,
                decoration: InputDecoration(
                  icon: const Icon(Icons.location_on),
                  filled: true,
                  fillColor: _textFieldColor,
                  border: textFieldBorder,
                  hintText:
                      AppLocalizations.of(context).questionStartPointLabel,
                ),
              ),
              suggestionsCallback: (pattern) async {
                if (pattern.isEmpty || pattern.length < 3) {
                  return const Iterable<String>.empty();
                } else {
                  await _fetchStartAdresses(pattern);
                  return startMap!.keys.map((e) => e.parseString()).toList();
                }
              },
              itemBuilder: (context, String suggestion) {
                return ListTile(
                  leading: const Icon(Icons.location_on),
                  title: Text(suggestion),
                );
              },
              onSuggestionSelected: (String suggestion) {
                _startController.text = suggestion;
                startChosen = suggestion;
              },
            ),
            TypeAheadField(
              hideOnEmpty: true,
              textFieldConfiguration: TextFieldConfiguration(
                controller: _destController,
                autofocus: true,
                decoration: InputDecoration(
                  icon: const Icon(Icons.location_on),
                  filled: true,
                  fillColor: _textFieldColor,
                  border: textFieldBorder,
                  hintText:
                      AppLocalizations.of(context).questionDestinationLabel,
                ),
              ),
              suggestionsCallback: (pattern) async {
                if (pattern.isEmpty || pattern.length < 3) {
                  return const Iterable<String>.empty();
                } else {
                  await _fetchDestinationAdresses(pattern);
                  return destinationMap!.keys
                      .map((e) => e.parseString())
                      .toList();
                }
              },
              itemBuilder: (context, String suggestion) {
                return ListTile(
                  leading: const Icon(Icons.location_on),
                  title: Text(suggestion),
                );
              },
              onSuggestionSelected: (String suggestion) {
                _destController.text = suggestion;
                destChosen = suggestion;
              },
            ),
            TextField(
              controller: _scheduleInput,
              decoration: InputDecoration(
                  filled: true,
                  fillColor: _textFieldColor,
                  icon: const Icon(Icons.access_time),
                  hintText: AppLocalizations.of(context).chooseScheduleLabel,
                  border: textFieldBorder),
              readOnly: true,
              onTap: () => _selectSchedule(context),
            ),
            ElevatedButton(
              onPressed: () => _createTrip(context),
              child: Text(
                AppLocalizations.of(context).showTripLabel,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
