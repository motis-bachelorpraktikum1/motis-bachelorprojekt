import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/components/avatar.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/page_utils/page_utils.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/utils_passenger.dart';
import 'package:supabase_quickstart/supabase_resources/passenger.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;
import 'package:supabase_quickstart/theme.dart' as theme;

class AccountPage extends StatefulWidget {
  final PassengerMainPageState passengerMainState;
  const AccountPage({super.key, required this.passengerMainState});

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  late PassengerMainPageState _passengerMainState;
  Profile? userProfile;
  Passenger? passengerData;
  final _usernameController = TextEditingController();
  final _nameController = TextEditingController();
  String? _avatarUrl;
  bool _updated = false;
  late bool chosenTheme;

  late Future<void> future;

  @override
  void initState() {
    super.initState();
    _passengerMainState = widget.passengerMainState;
    future = setupPage();
  }

  ///Fills textfields with information.
  Future<bool> setupPage() async {
    userProfile = _passengerMainState.getCurrentProfile();
    passengerData = _passengerMainState.getCurrentPassenger();
    _avatarUrl = _passengerMainState.getCurrentProfile().avatarUrl;
    _usernameController.text = userProfile!.userName!;
    _nameController.text = userProfile!.fullName!;

    return true;
  }

  /// Uploads image to Supabase storage from within Avatar widget
  Future<void> _onUpload(String imageUrl) async {
    try {
      final userId = supabase.auth.currentUser!.id;
      await supabase.from(supabase_tables.profilesTableId).update({
        Profile.SbVar_userId: userId,
        Profile.SbVar_avatarUrl: imageUrl,
        Profile.SbVar_updatedAt: DateTime.now().toIso8601String(),
      }).match({'id': userId});
      if (mounted) {
        context.showSnackBar(
          message: AppLocalizations.of(context).updateProfilePicture,
        );
      }
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }

    userProfile!.avatarUrl = imageUrl;
    setState(() {
      _avatarUrl = imageUrl;
    });
  }

  /// Called when Update Profile Button is pressed
  /// Saves the information in textfields to supabase
  Future<void> _updateProfile() async {
    try {
      final userId = supabase.auth.currentUser!.id;
      final userName = _usernameController.text;
      final fullName = _nameController.text;
      await supabase.from(supabase_tables.profilesTableId).update({
        Profile.SbVar_userId: userId,
        Profile.SbVar_userName: userName,
        Profile.SbVar_fullName: fullName,
        Profile.SbVar_updatedAt: DateTime.now().toIso8601String(),
      }).match({'id': userId});
      if (mounted) {
        context.showSnackBar(
          message: AppLocalizations.of(context).updateProfile,
        );
      }
    } on PostgrestException catch (error) {
      context.showErrorSnackBar(message: error.message);
    } catch (error) {
      context.showErrorSnackBar(
        message: AppLocalizations.of(context).unexpectedError,
      );
    }

    final userData = await supabase
        .from(supabase_tables.profilesTableId)
        .select()
        .eq(Profile.SbVar_userId, userProfile!.userId)
        .single();

    userProfile = Profile.parseProfileModel(userData as Map);

    _passengerMainState.setState(() {
      _passengerMainState.userProfile = userProfile!;
      _updated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<theme.MotisTheme>(
      builder: (context, theme, child) {
        chosenTheme = !theme.darkTheme;
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return Scaffold(
              appBar: createPassengerAppBar(
                  AppLocalizations.of(context).appbarProfile),
              body: SingleChildScrollView(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: createDefaultPaddedWidgetList([
                    Avatar(
                      imageUrl: _avatarUrl,
                      onUpload: _onUpload,
                    ),
                    TextField(
                      controller: _usernameController,
                      decoration: InputDecoration(
                          labelText:
                              AppLocalizations.of(context).lableUserName),
                      textAlign: TextAlign.left,
                    ),
                    TextField(
                      controller: _nameController,
                      decoration: InputDecoration(
                          labelText:
                              AppLocalizations.of(context).nameLastnameLabel),
                      textAlign: TextAlign.left,
                    ),
                    ElevatedButton(
                      onPressed: _updateProfile,
                      child: Text(
                        AppLocalizations.of(context).buttonWaitingUpdateProfile,
                      ),
                    ),
                    Text(AppLocalizations.of(context).switchThemeText),
                    Switch(
                      activeColor: Colors.green,
                      activeTrackColor: Colors.blue,
                      inactiveThumbColor: Colors.blueGrey.shade600,
                      inactiveTrackColor: Colors.grey.shade400,
                      splashRadius: 50.0,
                      value: chosenTheme,
                      onChanged: (value) => {
                        chosenTheme = value,
                        if (value)
                          theme.switchThemeLight()
                        else
                          theme.switchThemeDark(),
                      },
                    ),
                  ]),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
