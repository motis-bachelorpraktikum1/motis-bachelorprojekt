import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_response.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_service.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/matching_trip_resources/matching_state.dart';
import 'package:supabase_quickstart/pages/passenger_pages/resources/osrm_route_resource.dart';

class MatchingPage extends StatefulWidget {
  final TripInformation? tripInformation;
  final PassengerMainPageState passengerMainState;

  const MatchingPage(
      {super.key,
      required this.tripInformation,
      required this.passengerMainState});

  @override
  State<MatchingPage> createState() => _MatchingPageState();
}

class _MatchingPageState extends State<MatchingPage> {
  PassengerMainPageState? _passengerMainState;
  TripInformation? _tripInformation;
  late Future<MatchingResponse> _matchingResult;
  String _departureInfo = '';
  String _arrivalInfo = '';
  String _driverId = '';
  MatchingResponse? response;
  String? _driverName;

  @override
  void initState() {
    super.initState();
    _tripInformation = widget.tripInformation;
    _passengerMainState = widget.passengerMainState;
    _matchingResult = _findDriver();
  }

  ///Sends request to match to driver.
  ///
  ///Returns `MatchingResponse`.
  Future<MatchingResponse> _findDriver() async {
    final MatchingService matchingService = MatchingService();
    response = await matchingService.matchTrip(_tripInformation!);

    if (response!.driverId != null) {
      _driverName = await _fetchDriverInfo(response!.driverId!);
    }

    setState(() {
      _driverId = response!.driverId ?? '';
      _departureInfo = response!.departureInfo ?? '';
      _arrivalInfo = response!.arrivalInfo ?? '';
    });
    return response!;
  }

  ///Fetch the fullname of driver that is found for [driverId].
  Future<String> _fetchDriverInfo(String driverId) async {
    final data = await supabase
        .from('profiles')
        .select('full_name')
        .eq('id', driverId)
        .single() as Map;

    return data['full_name'] as String;
  }

  ///Called when `Back` button is pressed.
  ///
  ///Returns back to `TripViewPage`.
  void _returnBackToView() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _matchingResult,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        return Scaffold(
          appBar: AppBar(
            title: Text(
              response!.status == MatchingState.SUCCESS
                  ? AppLocalizations.of(context).driverFoundMessage
                  : AppLocalizations.of(context).noDriverMessage,
            ),
            centerTitle: true,
          ),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Text(
                  style: const TextStyle(fontSize: 22),
                  response!.status == MatchingState.SUCCESS
                      ? "${AppLocalizations.of(context).driverLabel}: $_driverName"
                      : AppLocalizations.of(context).tryAgainMessageMatching,
                ),
                if (response!.status == MatchingState.SUCCESS)
                  Divider(height: 5, color: Colors.blueGrey.shade600),
                Text(
                  style: const TextStyle(fontSize: 22),
                  response!.status == MatchingState.SUCCESS
                      ? '${AppLocalizations.of(context).departureTripInfoString} $_departureInfo'
                      : '',
                ),
                if (response!.status == MatchingState.SUCCESS)
                  Divider(height: 5, color: Colors.blueGrey.shade600),
                Text(
                  style: const TextStyle(fontSize: 22),
                  response!.status == MatchingState.SUCCESS
                      ? '${AppLocalizations.of(context).arrivalTripInfoString} $_arrivalInfo'
                      : '',
                ),
                if (response!.status == MatchingState.SUCCESS)
                  Divider(height: 5, color: Colors.blueGrey.shade600),
                ElevatedButton(
                  onPressed: _returnBackToView,
                  child: Text(AppLocalizations.of(context).backButtonLabel),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
