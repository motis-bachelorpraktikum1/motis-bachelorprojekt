import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_main.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_registration_completion.dart';
import 'package:supabase_quickstart/pages/page_utils/motis_state.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_main.dart';
import 'package:supabase_quickstart/supabase_resources/driver.dart';
import 'package:supabase_quickstart/supabase_resources/passenger.dart';
import 'package:supabase_quickstart/supabase_resources/profile.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends MotisState<SplashPage> {
  bool _redicrectCalled = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _redirect();
  }

  ///Redirects the user based on assigned role to the corresponding app part.
  ///
  ///If not logged in he is sent to the login page.
  ///
  ///If it is the first login he is sent to the registration page
  Future<void> _redirect() async {
    await Future.delayed(Duration.zero);
    if (_redicrectCalled || !mounted) {
      return;
    }

    _redicrectCalled = true;
    final session = supabase.auth.currentSession;
    if (session != null) {
      try {
        final userId = supabase.auth.currentUser!.id;
        final data = await supabase
            .from('profiles')
            .select('role')
            .eq('id', userId)
            .single() as Map;

        final role = (data['role'] ?? '') as String;

        switch (role) {
          case 'passenger':
            final profileData = await supabase
                .from(supabase_tables.profilesTableId)
                .select()
                .eq(Profile.SbVar_userId, userId)
                .single();

            final Profile profile =
                Profile.parseProfileModel(profileData as Map);

            final passengerData = await supabase
                .from(supabase_tables.passengerTableId)
                .select()
                .eq(Passenger.SbVar_userId, userId);

            if ((passengerData as List).isEmpty) {
              Navigator.of(context)
                  .pushReplacementNamed('/passenger/registration');
            }

            final Passenger passenger =
                Passenger.parsePassenger(passengerData[0] as Map);

            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                settings: const RouteSettings(name: '/passenger/main'),
                builder: (_) => PassengerMainPage(
                  userProfile: profile,
                  passengerData: passenger,
                ),
              ),
            );
            break;
          case 'admin':
            Navigator.of(context).pushReplacementNamed('/admin');
            break;
          case 'driver':
            final profileData = await supabase
                .from(supabase_tables.profilesTableId)
                .select()
                .eq(Profile.SbVar_userId, userId)
                .single();

            final Profile profile =
                Profile.parseProfileModel(profileData as Map);

            final driverData = await supabase
                .from(supabase_tables.driverTableId)
                .select()
                .eq(Driver.SbVar_userId, userId) as List;

            if (driverData.isEmpty) {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  settings: const RouteSettings(
                      name: '/driver/registration/completion'),
                  builder: (_) => const DriverRegistrationPage(),
                ),
              );
              return;
            }

            final Driver driver = Driver.parseDriverModel(
              driverData[0] as Map<dynamic, dynamic>,
            );
            if (!driver.registered!) {
              debugPrint("Not Registered ${profile.userId}");
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  settings: const RouteSettings(
                      name: '/driver/registration/completion'),
                  builder: (_) => const DriverRegistrationPage(),
                ),
              );
              return;
            }

            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                settings: const RouteSettings(name: '/driver/main'),
                builder: (_) => DriverMainPage(
                  userProfile: profile,
                  driverData: driver,
                ),
              ),
            );
            break;

          default:
            Navigator.of(context).pushReplacementNamed('/login');
        }
      } on Exception catch (error) {
        context.showErrorSnackBar(
          message: AppLocalizations.of(context).unexpectedError,
        );
        Navigator.of(context).pushReplacementNamed('/login');
      }
    } else {
      Navigator.of(context).pushReplacementNamed('/login');
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );
  }
}
