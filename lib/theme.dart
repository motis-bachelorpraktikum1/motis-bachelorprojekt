import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

const Color motisRed = Color.fromARGB(255, 255, 61, 109);
const Color motisBlue = Color.fromARGB(255, 16, 137, 254);
const Color motisOrange = Color.fromARGB(255, 255, 185, 92);
const Color motisPurple = Color.fromARGB(255, 127, 43, 255);
const Color motisDarkBlue = Color.fromARGB(100, 50, 130, 184);
const Color motisDarkOrange = Color.fromARGB(255, 182, 105, 5);

ThemeData getMotisLightTheme(BuildContext context) {
  return ThemeData(
    colorScheme: ColorScheme.light(
      primary: motisBlue.withOpacity(0.72),
      secondary: motisBlue,
    ),
    useMaterial3: true,
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: motisBlue,
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: motisBlue,
      ),
    ),
    textTheme: GoogleFonts.ralewayTextTheme(
      Theme.of(context).textTheme.apply(
            bodyColor: Colors.black,
            displayColor: Colors.black,
            fontSizeDelta: 2,
          ),
    ),
  );
}

ThemeData getMotisDarkTheme(BuildContext context) {
  return ThemeData(
    colorScheme: ColorScheme.dark(
      primary: motisDarkBlue.withOpacity(0.72),
      secondary: motisDarkBlue,
    ),
    useMaterial3: true,
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: motisDarkBlue, // Use this
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: motisDarkBlue,
      ),
    ),
    textTheme: GoogleFonts.ralewayTextTheme(
      Theme.of(context).textTheme.apply(
            bodyColor: Colors.white,
            displayColor: Colors.white,
            fontSizeDelta: 2,
          ),
    ),
  );
}

///Observer for Theme
///
///Notifies its listeners
class MotisTheme extends ChangeNotifier {
  final String key = "theme";
  SharedPreferences? _prefs;
  late bool _darkTheme;

  bool get darkTheme => _darkTheme;

  MotisTheme() {
    _darkTheme = false;
    _loadprefs();
  }

  void switchThemeLight() {
    _darkTheme = false;
    _savePrefs();
    notifyListeners();
  }

  void switchThemeDark() {
    _darkTheme = true;
    _savePrefs();
    notifyListeners();
  }

  Future<void> _initiatePrefs() async {
    _prefs ??= await SharedPreferences.getInstance();
  }

  Future<void> _loadprefs() async {
    await _initiatePrefs();
    _darkTheme = _prefs?.getBool(key) ?? true;
    notifyListeners();
  }

  Future<void> _savePrefs() async {
    await _initiatePrefs();
    _prefs?.setBool(key, _darkTheme);
  }
}
