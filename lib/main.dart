//versuche "flutter pub upgrade" in der Konsole auszuführen wenn VSCode sagt:
//"Target of URI doesn't exist."
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/app_configuration.dart' as config;
import 'package:supabase_quickstart/l10n/l10n.dart';
import 'package:supabase_quickstart/pages/admin_pages/admin_home_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/admin_trip_calender_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/driver_applications_table_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/fleet_table_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/profile_table_page.dart';
import 'package:supabase_quickstart/pages/admin_pages/vehicle_table_page.dart';
import 'package:supabase_quickstart/pages/driver_login_page.dart';
import 'package:supabase_quickstart/pages/driver_pages/driver_registration_completion.dart';
import 'package:supabase_quickstart/pages/login_page.dart';
import 'package:supabase_quickstart/pages/passenger_pages/passenger_registration_page.dart';
import 'package:supabase_quickstart/pages/splash_page.dart';
import 'package:supabase_quickstart/supabase_resources/role.dart';
import 'package:supabase_quickstart/theme.dart' as theme;

Future<void> main() async {
  await Supabase.initialize(
    url: config.url,
    anonKey: config.anon,
  );
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late ThemeData light;
  late ThemeData dark;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => theme.MotisTheme(),
      child: Consumer<theme.MotisTheme>(
        builder: (context, state, child) {
          return MaterialApp(
            builder: (context, child) => MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
              child: child!,
            ),
            theme: state.darkTheme
                ? dark = theme.getMotisDarkTheme(context)
                : light = theme.getMotisLightTheme(context),
            title: 'Motis OnDemand',
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: l10n.all,
            initialRoute: '/',
            routes: <String, WidgetBuilder>{
              '/': (_) => const SplashPage(),
              '/login': (_) => const LoginPage(),
              //--------------- Passenger Pages ---------------
              '/passenger/registration': (_) =>
                  const PassengerRegistrationPage(),
              //--------------- Admin Pages ---------------
              '/admin': (_) => const AdminHomePage(),
              '/admin/drivers': (_) => const ProfileTablePage(Role.DRIVER),
              '/admin/passenger': (_) => const ProfileTablePage(Role.PASSENGER),
              '/admin/driver-applications': (_) =>
                  const DriverApplicationsTable(),
              '/admin/fleet': (_) => const FleetTablePage(),
              '/admin/vehicle': (_) => const VehicleTablePage(),
              '/admin/trips-today': (_) => const AdminTripCalendarPage(),
              //--------------- Driver Pages ---------------
              '/driver/registration': (_) => const DriverLoginPage(),
              '/driver/registration/completion': (_) =>
                  const DriverRegistrationPage(),
            },
          );
        },
      ),
    );
  }
}
