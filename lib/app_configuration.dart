//Api Credentials
const String url = 'https://untvcekocvlasqgidlgx.supabase.co';
const String anon =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVudHZjZWtvY3ZsYXNxZ2lkbGd4Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzM3OTA3MTksImV4cCI6MTk4OTM2NjcxOX0.XHmJekEenTENZnSMDrrW6B_ie771iuBZEK8-zRupHLs';

//********** Android Emulator URLs **********/
const String matchingUrl = 'http://10.0.2.2:8000/trip/create';
final Uri matchingLink = Uri.parse(matchingUrl);

const String cancelUrl = 'http://10.0.2.2:8000/trip/cancel';
final Uri cancelLink = Uri.parse(cancelUrl);
