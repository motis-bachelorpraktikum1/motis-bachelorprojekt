enum Role {
  DRIVER,
  PASSENGER,
  ADMIN;

  @override
  String toString() {
    return name.toLowerCase();
  }
}
