import 'package:flutter/widgets.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;

class Trip {
  static String SbVar_tripId = 'id';
  String? tripId;
  static String SbVar_requestedAt = 'requested_at';
  DateTime? requestedAt;
  static String SbVar_driverId = 'driver';
  String? driverId;
  static String SbVar_passengerId = 'passenger';
  String? passengerId;
  static String SbVar_cancelled = 'cancelled';
  bool? cancelled;
  static String SbVar_scheduledOn = 'scheduled_on';
  DateTime? scheduledOn;
  static String SbVar_duration = 'duration';
  int? duration;

  static String SbVar_startLocation = 'start_location';
  String? startLocation;
  static String SbVar_endLocation = 'end_location';
  String? endLocation;

  static String SbVar_startPointDisplayName = 'start_point_display_name';
  String? startPointDisplayName;
  static String SbVar_destinationDisplayName = 'destination_display_name';
  String? destinationDisplayName;

  static String SbVar_departurePicked = 'departure_picked';
  bool? isDeparturePicked;

  static String SbVar_rating = 'rating';
  int? rating;

  static Trip parseTripModel(Map<dynamic, dynamic> tripMap) {
    Trip trip = new Trip();
    trip.tripId = tripMap[SbVar_tripId] as String;
    trip.requestedAt = DateTime.parse(tripMap[SbVar_requestedAt] as String);
    trip.driverId = (tripMap[SbVar_driverId] ?? '') as String;
    trip.passengerId = tripMap[SbVar_passengerId] as String;
    trip.cancelled = tripMap[SbVar_cancelled] as bool;
    trip.scheduledOn = DateTime.parse(tripMap[SbVar_scheduledOn] as String);
    trip.duration = tripMap[SbVar_duration] as int;
    trip.startLocation = tripMap[SbVar_startLocation] as String;
    trip.endLocation = tripMap[SbVar_endLocation] as String;
    trip.startPointDisplayName = tripMap[SbVar_startPointDisplayName] as String;
    trip.destinationDisplayName =
        tripMap[SbVar_destinationDisplayName] as String;
    trip.isDeparturePicked = tripMap[SbVar_departurePicked] as bool;
    trip.rating = tripMap[SbVar_rating] as int;
    return trip;
  }

  Future<void> saveToSupabase(BuildContext context) async {
    await supabase.from(supabase_tables.tripTableId).upsert({
      SbVar_tripId: tripId,
      SbVar_requestedAt: requestedAt!.toIso8601String(),
      SbVar_driverId: driverId ?? 'null',
      SbVar_passengerId: passengerId,
      SbVar_cancelled: cancelled,
      SbVar_scheduledOn: scheduledOn!.toIso8601String(),
      SbVar_duration: duration,
      SbVar_startLocation: startLocation,
      SbVar_endLocation: endLocation,
      SbVar_startPointDisplayName: startPointDisplayName,
      SbVar_destinationDisplayName: destinationDisplayName,
      SbVar_departurePicked: isDeparturePicked,
      SbVar_rating: rating,
    }).eq(SbVar_tripId, tripId);
  }
}
