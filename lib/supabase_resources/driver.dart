import 'package:flutter/cupertino.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/supabase_resources/area_of_operation.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;

class Driver {
  static String SbVar_userId = 'id';
  String? userId;
  static String SbVar_currentVehicleId = 'current_vehicle';
  String? currentVehicleId;
  static String SbVar_areaOfOperationPolygons = 'area_of_operation_polygons';
  AreaOfOperation? areaOfOperationPolygons;
  static String SbVar_fleetAssignedId = 'fleet_assigned';
  String? fleetId;
  static String SbVar_registered = 'registered';
  bool? registered;
  String? polygonWKT;
  static String SbVar_accumulated_rating = 'accumulated_rating';
  int? accumulated_rating;
  static String SbVar_number_of_ratings = 'number_of_ratings';
  int? number_of_ratings;

  static Driver parseDriverModel(Map<dynamic, dynamic> driverMap) {
    Driver driver = new Driver();
    driver.userId = driverMap[SbVar_userId] as String;
    driver.currentVehicleId =
        (driverMap[SbVar_currentVehicleId] ?? '') as String;
    driver.areaOfOperationPolygons = driverMap[SbVar_areaOfOperationPolygons] ==
            null
        ? null
        : AreaOfOperation.fromJson(
            driverMap[SbVar_areaOfOperationPolygons] as Map<String, dynamic>,
          );
    driver.fleetId = (driverMap[SbVar_fleetAssignedId] ?? '') as String;
    driver.registered = driverMap[SbVar_registered] as bool;
    driver.accumulated_rating = driverMap[SbVar_accumulated_rating] as int;
    driver.number_of_ratings = driverMap[SbVar_number_of_ratings] as int;
    return driver;
  }

  Future<void> deleteAreaOfOperation() async {
    await supabase
        .from(supabase_tables.driverTableId)
        .update({SbVar_areaOfOperationPolygons: null}).eq(SbVar_userId, userId);
  }

  Future<void> saveToSupabase() async {
    Map<String, dynamic> saveMap = {
      SbVar_userId: userId,
      SbVar_registered: registered,
      SbVar_currentVehicleId: currentVehicleId,
      SbVar_fleetAssignedId: fleetId,
      SbVar_accumulated_rating: accumulated_rating,
      SbVar_number_of_ratings: number_of_ratings,
    };

    await supabase
        .from(supabase_tables.driverTableId)
        .upsert(saveMap)
        .eq(SbVar_userId, userId);

    if ((polygonWKT ?? '').isEmpty) {
      return;
    } else {
      try {
        String status = await supabase.rpc(
          'insert_polygon',
          params: {
            'driver_id': userId,
            'polyg': polygonWKT,
          },
        ) as String;
        if (status == "Success") {
          debugPrint("Success");
        }
      } on PostgrestException catch (error) {
        debugPrint(error.message);
      }
    }
  }
}
