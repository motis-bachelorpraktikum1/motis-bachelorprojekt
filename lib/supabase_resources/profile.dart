import 'package:flutter/cupertino.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/supabase_resources/role.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart'
    as supabase_tables;

class Profile {
  static String SbVar_userId = 'id';
  String? userId;
  static String SbVar_updatedAt = 'updated_at';
  String? updatedAt;
  static String SbVar_userName = 'username';
  String? userName;
  static String SbVar_fullName = 'full_name';
  String? fullName;
  static String SbVar_avatarUrl = 'avatar_url';
  String? avatarUrl;
  static String SbVar_eMail = 'email';
  String? eMail;
  static String SbVar_role = 'role';
  Role? role;

  static Profile parseProfileModel(Map<dynamic, dynamic> userMap) {
    Profile user = new Profile();
    user.userId = userMap[SbVar_userId] as String;
    user.updatedAt = (userMap[SbVar_updatedAt] ?? '') as String;
    user.userName = (userMap[SbVar_userName] ?? '') as String;
    user.fullName = (userMap[SbVar_fullName] ?? '') as String;
    user.avatarUrl = (userMap[SbVar_avatarUrl] ?? '') as String;
    user.eMail = (userMap[SbVar_eMail] ?? '') as String;
    String? roleString = (userMap[SbVar_role] ?? '') as String;

    if (roleString.isNotEmpty) {
      user.role = _parseRoleString(roleString);
    }
    return user;
  }

  static Role _parseRoleString(String roleString) {
    switch (roleString) {
      case 'passenger':
        return Role.PASSENGER;
      case 'driver':
        return Role.DRIVER;
      case 'admin':
        return Role.ADMIN;
      default:
        throw ArgumentError('Invalid RoleString found: {}', roleString);
    }
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.profilesTableId).upsert({
      SbVar_userId: userId,
      SbVar_updatedAt: DateTime.now().toIso8601String(),
      SbVar_userName: userName!.isEmpty ? '' : userName!,
      SbVar_fullName: fullName!.isEmpty ? '' : fullName,
      SbVar_avatarUrl: avatarUrl!.isEmpty ? '' : avatarUrl,
      SbVar_eMail: eMail,
      SbVar_role: role.toString(),
    }).eq(SbVar_userId, userId);
  }
}
