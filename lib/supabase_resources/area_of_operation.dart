// ignore_for_file: avoid_dynamic_calls

import 'package:latlong2/latlong.dart';

class AreaOfOperation {
  Coordinates coordinates;

  AreaOfOperation({
    required this.coordinates,
  });

  factory AreaOfOperation.fromJson(Map<String, dynamic> json) {
    return AreaOfOperation(
      coordinates: Coordinates.coordinatesFromList(
        (json['coordinates'] as List<dynamic>).first as List<dynamic>,
      ),
    );
  }
}

class Coordinates {
  List<LatLng> coordinateList;

  Coordinates({
    required this.coordinateList,
  });

  factory Coordinates.coordinatesFromList(List<dynamic> innerList) {
    return Coordinates(
      coordinateList:
          innerList.map((e) => LatLng(e[1] as double, e[0] as double)).toList(),
    );
  }
}
