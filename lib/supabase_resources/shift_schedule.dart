import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/supabase_resources/supabase_tables.dart' as supabase_tables;

class ShiftSchedule {
  static String SbVar_shiftId = 'shift_id';
  String? shiftId;
  static String SbVar_driverId = 'driver_id';
  String? driverId;
  static String SbVar_startTime= 'start_time';
  DateTime? startTime;
  static String SbVar_endTime = 'end_time';
  DateTime? endTime;

  static ShiftSchedule parseShiftSchedule(Map<dynamic, dynamic> shiftMap) {
    ShiftSchedule shift = ShiftSchedule();
    shift.shiftId = shiftMap[SbVar_shiftId] as String;
    shift.driverId = shiftMap[SbVar_driverId] as String;
    shift.startTime = DateTime.parse(shiftMap[SbVar_startTime] as String);
    shift.endTime = DateTime.parse(shiftMap[SbVar_endTime] as String);

    return shift;
  }

  Future<void> saveToSupabase() async {
    await supabase.from(supabase_tables.driverScheduleTableId).upsert({
      SbVar_shiftId: shiftId,
      SbVar_driverId: driverId,
      SbVar_startTime: startTime!.toIso8601String(),
      SbVar_endTime: endTime!.toIso8601String(),
    }).eq(SbVar_driverId, driverId);
  }
}
