# Contributors:

- Faran Ahmad
- Daniel Bossauer
- Daniel Dennert
- Florian Guldan
- Maxim Pasitschni

# Motis OnDemand Application

This repo is a mobile/web application that enables on-demand ridesharing. It is an extension to the already existing Motis project.
Motis aims to get people to their destination through appropriate routing and utilization of different transport options. This includes the use of trains, buses, planes, ridesharing etc. Motis - OnDemand serves as a ridesharing complement.

It allows users to book rides in advance, with respect to otherwise used transport options, i.e. Motis - OnDemand takes care that you do not miss your connecting train. In addition, unlike other competitors in the industry, Motis - OnDemand takes care to achieve a favorable environmental balance. The aim is to fill up moving vehicles as much as possible so that multiple vehicles do not have to travel a similar route.

Motis OnDemand also allows users to operate as a driver. The driver can schedule work hours aswells as provide an area.

A web application is added that allows administrators to modify and manage profiles of users, drivers, vehicles, routes, etc. This includes: creating, deleting and managing profiles aswell as creation, deletion and modification of user data. Furthermore, the viewing of driver routes. The Admin is also ablte to manage fleets. That is, quantities of vehicles that are made available by Motis.

[Documentation](https://docs.google.com/document/d/1WEN4aRD5h_PyLfFoJGwf_sX7P__PsryJSMWL4h4M_VE/edit#).

## Getting Started

Before running this app, you need to create a Supabase project and copy [your credentials](https://supabase.io/docs/guides/with-flutter#get-the-api-keys) to `main.dart`.

You can run this app on iOS, Android or the Web.

To run this application follow these steps:

[Install Flutter](https://docs.flutter.dev/get-started/install)

Install the German and English localizations:

```bash
flutter gen-l10n
```

```bash
flutter run
```

Additional steps if errors occur:

```bash
flutter pub get
```

```bash
flutter pub upgrade
```
